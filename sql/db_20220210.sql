/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.4.21-MariaDB : Database - db_mki_hris
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_mki_hris` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `db_mki_hris`;

/*Table structure for table `mst_menu` */

DROP TABLE IF EXISTS `mst_menu`;

CREATE TABLE `mst_menu` (
  `menu_id` int(5) NOT NULL AUTO_INCREMENT,
  `app_code` varchar(4) DEFAULT NULL,
  `menu_name` varchar(50) NOT NULL,
  `menu_url` varchar(75) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `menu_group` varchar(20) NOT NULL,
  `description` text DEFAULT NULL,
  `menu_no` smallint(2) DEFAULT NULL,
  `group_no` smallint(2) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `mst_menu` */

insert  into `mst_menu`(`menu_id`,`app_code`,`menu_name`,`menu_url`,`icon`,`menu_group`,`description`,`menu_no`,`group_no`,`is_active`) values 
(1,'ADM','Menu','admin/menu','fas fa-user-tie','Admin',NULL,1,3,1),
(2,'ADM','Users','admin/users','fas fa-user-tie','Admin',NULL,2,3,1),
(3,'ADM','User Access','admin/access','fas fa-user-tie','Admin',NULL,3,3,1),
(4,'MST','Biodata','master/biodata','fas fa-desktop','Master',NULL,3,1,1),
(5,'TRN','Timesheet & Manual Value','transaction/timesheet','fas fa-file-alt','Transaction',NULL,1,2,1),
(6,'TLS','Recalcullate','tools/recalculate','fas fa-tools','Tools',NULL,1,4,1),
(7,'HLP','Tutorial','help/tutorial','fas fa-question','Help',NULL,1,5,1),
(8,'MST','Class','master/mt_class','fas fa-desktop','Master',NULL,6,1,1),
(9,'MST','Salary','master/mt_salary','fas fa-desktop','Master',NULL,5,1,1),
(10,'MST','Contract','master/mt_contract','fas fa-desktop','Master',NULL,4,1,1),
(12,'MST','Master Bank','master/mt_bank','fas fa-desktop','Master',NULL,7,1,1),
(13,'MST','Master Process Closing','master/mt_process_closing','fas fa-desktop','Master',NULL,10,1,1),
(14,'TRN','Process','transaction/trn_slip','fas fa-desktop','Transaction',NULL,1,2,1),
(15,'MST','Statutory Date','master/mt_st_date','fas fa-desktop','Master',NULL,11,2,1),
(16,'TRN','Report','transaction/trn_report','fas fa-desktop','Transaction',NULL,1,2,1),
(17,'MST','Dept','master/mt_dept','fas fa-desktop','Master',NULL,2,1,1),
(18,'MST','Pvb Dept','master/mt_pvb_dept','fas fa-desktop','Master',NULL,9,1,1),
(19,'MST','Currency Rate','master/mt_currency_rate','fas fa-desktop','Master',NULL,8,1,1),
(20,'MST','Company','master/company','fas fa-desktop','Master',NULL,1,1,1);

/*Table structure for table `mst_user` */

DROP TABLE IF EXISTS `mst_user`;

CREATE TABLE `mst_user` (
  `user_id` int(8) NOT NULL,
  `nip` varchar(15) NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `user_name` varchar(25) NOT NULL,
  `user_password` varchar(30) NOT NULL,
  `user_group` varchar(20) DEFAULT NULL,
  `phone_imei` varchar(25) DEFAULT NULL,
  `user_level` int(1) DEFAULT NULL COMMENT 'Tingkatan pada approve PR',
  `last_login` datetime DEFAULT NULL,
  `last_ip` varchar(45) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `pic_input` int(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mst_user` */

insert  into `mst_user`(`user_id`,`nip`,`full_name`,`user_name`,`user_password`,`user_group`,`phone_imei`,`user_level`,`last_login`,`last_ip`,`is_active`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(19,'1999','Venny','admin','asd',NULL,NULL,1,NULL,NULL,1,NULL,NULL,NULL,NULL),
(20,'2021','Ricky','ricky','1234',NULL,NULL,1,NULL,NULL,1,NULL,NULL,NULL,NULL),
(21,'12154','tmk','tmk','password',NULL,NULL,1,NULL,NULL,1,NULL,NULL,NULL,NULL);

/*Table structure for table `mst_user_activity` */

DROP TABLE IF EXISTS `mst_user_activity`;

CREATE TABLE `mst_user_activity` (
  `activity_id` int(11) NOT NULL AUTO_INCREMENT,
  `activity` varchar(255) DEFAULT NULL,
  `module` varchar(100) DEFAULT NULL,
  `table` varchar(100) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `pic_input` int(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `mst_user_activity` */

/*Table structure for table `mt_allowance` */

DROP TABLE IF EXISTS `mt_allowance`;

CREATE TABLE `mt_allowance` (
  `ma_id` int(10) NOT NULL,
  `company_id` int(10) NOT NULL,
  `ma_name` varchar(20) NOT NULL,
  `ma_formula` varchar(100) NOT NULL COMMENT 'ex: Base*(10/100), BasePay*(5/100), Attendance*(Base/173),..',
  `is_tax` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1 = Tax, 0 = Non Tax',
  `tax_type` smallint(6) NOT NULL COMMENT '1 = Fix, 2 = Variable',
  PRIMARY KEY (`ma_id`),
  KEY `mt_allowance_FK` (`company_id`),
  CONSTRAINT `mt_allowance_FK` FOREIGN KEY (`company_id`) REFERENCES `mt_company` (`company_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Dynamic allowance table';

/*Data for the table `mt_allowance` */

/*Table structure for table `mt_bank` */

DROP TABLE IF EXISTS `mt_bank`;

CREATE TABLE `mt_bank` (
  `bank_id` int(5) NOT NULL,
  `bank_code` varchar(8) DEFAULT NULL,
  `bank_name` varchar(35) DEFAULT NULL,
  `is_local` tinyint(1) DEFAULT 1,
  `is_active` tinyint(1) DEFAULT 1,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`bank_id`),
  UNIQUE KEY `bank_code` (`bank_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_bank` */

/*Table structure for table `mt_biodata_01` */

DROP TABLE IF EXISTS `mt_biodata_01`;

CREATE TABLE `mt_biodata_01` (
  `biodata_id` int(11) NOT NULL,
  `dept_id` int(10) DEFAULT NULL,
  `full_name` varchar(35) DEFAULT NULL,
  `first_name` varchar(15) DEFAULT NULL,
  `middle_name` varchar(15) DEFAULT NULL,
  `last_name` varchar(15) DEFAULT NULL,
  `place_of_birth` varchar(30) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `ethnic` varchar(30) DEFAULT NULL,
  `nationality` varchar(30) DEFAULT NULL,
  `company` varchar(15) DEFAULT NULL,
  `dept` varchar(50) DEFAULT NULL,
  `emp_position` varchar(50) DEFAULT NULL,
  `passport_no` varchar(30) DEFAULT NULL,
  `placement` varchar(15) DEFAULT NULL,
  `approve_level` varchar(15) DEFAULT NULL,
  `internal_id` varchar(11) DEFAULT NULL,
  `position_level` smallint(6) DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `id_card_no` varchar(30) DEFAULT NULL,
  `id_card_address` varchar(150) DEFAULT NULL,
  `current_address` varchar(150) DEFAULT NULL,
  `residence_status` varchar(30) DEFAULT NULL,
  `religion` varchar(15) DEFAULT NULL,
  `driving_license` varchar(10) DEFAULT NULL,
  `marital_status` varchar(15) DEFAULT NULL,
  `emp_height` smallint(6) DEFAULT 0,
  `emp_weight` smallint(6) DEFAULT 0,
  `blood_type` varchar(5) DEFAULT NULL,
  `email_address` varchar(40) DEFAULT NULL,
  `telp_no` varchar(15) DEFAULT NULL,
  `cell_no` varchar(15) DEFAULT NULL,
  `is_glasses` smallint(6) DEFAULT 0,
  `emp_status` varchar(25) DEFAULT NULL,
  `tax_no` varchar(20) DEFAULT NULL,
  `bpjs_no` varchar(20) DEFAULT NULL,
  `bpjs_tk_no` varchar(20) DEFAULT NULL,
  `is_active` smallint(6) DEFAULT 1,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`biodata_id`),
  KEY `mt_biodata_01_FK` (`dept_id`),
  CONSTRAINT `mt_biodata_01_FK` FOREIGN KEY (`dept_id`) REFERENCES `mt_dept` (`dept_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_biodata_01` */

/*Table structure for table `mt_biodata_02` */

DROP TABLE IF EXISTS `mt_biodata_02`;

CREATE TABLE `mt_biodata_02` (
  `education_id` int(11) NOT NULL,
  `biodata_id` int(11) DEFAULT NULL,
  `school_name` varchar(50) DEFAULT NULL,
  `education_year` smallint(4) DEFAULT NULL,
  `major` varchar(35) DEFAULT NULL,
  `city_name` varchar(30) DEFAULT NULL,
  `is_certified` tinyint(1) DEFAULT NULL,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`education_id`),
  KEY `biodata_id` (`biodata_id`),
  CONSTRAINT `mt_biodata_02_FK` FOREIGN KEY (`biodata_id`) REFERENCES `mt_biodata_01` (`biodata_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_biodata_02` */

/*Table structure for table `mt_biodata_03` */

DROP TABLE IF EXISTS `mt_biodata_03`;

CREATE TABLE `mt_biodata_03` (
  `experience_id` int(11) NOT NULL,
  `biodata_id` int(11) DEFAULT NULL,
  `company_name` varchar(60) DEFAULT NULL,
  `work_year` smallint(4) DEFAULT NULL,
  `work_period` smallint(1) DEFAULT NULL,
  `job_position` varchar(100) DEFAULT NULL,
  `job_desc` varchar(150) DEFAULT NULL,
  `moving_reason` varchar(180) DEFAULT NULL,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`experience_id`),
  KEY `biodata_id` (`biodata_id`),
  CONSTRAINT `mt_biodata_03_FK` FOREIGN KEY (`biodata_id`) REFERENCES `mt_biodata_01` (`biodata_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_biodata_03` */

/*Table structure for table `mt_biodata_04` */

DROP TABLE IF EXISTS `mt_biodata_04`;

CREATE TABLE `mt_biodata_04` (
  `skill_id` int(11) NOT NULL,
  `biodata_id` int(11) DEFAULT NULL,
  `skill` varchar(50) DEFAULT NULL,
  `remarks` varchar(200) DEFAULT NULL,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`skill_id`),
  KEY `biodata_id` (`biodata_id`),
  CONSTRAINT `mt_biodata_04_FK` FOREIGN KEY (`biodata_id`) REFERENCES `mt_biodata_01` (`biodata_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_biodata_04` */

/*Table structure for table `mt_class` */

DROP TABLE IF EXISTS `mt_class`;

CREATE TABLE `mt_class` (
  `class_id` int(5) NOT NULL COMMENT 'Table Id',
  `class_code` varchar(10) DEFAULT NULL COMMENT 'Class Code',
  `class_name` varchar(25) DEFAULT NULL COMMENT 'Class Name',
  `class_base` varchar(15) DEFAULT NULL COMMENT 'Staff/ Hourly',
  `base_wage` decimal(12,2) DEFAULT 0.00 COMMENT 'Base Wage',
  `prod_bonus` decimal(12,2) DEFAULT 0.00,
  `var_bonus` decimal(12,2) DEFAULT 0.00,
  `pension` decimal(10,2) DEFAULT 0.00,
  `vacation_sup` decimal(10,2) DEFAULT 0.00,
  `class_category` varchar(10) DEFAULT NULL COMMENT 'Category(CAD, CAD-H,USD,USD-H,PHP-H)',
  `currency` varchar(10) DEFAULT NULL COMMENT 'Currency(USD,AUD,CAD)',
  `is_active` tinyint(1) DEFAULT 1,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_class` */

/*Table structure for table `mt_company` */

DROP TABLE IF EXISTS `mt_company`;

CREATE TABLE `mt_company` (
  `company_id` int(10) NOT NULL AUTO_INCREMENT,
  `company_code` varchar(5) DEFAULT NULL,
  `company_name` varchar(50) DEFAULT NULL,
  `logo` varchar(70) DEFAULT NULL,
  `tax_no` varchar(20) DEFAULT NULL,
  `tax_name` varchar(25) DEFAULT NULL,
  `address` varchar(60) DEFAULT NULL,
  `phone_1` varchar(20) DEFAULT NULL,
  `phone_2` varchar(20) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT 1,
  `pic_input` int(11) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` int(11) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `mt_company` */

insert  into `mt_company`(`company_id`,`company_code`,`company_name`,`logo`,`tax_no`,`tax_name`,`address`,`phone_1`,`phone_2`,`is_active`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(1,'MKI','Maurice Karbotech Indonesia',NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL),
(3,'SRU','PT. SINAR RODA UTAMA','LOGO','1234567890123','PT. Sinar roda utama','Jalan Panjang','123456','654321',1,19,'2022-02-10 21:19:09',0,'1700-01-01 00:00:00');

/*Table structure for table `mt_contract` */

DROP TABLE IF EXISTS `mt_contract`;

CREATE TABLE `mt_contract` (
  `contract_id` int(11) NOT NULL,
  `biodata_id` varchar(15) DEFAULT NULL,
  `badge_no` varchar(20) DEFAULT NULL,
  `contract_no` varchar(20) DEFAULT NULL,
  `dept` varchar(30) DEFAULT NULL,
  `job_position` varchar(30) DEFAULT NULL,
  `contract_start` date NOT NULL,
  `contract_end` date NOT NULL,
  `contract_counter` int(4) NOT NULL DEFAULT 1,
  `is_close` tinyint(1) DEFAULT 0 COMMENT 'Reset Masa Kerja',
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `remarks` varchar(25) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_input` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`contract_id`),
  KEY `biodata_id` (`biodata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_contract` */

/*Table structure for table `mt_currency_rate` */

DROP TABLE IF EXISTS `mt_currency_rate`;

CREATE TABLE `mt_currency_rate` (
  `cr_id` int(5) NOT NULL,
  `cr_code` varchar(5) DEFAULT NULL,
  `cr_country` varchar(20) DEFAULT NULL,
  `rate_value` decimal(11,2) DEFAULT 0.00,
  `is_active` tinyint(1) DEFAULT 1,
  `pic_data` varchar(15) DEFAULT NULL,
  `data_time` datetime DEFAULT NULL,
  PRIMARY KEY (`cr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_currency_rate` */

insert  into `mt_currency_rate`(`cr_id`,`cr_code`,`cr_country`,`rate_value`,`is_active`,`pic_data`,`data_time`) values 
(220200001,'Dol','Dollar',14250.00,NULL,'admin','2022-02-10 21:35:17');

/*Table structure for table `mt_dept` */

DROP TABLE IF EXISTS `mt_dept`;

CREATE TABLE `mt_dept` (
  `dept_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Table Id',
  `dept_code` varchar(10) DEFAULT NULL COMMENT 'Class Code',
  `dept_name` varchar(25) DEFAULT NULL COMMENT 'Class Name',
  `is_active` tinyint(1) DEFAULT 1,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

/*Data for the table `mt_dept` */

insert  into `mt_dept`(`dept_id`,`dept_code`,`dept_name`,`is_active`,`pic_input`,`input_time`,`pic_edit`,`edit_time`) values 
(20,'ITS','Information Technology Su',1,'admin','2022-02-10 21:19:24',NULL,NULL),
(21,'FIN','Finance',1,'admin','2022-02-10 21:20:45','admin','2022-02-10 21:25:26'),
(24,'OPS','Operations',1,'admin','2022-02-10 21:21:58',NULL,NULL),
(25,'HRD','Human Resource Department',1,'admin','2022-02-10 21:22:50',NULL,NULL);

/*Table structure for table `mt_exchange_rate` */

DROP TABLE IF EXISTS `mt_exchange_rate`;

CREATE TABLE `mt_exchange_rate` (
  `rate_code` int(5) NOT NULL,
  `rate_val` decimal(10,2) DEFAULT 0.00,
  `country_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`rate_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_exchange_rate` */

/*Table structure for table `mt_global_config` */

DROP TABLE IF EXISTS `mt_global_config`;

CREATE TABLE `mt_global_config` (
  `gc_code` varchar(3) NOT NULL,
  `gc_value` varchar(15) DEFAULT NULL,
  `gc_name` varchar(30) DEFAULT NULL,
  `gc_remarks` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`gc_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_global_config` */

insert  into `mt_global_config`(`gc_code`,`gc_value`,`gc_name`,`gc_remarks`) values 
('BPH','12000000','BPJS Health','BPJS Health Total'),
('WLV','130','Warning Letter','Warning Letter Value');

/*Table structure for table `mt_overtime` */

DROP TABLE IF EXISTS `mt_overtime`;

CREATE TABLE `mt_overtime` (
  `ot_id` int(10) NOT NULL AUTO_INCREMENT,
  `company_id` int(10) NOT NULL,
  `1st_ot` float(3,1) NOT NULL DEFAULT 0.0 COMMENT '<= 1st_ot value * 1st_multiplier\nex: 5(ot) * 1.5(multiplier)',
  `2st_ot` float(3,1) NOT NULL DEFAULT 0.0 COMMENT '> 1st_ot < 3st_ot * 2st_multiplier\nex: 1(ot) * 2(multiplier)',
  `3st_ot` float(3,1) NOT NULL DEFAULT 0.0,
  `4st_ot` float(3,1) NOT NULL DEFAULT 0.0 COMMENT '> 3st_ot  * 4st_multiplier\nex: 1(ot) * 4(multiplier)',
  `1st_multiplier` float(3,1) NOT NULL DEFAULT 0.0,
  `2st_multiplier` float(3,1) NOT NULL DEFAULT 0.0,
  `3st_multiplier` float(3,1) NOT NULL DEFAULT 0.0,
  `4st_multiplier` float(3,1) NOT NULL DEFAULT 0.0,
  PRIMARY KEY (`ot_id`),
  KEY `mt_overtime_FK` (`company_id`),
  CONSTRAINT `mt_overtime_FK` FOREIGN KEY (`company_id`) REFERENCES `mt_company` (`company_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Overtime formulations table';

/*Data for the table `mt_overtime` */

/*Table structure for table `mt_payroll_config` */

DROP TABLE IF EXISTS `mt_payroll_config`;

CREATE TABLE `mt_payroll_config` (
  `pc_id` int(3) NOT NULL COMMENT 'Id Tabel',
  `company_name` varchar(25) NOT NULL COMMENT 'Nama Klien',
  `health_bpjs` decimal(5,2) DEFAULT 0.00 COMMENT 'Iuran BPJS Kesehatan 4% Dari Basic, max 8 Juta)',
  `max_health_bpjs` decimal(10,2) DEFAULT 0.00 COMMENT 'Maksimal Iuran BPJS',
  `max_emp_bpjs` decimal(10,2) DEFAULT 0.00 COMMENT 'Maksimal Iuran BPJS (Karyawan)',
  `max_jp` decimal(10,2) DEFAULT 0.00 COMMENT 'Maksimal Nilai Jaminan Pensiun Perusahaan',
  `max_emp_jp` decimal(10,2) DEFAULT 0.00 COMMENT 'Maksimal Nilai Jaminan Pensiun Karyawan',
  `jkk_jkm` decimal(5,2) DEFAULT 0.00 COMMENT 'Jaminan Keselamatan Kerja & Kematian (2.04 x Basic Salary)',
  `jp` decimal(5,2) DEFAULT 0.00 COMMENT 'Jaminan Pensiun Tanggungan Perusahaan',
  `jht` decimal(5,2) DEFAULT 0.00 COMMENT 'Jaminan Hari Tua',
  `emp_jht` decimal(5,2) DEFAULT 0.00 COMMENT 'JHT Tanggungan Karyawan',
  `emp_health_bpjs` decimal(5,2) DEFAULT 0.00 COMMENT 'BPJS Kesehatan Tanggungan Karyawan',
  `emp_jp` decimal(5,2) DEFAULT 0.00 COMMENT 'Jaminan Pensiun Tanggungan Karyawan',
  `ptkp` decimal(11,2) DEFAULT 0.00 COMMENT 'Penghasilan Tidak Kena Pajak',
  `ptkp_dependent` decimal(11,2) DEFAULT 0.00 COMMENT 'PTKP Tanggungan',
  `npwp_charge` decimal(3,1) DEFAULT 0.0 COMMENT 'Potongan Tambahan Tanpa NPWP (Persen)',
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT '0000-00-00 00:00:00',
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`pc_id`),
  UNIQUE KEY `client_name` (`company_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_payroll_config` */

insert  into `mt_payroll_config`(`pc_id`,`company_name`,`health_bpjs`,`max_health_bpjs`,`max_emp_bpjs`,`max_jp`,`max_emp_jp`,`jkk_jkm`,`jp`,`jht`,`emp_jht`,`emp_health_bpjs`,`emp_jp`,`ptkp`,`ptkp_dependent`,`npwp_charge`,`pic_edit`,`edit_time`,`pic_input`,`input_time`) values 
(1,'Redpath_Expat',4.00,480000.00,120000.00,160980.00,89397.00,2.04,2.00,3.70,2.00,1.00,1.00,54000000.00,4500000.00,20.0,'dian.p','0000-00-00 00:00:00',NULL,'0000-00-00 00:00:00');

/*Table structure for table `mt_process_closing` */

DROP TABLE IF EXISTS `mt_process_closing`;

CREATE TABLE `mt_process_closing` (
  `closing_id` int(10) NOT NULL AUTO_INCREMENT,
  `close_year` smallint(4) DEFAULT NULL,
  `close_month` varchar(2) DEFAULT NULL,
  `period_no` smallint(1) DEFAULT 1,
  `is_active` tinyint(1) DEFAULT 1,
  PRIMARY KEY (`closing_id`),
  KEY `client_name` (`close_year`,`close_month`)
) ENGINE=InnoDB AUTO_INCREMENT=210300002 DEFAULT CHARSET=latin1;

/*Data for the table `mt_process_closing` */

/*Table structure for table `mt_pvb_dept` */

DROP TABLE IF EXISTS `mt_pvb_dept`;

CREATE TABLE `mt_pvb_dept` (
  `pvb_id` int(10) NOT NULL AUTO_INCREMENT,
  `dept_code` int(10) DEFAULT NULL,
  `remarks` varchar(150) DEFAULT NULL,
  `percent` decimal(4,1) DEFAULT 100.0,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  PRIMARY KEY (`pvb_id`)
) ENGINE=InnoDB AUTO_INCREMENT=210500005 DEFAULT CHARSET=latin1;

/*Data for the table `mt_pvb_dept` */

/*Table structure for table `mt_salary` */

DROP TABLE IF EXISTS `mt_salary`;

CREATE TABLE `mt_salary` (
  `salary_id` int(11) DEFAULT NULL,
  `biodata_id` int(11) DEFAULT NULL,
  `company_id` int(10) NOT NULL,
  `badge_no` varchar(20) DEFAULT NULL,
  `class_id` int(5) DEFAULT NULL,
  `local_bank_id` varchar(8) DEFAULT NULL,
  `foreign_bank_id` varchar(8) DEFAULT NULL,
  `is_emp_trv_tax` tinyint(4) DEFAULT 1,
  `is_active` tinyint(1) DEFAULT 1,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  UNIQUE KEY `badge_no` (`badge_no`),
  KEY `biodata_id` (`biodata_id`),
  KEY `class_id` (`class_id`),
  KEY `mt_salary_FK_1` (`company_id`),
  CONSTRAINT `mt_salary_FK` FOREIGN KEY (`class_id`) REFERENCES `mt_class` (`class_id`) ON UPDATE CASCADE,
  CONSTRAINT `mt_salary_FK_1` FOREIGN KEY (`company_id`) REFERENCES `mt_company` (`company_id`) ON UPDATE CASCADE,
  CONSTRAINT `mt_salary_FK_2` FOREIGN KEY (`biodata_id`) REFERENCES `mt_biodata_01` (`biodata_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_salary` */

/*Table structure for table `mt_salary_ori` */

DROP TABLE IF EXISTS `mt_salary_ori`;

CREATE TABLE `mt_salary_ori` (
  `salary_id` varchar(15) DEFAULT NULL,
  `biodata_id` varchar(15) DEFAULT NULL,
  `full_name` varchar(35) DEFAULT NULL,
  `class_id` varchar(15) DEFAULT NULL,
  `currency` varchar(10) DEFAULT NULL COMMENT 'Currency(USD,AUD,CAD)',
  `base_wage` decimal(12,2) DEFAULT 0.00,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_salary_ori` */

/*Table structure for table `mt_statutory_date` */

DROP TABLE IF EXISTS `mt_statutory_date`;

CREATE TABLE `mt_statutory_date` (
  `msd_id` int(10) NOT NULL,
  `msd_date` varchar(2) DEFAULT NULL,
  `msd_month` varchar(2) DEFAULT NULL,
  `remarks` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`msd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `mt_statutory_date` */

/*Table structure for table `mt_tax` */

DROP TABLE IF EXISTS `mt_tax`;

CREATE TABLE `mt_tax` (
  `mt_id` int(8) NOT NULL AUTO_INCREMENT,
  `company_id` int(10) NOT NULL,
  `tax_level` smallint(6) NOT NULL DEFAULT 0 COMMENT '<= 1st_ot value * 1st_multiplier\nex: 5(ot) * 1.5(multiplier)',
  `tax_percent` float(3,1) NOT NULL DEFAULT 0.0 COMMENT '> 1st_ot < 3st_ot * 2st_multiplier\nex: 1(ot) * 2(multiplier)',
  `tax_max_income` decimal(12,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`mt_id`),
  KEY `mt_overtime_FK` (`company_id`) USING BTREE,
  CONSTRAINT `mt_overtime_FK_copy` FOREIGN KEY (`company_id`) REFERENCES `mt_company` (`company_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='Overtime formulations table';

/*Data for the table `mt_tax` */

insert  into `mt_tax`(`mt_id`,`company_id`,`tax_level`,`tax_percent`,`tax_max_income`) values 
(1,1,1,5.0,60000000.00),
(2,1,2,15.0,200000000.00),
(3,1,3,25.0,250000000.00),
(4,1,4,30.0,500000000.00),
(5,1,5,35.0,0.00);

/*Table structure for table `tr_manual_value` */

DROP TABLE IF EXISTS `tr_manual_value`;

CREATE TABLE `tr_manual_value` (
  `mv_id` varchar(12) NOT NULL COMMENT 'Id Tabel',
  `biodata_id` varchar(15) DEFAULT NULL COMMENT 'PTFI Id',
  `full_name` varchar(35) DEFAULT NULL,
  `mv_code` varchar(2) DEFAULT NULL,
  `dept` varchar(30) DEFAULT NULL,
  `mv_year` int(4) DEFAULT NULL COMMENT 'Tahun',
  `mv_month` varchar(2) DEFAULT NULL COMMENT 'Bulan',
  `mv_period` smallint(1) DEFAULT NULL COMMENT 'Periode Gaji (1,2)',
  `category` varchar(15) NOT NULL COMMENT 'Jumbo Bonus, Deduction, Expenses, Warning Letter,..',
  `mv_val` decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT 'Nilai',
  `remarks` varchar(180) DEFAULT NULL,
  `pic_process` varchar(15) DEFAULT NULL,
  `process_time` datetime DEFAULT NULL,
  PRIMARY KEY (`mv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tr_manual_value` */

/*Table structure for table `tr_slip` */

DROP TABLE IF EXISTS `tr_slip`;

CREATE TABLE `tr_slip` (
  `slip_id` int(11) NOT NULL COMMENT 'Id Tabel',
  `biodata_id` int(11) NOT NULL COMMENT 'Referensi mst_bio_rec',
  `ts_id` int(10) DEFAULT NULL COMMENT 'Referensi tr_timesheet',
  `year_period` int(4) DEFAULT NULL COMMENT 'Periode Tahun Gaji',
  `month_period` varchar(2) DEFAULT NULL COMMENT 'Periode Bulan Gaji',
  `slip_period` smallint(1) DEFAULT NULL COMMENT 'Periode Gaji (1,2)',
  `full_name` varchar(30) DEFAULT NULL COMMENT 'Nama Karyawan',
  `dept` varchar(25) DEFAULT NULL COMMENT 'Dept Karyawan',
  `position` varchar(25) DEFAULT NULL COMMENT 'Posisi',
  `marital_status` varchar(15) DEFAULT NULL COMMENT 'Status Pernikahan',
  `class_id` varchar(25) NOT NULL COMMENT 'Golongan Salary',
  `class_base` varchar(15) DEFAULT NULL COMMENT 'Staff/Hourly',
  `work_total` decimal(5,2) DEFAULT 0.00 COMMENT 'Jumlah Hari/ Jam Kerja',
  `currency` varchar(4) DEFAULT NULL COMMENT 'Nama Mata Uang',
  `currency_rate` decimal(7,2) DEFAULT 0.00 COMMENT 'Nilai Tukar Rupiah',
  `base_wage` decimal(11,2) DEFAULT 0.00 COMMENT 'Gaji Pokok',
  `wages_in` decimal(11,2) DEFAULT 0.00 COMMENT 'Gaji Pokok Prorata',
  `fixed_bonus` decimal(11,2) DEFAULT 0.00,
  `variable_bonus` decimal(11,2) DEFAULT 0.00,
  `jumbo_bonus` decimal(11,2) DEFAULT 0.00,
  `salvac_bonus` decimal(11,2) DEFAULT 0.00,
  `statutory` decimal(11,2) DEFAULT 0.00,
  `travel` decimal(11,2) DEFAULT 0.00,
  `pvb_val` decimal(11,2) DEFAULT 0.00 COMMENT 'Dari mt_class',
  `pvb_percent` decimal(11,2) DEFAULT 0.00 COMMENT 'Nilai Upload',
  `wl_qty` smallint(1) DEFAULT 1 COMMENT 'Jumlah Warning Letter',
  `wl_value` decimal(11,2) DEFAULT 0.00 COMMENT 'Nilai Warning Letter',
  `allowance_01` decimal(11,2) DEFAULT 0.00,
  `allowance_02` decimal(11,2) DEFAULT 0.00 COMMENT 'Bonus Shift',
  `allowance_03` decimal(11,2) DEFAULT 0.00 COMMENT 'Bonus Lembur',
  `allowance_04` decimal(11,2) DEFAULT 0.00 COMMENT 'Insentif Bonus',
  `allowance_05` decimal(11,2) DEFAULT 0.00 COMMENT 'Bonus Produksi',
  `adjustment` decimal(11,2) DEFAULT 0.00 COMMENT 'Nilai Berupa +/-',
  `thr` decimal(11,2) DEFAULT 0.00 COMMENT 'Tunjangan Hari Raya',
  `contract_bonus` decimal(11,2) DEFAULT 0.00 COMMENT 'Tunjangan Shift Malam',
  `bpjs` decimal(11,2) DEFAULT 0.00 COMMENT 'Iuran BPJS Kesehatan (Dalam persentase)',
  `jk` decimal(11,2) DEFAULT 0.00,
  `jkk` decimal(11,2) DEFAULT 0.00 COMMENT 'Jaminan Keselamatan Kerja & Kematian (Persentase)',
  `jkm` decimal(11,2) DEFAULT 0.00,
  `jp` decimal(11,2) DEFAULT 0.00 COMMENT 'Jaminan Pensiun Tanggungan Perusahaan',
  `jht` decimal(11,2) DEFAULT 0.00 COMMENT 'Jaminan Hari Tua',
  `emp_bpjs` decimal(11,2) DEFAULT 0.00 COMMENT 'Nilai BPJS Tanggungan Karyawan',
  `emp_jp` decimal(11,2) DEFAULT 0.00 COMMENT 'Jaminan Pensiun Tanggungan Karyawan',
  `emp_jht` decimal(11,2) DEFAULT 0.00 COMMENT 'Nilai JHT Tanggungan Karyawan',
  `unpaid_count` int(2) DEFAULT 0 COMMENT 'Jumlah Hari Tidak Dibayar',
  `unpaid_total` decimal(11,2) DEFAULT 0.00 COMMENT 'Total Tidak Dibayar',
  `non_tax_allowance` decimal(11,2) DEFAULT 0.00 COMMENT 'Nilai Tidak Potong Pajak',
  `ptkp_total` decimal(11,2) DEFAULT 0.00 COMMENT 'Nilai Total PTKP',
  `irregular_tax` decimal(15,2) DEFAULT 0.00 COMMENT 'Penyesuaian Keluar',
  `regular_tax` decimal(15,2) DEFAULT 0.00 COMMENT 'Nilai Pajak',
  `salary_status` varchar(10) DEFAULT NULL COMMENT 'Status Salary',
  `status_remarks` varchar(200) DEFAULT NULL COMMENT 'Keterangan Status',
  `gross_earn` decimal(10,2) DEFAULT 0.00,
  `gross_earn_idr` decimal(12,2) DEFAULT 0.00,
  `pic_edit` varchar(15) DEFAULT NULL,
  `edit_time` datetime DEFAULT NULL,
  `pic_input` varchar(15) DEFAULT NULL,
  `input_time` datetime DEFAULT NULL,
  PRIMARY KEY (`slip_id`),
  UNIQUE KEY `bio_rec_id` (`biodata_id`,`year_period`,`month_period`,`slip_period`),
  CONSTRAINT `tr_slip_FK` FOREIGN KEY (`biodata_id`) REFERENCES `mt_biodata_01` (`biodata_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tr_slip` */

/*Table structure for table `tr_slip_allowance` */

DROP TABLE IF EXISTS `tr_slip_allowance`;

CREATE TABLE `tr_slip_allowance` (
  `sa_id` int(11) NOT NULL,
  `slip_id` int(11) NOT NULL,
  `sa_name` varchar(20) NOT NULL,
  `sa_value` decimal(11,2) NOT NULL DEFAULT 0.00,
  `tax_type` smallint(6) NOT NULL,
  `pic_data` varchar(100) NOT NULL,
  `data_date` datetime NOT NULL,
  KEY `slip_allowance_FK` (`slip_id`),
  CONSTRAINT `slip_allowance_FK` FOREIGN KEY (`slip_id`) REFERENCES `tr_slip` (`slip_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tr_slip_allowance` */

/*Table structure for table `tr_sum_gross_earn` */

DROP TABLE IF EXISTS `tr_sum_gross_earn`;

CREATE TABLE `tr_sum_gross_earn` (
  `biodata_id` varchar(15) DEFAULT NULL,
  `year_period` varchar(4) DEFAULT NULL,
  `period_no` smallint(1) DEFAULT 1,
  `sum_earn` decimal(10,2) DEFAULT 0.00,
  `sum_earn_idr` decimal(15,2) DEFAULT 0.00,
  `pic_process` varchar(15) DEFAULT NULL,
  `process_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tr_sum_gross_earn` */

/*Table structure for table `tr_timesheet` */

DROP TABLE IF EXISTS `tr_timesheet`;

CREATE TABLE `tr_timesheet` (
  `ts_id` int(10) NOT NULL COMMENT 'Id Tabel',
  `biodata_id` int(11) DEFAULT NULL,
  `badge_no` varchar(15) DEFAULT NULL,
  `full_name` varchar(35) DEFAULT NULL,
  `payroll_base` varchar(6) DEFAULT NULL COMMENT 'Staff/ Hourly',
  `client_name` varchar(25) DEFAULT NULL,
  `dept` varchar(30) DEFAULT NULL,
  `month_process` varchar(2) DEFAULT NULL,
  `year_process` int(4) DEFAULT NULL,
  `period_no` smallint(1) DEFAULT 1,
  `pass_day` int(2) DEFAULT 1,
  `d01` varchar(6) DEFAULT 'U',
  `d02` varchar(6) DEFAULT 'U',
  `d03` varchar(6) DEFAULT 'U',
  `d04` varchar(6) DEFAULT 'U',
  `d05` varchar(6) DEFAULT 'U',
  `d06` varchar(6) DEFAULT 'U',
  `d07` varchar(6) DEFAULT 'U',
  `d08` varchar(6) DEFAULT 'U',
  `d09` varchar(6) DEFAULT 'U',
  `d10` varchar(6) DEFAULT 'U',
  `d11` varchar(6) DEFAULT 'U',
  `d12` varchar(6) DEFAULT 'U',
  `d13` varchar(6) DEFAULT 'U',
  `d14` varchar(6) DEFAULT 'U',
  `d15` varchar(6) DEFAULT 'U',
  `d16` varchar(6) DEFAULT 'U',
  `d17` varchar(6) DEFAULT 'U',
  `d18` varchar(6) DEFAULT 'U',
  `d19` varchar(6) DEFAULT 'U',
  `d20` varchar(6) DEFAULT 'U',
  `d21` varchar(6) DEFAULT 'U',
  `d22` varchar(6) DEFAULT 'U',
  `d23` varchar(6) DEFAULT 'U',
  `d24` varchar(6) DEFAULT 'U',
  `d25` varchar(6) DEFAULT 'U',
  `d26` varchar(6) DEFAULT 'U',
  `d27` varchar(6) DEFAULT 'U',
  `d28` varchar(6) DEFAULT 'U',
  `d29` varchar(6) DEFAULT 'U',
  `d30` varchar(6) DEFAULT 'U',
  `d31` varchar(6) DEFAULT 'U',
  `is_active` tinyint(1) DEFAULT 1,
  `pic_process` varchar(15) DEFAULT NULL,
  `process_time` datetime DEFAULT NULL,
  PRIMARY KEY (`ts_id`),
  UNIQUE KEY `nie_year_month` (`badge_no`,`month_process`,`year_process`,`client_name`,`period_no`),
  KEY `nie` (`badge_no`),
  KEY `bio_rec_id` (`biodata_id`),
  CONSTRAINT `tr_timesheet_FK` FOREIGN KEY (`biodata_id`) REFERENCES `mt_biodata_01` (`biodata_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tr_timesheet` */

/*Table structure for table `trn_user_menu` */

DROP TABLE IF EXISTS `trn_user_menu`;

CREATE TABLE `trn_user_menu` (
  `user_id` varchar(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  UNIQUE KEY `user_id` (`user_id`,`menu_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_user_menu` */

insert  into `trn_user_menu`(`user_id`,`menu_id`) values 
(NULL,NULL),
(NULL,NULL),
('19',1),
('19',2),
('19',3),
('19',4),
('19',5),
('19',8),
('19',9),
('19',10),
('19',12),
('19',13),
('19',14),
('19',15),
('19',17),
('19',18),
('19',19),
('19',20),
('20',4),
('20',5),
('20',8),
('20',9),
('20',10),
('20',12),
('20',13),
('20',14),
('20',15),
('20',17),
('20',18),
('20',19),
('21',4),
('21',5),
('21',8),
('21',9),
('21',10),
('21',12),
('21',13),
('21',14),
('21',15),
('21',17),
('21',18),
('21',19),
('tmk',1),
('tmk',2),
('tmk',3),
('tmk',4),
('tmk',5),
('tmk',6),
('tmk',7),
('tmk',14);

/*Table structure for table `trn_user_menu_copy` */

DROP TABLE IF EXISTS `trn_user_menu_copy`;

CREATE TABLE `trn_user_menu_copy` (
  `user_id` varchar(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  UNIQUE KEY `user_id` (`user_id`,`menu_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `trn_user_menu_copy` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
