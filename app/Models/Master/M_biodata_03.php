<?php namespace App\Models\Master; 
/*********************************************************************
 *  Created By       :  Generator Version 1.0.23                     *
 *  Created Date     :  Nov 30, 2020                                 *
 *  Description      : All code generated by model generator         *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
use CodeIgniter\Model;

class M_biodata_03 extends Model
{
     /* START PRIVATE VARIABLES */
     protected $DBGroup = 'hris';
     protected $table = 'mt_biodata_03';
     protected $primaryKey = 'experience_id';
     protected $returnType = 'array';
     protected $db = null;

     protected $experienceId;
     protected $biodataId;
     protected $companyName;
     protected $workYear;
     protected $workPeriod;
     protected $jobPosition;
     protected $jobDesc;
     protected $movingReason;
     protected $picInput;
     protected $inputTime;
     protected $picEdit;
     protected $editTime;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */ 
     public function __construct()
     {
          parent::__construct();
          $this->db = \Config\Database::connect($this->DBGroup, false);
          $this->experienceId = '';
          $this->biodataId = '';
          $this->companyName = '';
          $this->workYear = 0;
          $this->workPeriod = 0;
          $this->jobPosition = '';
          $this->jobDesc = '';
          $this->movingReason = '';
          $this->picInput = '';
          $this->inputTime = '1700-01-01 00:00:00';
          $this->picEdit = '';
          $this->editTime = '1700-01-01 00:00:00';
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setExperienceId($aExperienceId)
     {
          $this->experienceId = $aExperienceId;
     }
     public function getExperienceId()
     {
          return $this->experienceId;
     }
     public function setBiodataId($aBiodataId)
     {
          $this->biodataId = $aBiodataId;
     }
     public function getBiodataId()
     {
          return $this->biodataId;
     }
     public function setCompanyName($aCompanyName)
     {
          $this->companyName = $aCompanyName;
     }
     public function getCompanyName()
     {
          return $this->companyName;
     }
     public function setWorkYear($aWorkYear)
     {
          $this->workYear = $aWorkYear;
     }
     public function getWorkYear()
     {
          return $this->workYear;
     }
     public function setWorkPeriod($aWorkPeriod)
     {
          $this->workPeriod = $aWorkPeriod;
     }
     public function getWorkPeriod()
     {
          return $this->workPeriod;
     }
     public function setJobPosition($aJobPosition)
     {
          $this->jobPosition = $aJobPosition;
     }
     public function getJobPosition()
     {
          return $this->jobPosition;
     }
     public function setJobDesc($aJobDesc)
     {
          $this->jobDesc = $aJobDesc;
     }
     public function getJobDesc()
     {
          return $this->jobDesc;
     }
     public function setMovingReason($aMovingReason)
     {
          $this->movingReason = $aMovingReason;
     }
     public function getMovingReason()
     {
          return $this->movingReason;
     }
     public function setPicInput($aPicInput)
     {
          $this->picInput = $aPicInput;
     }
     public function getPicInput()
     {
          return $this->picInput;
     }
     public function setInputTime($aInputTime)
     {
          $this->inputTime = $aInputTime;
     }
     public function getInputTime()
     {
          return $this->inputTime;
     }
     public function setPicEdit($aPicEdit)
     {
          $this->picEdit = $aPicEdit;
     }
     public function getPicEdit()
     {
          return $this->picEdit;
     }
     public function setEditTime($aEditTime)
     {
          $this->editTime = $aEditTime;
     }
     public function getEditTime()
     {
          return $this->editTime;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function ins()
     {
          if($this->experienceId =="" || $this->experienceId == NULL) 
          {
               $this->experienceId = ''; 
          }
          if($this->biodataId =="" || $this->biodataId == NULL) 
          {
               $this->biodataId = ''; 
          }
          if($this->companyName =="" || $this->companyName == NULL) 
          {
               $this->companyName = ''; 
          }
          if($this->workYear =="" || $this->workYear == NULL) 
          {
               $this->workYear = 0; 
          }
          if($this->workPeriod =="" || $this->workPeriod == NULL) 
          {
               $this->workPeriod = 0; 
          }
          if($this->jobPosition =="" || $this->jobPosition == NULL) 
          {
               $this->jobPosition = ''; 
          }
          if($this->jobDesc =="" || $this->jobDesc == NULL) 
          {
               $this->jobDesc = ''; 
          }
          if($this->movingReason =="" || $this->movingReason == NULL) 
          {
               $this->movingReason = ''; 
          }
          if($this->picInput =="" || $this->picInput == NULL) 
          {
               $this->picInput = ''; 
          }
          if($this->inputTime =="" || $this->inputTime == NULL) 
          {
               $this->inputTime = '1700-01-01 00:00:00'; 
          }
          if($this->picEdit =="" || $this->picEdit == NULL) 
          {
               $this->picEdit = ''; 
          }
          if($this->editTime =="" || $this->editTime == NULL) 
          {
               $this->editTime = '1700-01-01 00:00:00'; 
          }

          $stQuery  = "INSERT INTO ".$this->table." ";
          $stQuery .= "( ";
          $stQuery .=   "experience_id,"; 
          $stQuery .=   "biodata_id,"; 
          $stQuery .=   "company_name,"; 
          $stQuery .=   "work_year,"; 
          $stQuery .=   "work_period,"; 
          $stQuery .=   "job_position,"; 
          $stQuery .=   "job_desc,"; 
          $stQuery .=   "moving_reason,"; 
          $stQuery .=   "pic_input,"; 
          $stQuery .=   "input_time,"; 
          $stQuery .=   "pic_edit,"; 
          $stQuery .=   "edit_time"; 
          $stQuery .= ") "; 
          $stQuery .= "VALUES "; 
          $stQuery .= "( "; 
          $stQuery .=   "'".$this->db->escapeString($this->experienceId)."',";
          $stQuery .=   "'".$this->db->escapeString($this->biodataId)."',";
          $stQuery .=   "'".$this->db->escapeString($this->companyName)."',";
          $stQuery .=   " ".$this->workYear.",";
          $stQuery .=   " ".$this->workPeriod.",";
          $stQuery .=   "'".$this->db->escapeString($this->jobPosition)."',";
          $stQuery .=   "'".$this->db->escapeString($this->jobDesc)."',";
          $stQuery .=   "'".$this->db->escapeString($this->movingReason)."',";
          $stQuery .=   "'".$this->db->escapeString($this->picInput)."',";
          $stQuery .=   "'".$this->db->escapeString($this->inputTime)."',";
          $stQuery .=   "'".$this->db->escapeString($this->picEdit)."',";
          $stQuery .=   "'".$this->db->escapeString($this->editTime)."'";
          $stQuery .= "); "; 
          return $stQuery;
          // $this->db->query($stQuery); 
     }
     /* END INSERT */
     /* START UPDATE */
     public function upd($id)
     {
          if($this->experienceId =="" || $this->experienceId == NULL) 
          { 
               $this->experienceId = ''; 
          } 
          if($this->biodataId =="" || $this->biodataId == NULL) 
          { 
               $this->biodataId = ''; 
          } 
          if($this->companyName =="" || $this->companyName == NULL) 
          { 
               $this->companyName = ''; 
          } 
          if($this->workYear =="" || $this->workYear == NULL) 
          { 
               $this->workYear = 0; 
          } 
          if($this->workPeriod =="" || $this->workPeriod == NULL) 
          { 
               $this->workPeriod = 0; 
          } 
          if($this->jobPosition =="" || $this->jobPosition == NULL) 
          { 
               $this->jobPosition = ''; 
          } 
          if($this->jobDesc =="" || $this->jobDesc == NULL) 
          { 
               $this->jobDesc = ''; 
          } 
          if($this->movingReason =="" || $this->movingReason == NULL) 
          { 
               $this->movingReason = ''; 
          } 
          if($this->picInput =="" || $this->picInput == NULL) 
          { 
               $this->picInput = ''; 
          } 
          if($this->inputTime =="" || $this->inputTime == NULL) 
          { 
               $this->inputTime = '1700-01-01 00:00:00'; 
          } 
          if($this->picEdit =="" || $this->picEdit == NULL) 
          { 
               $this->picEdit = ''; 
          } 
          if($this->editTime =="" || $this->editTime == NULL) 
          { 
               $this->editTime = '1700-01-01 00:00:00'; 
          } 

          $stQuery  = "UPDATE ".$this->table." "; 
          $stQuery .= "SET "; 
          $stQuery .=   "experience_id ='".$this->db->escapeString($this->experienceId)."', "; 
          $stQuery .=   "biodata_id ='".$this->db->escapeString($this->biodataId)."', "; 
          $stQuery .=   "company_name ='".$this->db->escapeString($this->companyName)."', "; 
          $stQuery .=   'work_year ='.$this->workYear.','; 
          $stQuery .=   'work_period ='.$this->workPeriod.','; 
          $stQuery .=   "job_position ='".$this->db->escapeString($this->jobPosition)."', "; 
          $stQuery .=   "job_desc ='".$this->db->escapeString($this->jobDesc)."', "; 
          $stQuery .=   "moving_reason ='".$this->db->escapeString($this->movingReason)."', "; 
          $stQuery .=   "pic_input ='".$this->db->escapeString($this->picInput)."', "; 
          $stQuery .=   "input_time ='".$this->db->escapeString($this->inputTime)."', "; 
          $stQuery .=   "pic_edit ='".$this->db->escapeString($this->picEdit)."', "; 
          $stQuery .=   "edit_time ='".$this->db->escapeString($this->editTime)."' "; 
          $stQuery .= 'WHERE '; 
          $stQuery .=   "experience_id = '".$this->db->escapeString($id)."'"; 
          $this->db->query($stQuery); 
     }
     /* END UPDATE */
     /* START DELETE */
     public function del($id)
     {
          $stQuery  = "DELETE FROM ".$this->table." ";      
          $stQuery .= "WHERE experience_id = '".$this->db->escapeString($id)."'";
          $db->query($stQuery); 
     }
     /* END DELETE */
     /* START GET ALL DATA */
     public function getAll()
     {
          $stQuery  = "SELECT * FROM ".$this->table." "; 
          $query  = $this->db->query($stQuery); 
          $arrayList = $query->getResultArray(); 
          return $arrayList; 
     }
     /* END GET ALL DATA */
     /* START GET DATA BY ID */
     public function getById($id)
     {
          $stQuery  = "SELECT * FROM ".$this->table." ";     
          $stQuery .= "WHERE experience_id = '".$this->db->escapeString($id)."'";
          $query  = $this->db->query($stQuery); 
          $arrayRow = $query->getRowArray(); 
          return $arrayRow; 
     }
     /* END GET DATA BY ID */
     /* START GET OBJECT DATA BY ID */
     public function setObjectById($id)
     {
          $stQuery  = "SELECT * FROM ".$this->table." "; 
          $stQuery .= "WHERE experience_id = '".$this->db->escapeString($id)."'";
          $query  = $this->db->query($stQuery); 
          $arrayRow = $query->getRowArray(); 
          $this->experienceId = $arrayRow['experience_id']; 
          $this->biodataId = $arrayRow['biodata_id']; 
          $this->companyName = $arrayRow['company_name']; 
          $this->workYear = $arrayRow['work_year']; 
          $this->workPeriod = $arrayRow['work_period']; 
          $this->jobPosition = $arrayRow['job_position']; 
          $this->jobDesc = $arrayRow['job_desc']; 
          $this->movingReason = $arrayRow['moving_reason']; 
          $this->picInput = $arrayRow['pic_input']; 
          $this->inputTime = $arrayRow['input_time']; 
          $this->picEdit = $arrayRow['pic_edit']; 
          $this->editTime = $arrayRow['edit_time']; 
     }
     /* END GET OBJECT DATA BY ID */
     /* START OF RESET VALUES */
     public function resetValues()
     {
          $this->experienceId = ''; 
          $this->biodataId = ''; 
          $this->companyName = ''; 
          $this->workYear = 0; 
          $this->workPeriod = 0; 
          $this->jobPosition = ''; 
          $this->jobDesc = ''; 
          $this->movingReason = ''; 
          $this->picInput = ''; 
          $this->inputTime = '1700-01-01 00:00:00'; 
          $this->picEdit = ''; 
          $this->editTime = '1700-01-01 00:00:00'; 
     }
     /* END OF RESET VALUES */
     /* START OF ID GENERATOR */
     public function generateId($columnId)
     {
          $strSql = "SELECT ";
          $strSql .= " CASE WHEN id_year = curr_ym THEN ";
          $strSql .= "   CONCAT(id_year,LPAD(CAST( (CAST(RIGHT(fi,5) AS INTEGER)+1) AS CHAR),5,'0')) ";
          $strSql .= " ELSE ";
          $strSql .= "   CONCAT(curr_ym, '00001') ";
          $strSql .= " END AS doc_id ";
          $strSql .= "FROM ";
          $strSql .= " (SELECT ";
          $strSql .= "   MAX(".$columnId.") fi,";
          $strSql .= "   LEFT(".$columnId.",4) id_year,";
          $strSql .= "   current_date dt,";
          $strSql .= "   RIGHT(".$columnId.", 5) curr_id,";
          $strSql .= "   CONCAT(";
          $strSql .= "   RIGHT(CAST(DATE_FORMAT(NOW(), '%y') AS CHAR),2), ";
          $strSql .= "   LPAD(";
          $strSql .= "      CAST(DATE_FORMAT(NOW(), '%m') AS CHAR),2,'0'";
          $strSql .= "    )";
          $strSql .= "   ) curr_ym,";
          $strSql .= "   RIGHT(CAST(DATE_FORMAT(NOW(), '%y') AS CHAR),2) tYear,";
          $strSql .= "   CAST(DATE_FORMAT(NOW(), '%m') AS CHAR) tMonth ";
          $strSql .= " FROM ";
          $strSql .= "   ".$this->table." ";
          $strSql .= " GROUP BY ".$columnId.") a ";
          $strSql .= " ORDER BY doc_id DESC LIMIT 1 ";
          $query  = $this->db->query($strSql); 
          $arrayRow = $query->getRowArray(); 
          if(!isset($arrayRow))
          {
               $curYM = date('ym');
               $arrayRow['doc_id'] = $curYM.'00001';
          }
          return $arrayRow;
     }
     /* END OF ID GENERATOR */

     /* ADD MAUALLY */
     public function getByHeadId($biodataId)
     {
          $stQuery  = "SELECT company_name, job_position, job_desc, work_year, work_period, moving_reason FROM ".$this->table." ";     
          $stQuery .= "WHERE biodata_id = '".$this->db->escapeString($biodataId)."'";
          $query  = $this->db->query($stQuery); 
          $arrayRows = $query->getResultArray(); 
          return $arrayRows; 
     }
     
     public function delByHeadId($biodataId)
     {
          $stQuery  = "DELETE FROM ".$this->table." ";      
          $stQuery .= "WHERE biodata_id = '".$this->db->escapeString($biodataId)."'";
          // return $stQuery; exit();
          $this->db->query($stQuery); 
     }
}
