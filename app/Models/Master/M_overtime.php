<?php namespace App\Models\Master; 
/*********************************************************************
 *  Created By       :  Generator Version 1.0.23                     *
 *  Created Date     :  Feb 24, 2022                                 *
 *  Description      : All code generated by model generator         *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
use CodeIgniter\Model;

class M_overtime extends Model
{
     /* START PRIVATE VARIABLES */
     protected $DBGroup = '';
     protected $table = 'mt_overtime';
     protected $primaryKey = 'ot_id';
     protected $returnType = 'array';
     protected $db = null;

     protected $otId;
     protected $companyId;
     protected $ot1;
     protected $ot2;
     protected $ot3;
     protected $ot4;
     protected $multiplier1;
     protected $multiplier2;
     protected $multiplier3;
     protected $multiplier4;
     /* END PRIVATE VARIABLES */
     /* START CONSTRUCTOR */ 
     public function __construct()
     {
          parent::__construct();
          $this->db = \Config\Database::connect($this->DBGroup, false);
          $this->otId = 0;
          $this->companyId = 0;
          $this->ot1 = 0;
          $this->ot2 = 0;
          $this->ot3 = 0;
          $this->ot4 = 0;
          $this->multiplier1 = 0;
          $this->multiplier2 = 0;
          $this->multiplier3 = 0;
          $this->multiplier4 = 0;
     }
     /* END CONSTRUCTOR */
     
     /* START GENERATE SETTER AND GETTER */
     public function setOtId($aOtId)
     {
          $this->otId = $aOtId;
     }
     public function getOtId()
     {
          return $this->otId;
     }
     public function setCompanyId($aCompanyId)
     {
          $this->companyId = $aCompanyId;
     }
     public function getCompanyId()
     {
          return $this->companyId;
     }
     public function setOt1($aOt1)
     {
          $this->ot1 = $aOt1;
     }
     public function getOt1()
     {
          return $this->ot1;
     }
     public function setOt2($aOt2)
     {
          $this->ot2 = $aOt2;
     }
     public function getOt2()
     {
          return $this->ot2;
     }
     public function setOt3($aOt3)
     {
          $this->ot3 = $aOt3;
     }
     public function getOt3()
     {
          return $this->ot3;
     }
     public function setOt4($aOt4)
     {
          $this->ot4 = $aOt4;
     }
     public function getOt4()
     {
          return $this->ot4;
     }
     public function setMultiplier1($aMultiplier1)
     {
          $this->multiplier1 = $aMultiplier1;
     }
     public function getMultiplier1()
     {
          return $this->multiplier1;
     }
     public function setMultiplier2($aMultiplier2)
     {
          $this->multiplier2 = $aMultiplier2;
     }
     public function getMultiplier2()
     {
          return $this->multiplier2;
     }
     public function setMultiplier3($aMultiplier3)
     {
          $this->multiplier3 = $aMultiplier3;
     }
     public function getMultiplier3()
     {
          return $this->multiplier3;
     }
     public function setMultiplier4($aMultiplier4)
     {
          $this->multiplier4 = $aMultiplier4;
     }
     public function getMultiplier4()
     {
          return $this->multiplier4;
     }
     /* END GENERATE SETTER AND GETTER */
     /* START INSERT */
     public function ins()
     {
          if($this->otId =="" || $this->otId == NULL) 
          {
               $this->otId = 0; 
          }
          if($this->companyId =="" || $this->companyId == NULL) 
          {
               $this->companyId = 0; 
          }
          if($this->ot1 =="" || $this->ot1 == NULL) 
          {
               $this->ot1 = 0; 
          }
          if($this->ot2 =="" || $this->ot2 == NULL) 
          {
               $this->ot2 = 0; 
          }
          if($this->ot3 =="" || $this->ot3 == NULL) 
          {
               $this->ot3 = 0; 
          }
          if($this->ot4 =="" || $this->ot4 == NULL) 
          {
               $this->ot4 = 0; 
          }
          if($this->multiplier1 =="" || $this->multiplier1 == NULL) 
          {
               $this->multiplier1 = 0; 
          }
          if($this->multiplier2 =="" || $this->multiplier2 == NULL) 
          {
               $this->multiplier2 = 0; 
          }
          if($this->multiplier3 =="" || $this->multiplier3 == NULL) 
          {
               $this->multiplier3 = 0; 
          }
          if($this->multiplier4 =="" || $this->multiplier4 == NULL) 
          {
               $this->multiplier4 = 0; 
          }

          $stQuery  = "INSERT INTO ".$this->table." ";
          $stQuery .= "( ";
          $stQuery .=   "ot_id,"; 
          $stQuery .=   "company_id,"; 
          $stQuery .=   "ot1,"; 
          $stQuery .=   "ot2,"; 
          $stQuery .=   "ot3,"; 
          $stQuery .=   "ot4,"; 
          $stQuery .=   "multiplier1,"; 
          $stQuery .=   "multiplier2,"; 
          $stQuery .=   "multiplier3,"; 
          $stQuery .=   "multiplier4"; 
          $stQuery .= ") "; 
          $stQuery .= "VALUES "; 
          $stQuery .= "( "; 
          $stQuery .=   " ".$this->otId.",";
          $stQuery .=   " ".$this->companyId.",";
          $stQuery .=   " ".$this->ot1.",";
          $stQuery .=   " ".$this->ot2.",";
          $stQuery .=   " ".$this->ot3.",";
          $stQuery .=   " ".$this->ot4.",";
          $stQuery .=   " ".$this->multiplier1.",";
          $stQuery .=   " ".$this->multiplier2.",";
          $stQuery .=   " ".$this->multiplier3.",";
          $stQuery .=   " ".$this->multiplier4."";
          $stQuery .= "); "; 
          $this->db->query($stQuery); 
     }
     /* END INSERT */
     /* START QUERY INSERT */
     public function getInsQuery()
     {
          if($this->otId =="" || $this->otId == NULL) 
          {
               $this->otId = 0; 
          }
          if($this->companyId =="" || $this->companyId == NULL) 
          {
               $this->companyId = 0; 
          }
          if($this->ot1 =="" || $this->ot1 == NULL) 
          {
               $this->ot1 = 0; 
          }
          if($this->ot2 =="" || $this->ot2 == NULL) 
          {
               $this->ot2 = 0; 
          }
          if($this->ot3 =="" || $this->ot3 == NULL) 
          {
               $this->ot3 = 0; 
          }
          if($this->ot4 =="" || $this->ot4 == NULL) 
          {
               $this->ot4 = 0; 
          }
          if($this->multiplier1 =="" || $this->multiplier1 == NULL) 
          {
               $this->multiplier1 = 0; 
          }
          if($this->multiplier2 =="" || $this->multiplier2 == NULL) 
          {
               $this->multiplier2 = 0; 
          }
          if($this->multiplier3 =="" || $this->multiplier3 == NULL) 
          {
               $this->multiplier3 = 0; 
          }
          if($this->multiplier4 =="" || $this->multiplier4 == NULL) 
          {
               $this->multiplier4 = 0; 
          }

          $stQuery  = "INSERT INTO ".$this->table." ";
          $stQuery .= "( ";
          $stQuery .=   "ot_id,"; 
          $stQuery .=   "company_id,"; 
          $stQuery .=   "ot1,"; 
          $stQuery .=   "ot2,"; 
          $stQuery .=   "ot3,"; 
          $stQuery .=   "ot4,"; 
          $stQuery .=   "multiplier1,"; 
          $stQuery .=   "multiplier2,"; 
          $stQuery .=   "multiplier3,"; 
          $stQuery .=   "multiplier4"; 
          $stQuery .= ") "; 
          $stQuery .= "VALUES "; 
          $stQuery .= "( "; 
          $stQuery .=   " ".$this->otId.",";
          $stQuery .=   " ".$this->companyId.",";
          $stQuery .=   " ".$this->ot1.",";
          $stQuery .=   " ".$this->ot2.",";
          $stQuery .=   " ".$this->ot3.",";
          $stQuery .=   " ".$this->ot4.",";
          $stQuery .=   " ".$this->multiplier1.",";
          $stQuery .=   " ".$this->multiplier2.",";
          $stQuery .=   " ".$this->multiplier3.",";
          $stQuery .=   " ".$this->multiplier4."";
          $stQuery .= "); "; 
          return $stQuery; 
     }
     /* END QUERY INSERT */
     /* START UPDATE */
     public function upd($id)
     {
          if($this->otId =="" || $this->otId == NULL) 
          { 
               $this->otId = 0; 
          } 
          if($this->companyId =="" || $this->companyId == NULL) 
          { 
               $this->companyId = 0; 
          } 
          if($this->ot1 =="" || $this->ot1 == NULL) 
          { 
               $this->ot1 = 0; 
          } 
          if($this->ot2 =="" || $this->ot2 == NULL) 
          { 
               $this->ot2 = 0; 
          } 
          if($this->ot3 =="" || $this->ot3 == NULL) 
          { 
               $this->ot3 = 0; 
          } 
          if($this->ot4 =="" || $this->ot4 == NULL) 
          { 
               $this->ot4 = 0; 
          } 
          if($this->multiplier1 =="" || $this->multiplier1 == NULL) 
          { 
               $this->multiplier1 = 0; 
          } 
          if($this->multiplier2 =="" || $this->multiplier2 == NULL) 
          { 
               $this->multiplier2 = 0; 
          } 
          if($this->multiplier3 =="" || $this->multiplier3 == NULL) 
          { 
               $this->multiplier3 = 0; 
          } 
          if($this->multiplier4 =="" || $this->multiplier4 == NULL) 
          { 
               $this->multiplier4 = 0; 
          } 

          $stQuery  = "UPDATE ".$this->table." "; 
          $stQuery .= "SET "; 
          $stQuery .=   'ot_id ='.$this->otId.','; 
          $stQuery .=   'company_id ='.$this->companyId.','; 
          $stQuery .=   'ot1 ='.$this->ot1.','; 
          $stQuery .=   'ot2 ='.$this->ot2.','; 
          $stQuery .=   'ot3 ='.$this->ot3.','; 
          $stQuery .=   'ot4 ='.$this->ot4.','; 
          $stQuery .=   'multiplier1 ='.$this->multiplier1.','; 
          $stQuery .=   'multiplier2 ='.$this->multiplier2.','; 
          $stQuery .=   'multiplier3 ='.$this->multiplier3.','; 
          $stQuery .=   "multiplier4 =".$this->multiplier4." "; 
          $stQuery .= 'WHERE '; 
          $stQuery .=   'ot_id = '.$this->db->escapeString($id).''; 
          $this->db->query($stQuery); 
     }
     /* END UPDATE */
     /* START QUERY UPDATE */
     public function getUpdQuery($id)
     {
          if($this->otId =="" || $this->otId == NULL) 
          { 
               $this->otId = 0; 
          } 
          if($this->companyId =="" || $this->companyId == NULL) 
          { 
               $this->companyId = 0; 
          } 
          if($this->ot1 =="" || $this->ot1 == NULL) 
          { 
               $this->ot1 = 0; 
          } 
          if($this->ot2 =="" || $this->ot2 == NULL) 
          { 
               $this->ot2 = 0; 
          } 
          if($this->ot3 =="" || $this->ot3 == NULL) 
          { 
               $this->ot3 = 0; 
          } 
          if($this->ot4 =="" || $this->ot4 == NULL) 
          { 
               $this->ot4 = 0; 
          } 
          if($this->multiplier1 =="" || $this->multiplier1 == NULL) 
          { 
               $this->multiplier1 = 0; 
          } 
          if($this->multiplier2 =="" || $this->multiplier2 == NULL) 
          { 
               $this->multiplier2 = 0; 
          } 
          if($this->multiplier3 =="" || $this->multiplier3 == NULL) 
          { 
               $this->multiplier3 = 0; 
          } 
          if($this->multiplier4 =="" || $this->multiplier4 == NULL) 
          { 
               $this->multiplier4 = 0; 
          } 

          $stQuery  = "UPDATE ".$this->table." "; 
          $stQuery .= "SET "; 
          $stQuery .=   'ot_id ='.$this->otId.','; 
          $stQuery .=   'company_id ='.$this->companyId.','; 
          $stQuery .=   'ot1 ='.$this->ot1.','; 
          $stQuery .=   'ot2 ='.$this->ot2.','; 
          $stQuery .=   'ot3 ='.$this->ot3.','; 
          $stQuery .=   'ot4 ='.$this->ot4.','; 
          $stQuery .=   'multiplier1 ='.$this->multiplier1.','; 
          $stQuery .=   'multiplier2 ='.$this->multiplier2.','; 
          $stQuery .=   'multiplier3 ='.$this->multiplier3.','; 
          $stQuery .=   "multiplier4 =".$this->multiplier4." "; 
          $stQuery .= 'WHERE '; 
          $stQuery .=   'ot_id = '.$this->db->escapeString($id).''; 
          return $stQuery; 
     }
     /* END QUERY UPDATE */
     /* START DELETE */
     public function del($id)
     {
          $stQuery  = "DELETE FROM ".$this->table." ";      
          $stQuery .= "WHERE ot_id = ".$this->db->escapeString($id)."";
          $db->query($stQuery); 
     }
     /* END DELETE */
     /* START GET ALL DATA */
     public function getAll()
     {
          $stQuery  = "SELECT * FROM ".$this->table." "; 
          $query  = $this->db->query($stQuery); 
          $arrayList = $query->getResultArray(); 
          return $arrayList; 
     }
     /* END GET ALL DATA */
     /* START GET DATA BY ID */
     public function getById($id)
     {
          $stQuery  = "SELECT * FROM ".$this->table." ";     
          $stQuery .= "WHERE ot_id = ".$this->db->escapeString($id)."";
          $query  = $this->db->query($stQuery); 
          $arrayRow = $query->getRowArray(); 
          return $arrayRow; 
     }
     public function getByIdCompany($id)
     {
          $stQuery  = "SELECT * FROM ".$this->table." ";     
          $stQuery .= "WHERE company_id = ".$this->db->escapeString($id)."";
          $query  = $this->db->query($stQuery); 
          $arrayRow = $query->getRowArray(); 
          return $arrayRow; 
     }
     /* END GET DATA BY ID */
     /* START GET OBJECT DATA BY ID */
     public function setObjectById($id)
     {
          $stQuery  = "SELECT * FROM ".$this->table." "; 
          $stQuery .= "WHERE ot_id = ".$this->db->escapeString($id)."";
          $query  = $this->db->query($stQuery); 
          $arrayRow = $query->getRowArray(); 
          $this->otId = $arrayRow['ot_id']; 
          $this->companyId = $arrayRow['company_id']; 
          $this->ot1 = $arrayRow['ot1']; 
          $this->ot2 = $arrayRow['ot2']; 
          $this->ot3 = $arrayRow['ot3']; 
          $this->ot4 = $arrayRow['ot4']; 
          $this->multiplier1 = $arrayRow['multiplier1']; 
          $this->multiplier2 = $arrayRow['multiplier2']; 
          $this->multiplier3 = $arrayRow['multiplier3']; 
          $this->multiplier4 = $arrayRow['multiplier4']; 
     }
     /* END GET OBJECT DATA BY ID */
     /* START OF RESET VALUES */
     public function resetValues()
     {
          $this->otId = 0; 
          $this->companyId = 0; 
          $this->ot1 = 0; 
          $this->ot2 = 0; 
          $this->ot3 = 0; 
          $this->ot4 = 0; 
          $this->multiplier1 = 0; 
          $this->multiplier2 = 0; 
          $this->multiplier3 = 0; 
          $this->multiplier4 = 0; 
     }
     /* END OF RESET VALUES */
     /* START OF ID GENERATOR */
     public function generateId($columnId)
     {
          $strSql = "SELECT ";
          $strSql .= " CASE WHEN id_year = curr_ym THEN ";
          $strSql .= "   CONCAT(id_year,LPAD(CAST( (CAST(RIGHT(fi,5) AS INTEGER)+1) AS CHAR),5,'0')) ";
          $strSql .= " ELSE ";
          $strSql .= "   CONCAT(curr_ym, '00001') ";
          $strSql .= " END AS doc_id ";
          $strSql .= "FROM ";
          $strSql .= " (SELECT ";
          $strSql .= "   MAX(".$columnId.") fi,";
          $strSql .= "   LEFT(".$columnId.",4) id_year,";
          $strSql .= "   current_date dt,";
          $strSql .= "   RIGHT(".$columnId.", 5) curr_id,";
          $strSql .= "   CONCAT(";
          $strSql .= "   RIGHT(CAST(DATE_FORMAT(NOW(), '%y') AS CHAR),2), ";
          $strSql .= "   LPAD(";
          $strSql .= "      CAST(DATE_FORMAT(NOW(), '%m') AS CHAR),2,'0'";
          $strSql .= "    )";
          $strSql .= "   ) curr_ym,";
          $strSql .= "   RIGHT(CAST(DATE_FORMAT(NOW(), '%y') AS CHAR),2) tYear,";
          $strSql .= "   CAST(DATE_FORMAT(NOW(), '%m') AS CHAR) tMonth ";
          $strSql .= " FROM ";
          $strSql .= "   ".$this->table." ";
          $strSql .= " GROUP BY ".$columnId.") a ";
          $strSql .= " ORDER BY doc_id DESC LIMIT 1 ";
          $query  = $this->db->query($strSql); 
          $arrayRow = $query->getRowArray(); 
          if(!isset($arrayRow))
          {
               $curYM = date('ym');
               $arrayRow['doc_id'] = $curYM.'00001';
          }
          return $arrayRow;
     }
     /* END OF ID GENERATOR */
}
