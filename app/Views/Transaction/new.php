      <div class="row">
        <div class="col-md-12">
          <div class="tile">
          <h3 class="tile-title">Slip Process</h3>
           <div class="tile-body">
           	<form class = "row is_header">
               <!-- <div class="form-group col-sm-12 col-md-2"> -->
                  <!-- <label class="control-label">CLIENT</label> -->
                  <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
                  <!-- <select class="form-control" id="clientName" name="clientName" required=""> -->
                    <!-- <option value="" disabled="" selected="">Pilih</option> -->
                    <!-- <option value="Redpath_CAD">Redpath</option> -->
                  <!-- </select> -->
                <!-- </div> -->

                <div class="form-group col-sm-12 col-md-2">
                  <label class="control-label">YEAR</label>
                  <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
                  <select class="form-control" id="yearPeriod" name="yearPeriod" required="">
                    <option value="" disabled="" selected="">Pilih</option>
                    <script type="text/javascript">
                      var dt = new Date();
                      var currYear = dt.getFullYear();
                      var currMonth = dt.getMonth();
                              var currDay = dt.getDate();
                              var tmpDate = new Date(currYear + 1, currMonth, currDay);
                              var startYear = tmpDate.getFullYear();
                      var endYear = startYear - 80;             
                      for (var i = startYear; i >= endYear; i--) 
                      {
                        document.write("<option value='"+i+"'>"+i+"</option>");           
                      }
                    </script>
                  </select>
                </div>

                <div class="form-group col-sm-12 col-md-2">
                  <label class="control-label">MONTH</label>
                  <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
                  <select class="form-control" id="monthPeriod" name="monthPeriod" required="">
                    <option value="" disabled="" selected="">Pilih</option>
                    <script type="text/javascript">
                      var tMonth = 1;
                      for (var i = tMonth; i <= 12; i++) 
                      {
                        if(i < 10)
                        {
                          document.write("<option value='0"+i+"'>0"+i+"</option>");             
                        }
                        else
                        {
                          document.write("<option value='"+i+"'>"+i+"</option>");               
                        }
                        
                      }

                    </script>
                  </select>
                </div>
                <!-- <input type="file" name="tsFile" id="tsFile"> -->
              </form>
                <div class="form-group col-sm-12">
                  <button class="btn btn-primary" type="submit" id="btnProcess">
                    <i class="fa fa-fw fa-lg fas fa-plus-circle "></i>Process
                  </button>
                </div>
              </form>
           </div>
           <br>
           <br>
           <div class="tile-body">
           	<!-- TABLE -->
           	<div class="table-responsive">
           	  <table class="table table-hover table-bordered" id="trnSlip">
           	    <thead style="background-color: rgb(13 81 198);color: white;">
           	     <tr>
           	       <th>Slip Id</th>
           	       <th>Full Name</th>
           	       <th>Dept</th>
           	       <th>Position</th>
                   <th>Update</th>
           	     </tr>
           	    </thead>
           	    <tbody>
           	     <!-- <tr> -->
           	      <!-- <td>slip_id</td> -->
           	      <!-- <td>biodata_id</td> -->
           	      <!-- <td>ts_id</td> -->
           	      <!-- <td>year_period</td> -->
           	      <!-- <td>month_period</td> -->
           	      <!-- <td>full_name</td> -->
           	      <!-- <td>dept</td> -->
           	      <!-- <td>position</td> -->
           	      <!-- <td>marital_status</td> -->
           	      <!-- <td>class_id</td> -->
           	      <!-- <td>class_base</td> -->
           	      <!-- <td>work_total</td> -->
           	      <!-- <td>base_wage</td> -->
           	      <!-- <td>wages_in</td> -->
           	      <!-- <td>fixed_bonus</td> -->
           	      <!-- <td>variable_bonus</td> -->
           	      <!-- <td>jumbo_bonus</td> -->
           	      <!-- <td>salvac_bonus</td> -->
           	      <!-- <td>statutory</td> -->
           	      <!-- <td>travel</td> -->
           	      <!-- <td>allowance_01</td> -->
           	      <!-- <td>allowance_02</td> -->
           	      <!-- <td>allowance_03</td> -->
           	      <!-- <td>allowance_04</td> -->
           	      <!-- <td>allowance_05</td> -->
           	      <!-- <td>adjustment</td> -->
           	      <!-- <td>thr</td> -->
           	      <!-- <td>contract_bonus</td> -->
           	      <!-- <td>bpjs</td> -->
           	      <!-- <td>jkk_jkm</td> -->
           	      <!-- <td>jp</td> -->
           	      <!-- <td>jht</td> -->
           	      <!-- <td>emp_bpjs</td> -->
           	      <!-- <td>emp_jp</td> -->
           	      <!-- <td>emp_jht</td> -->
           	      <!-- <td>unpaid_count</td> -->
           	      <!-- <td>unpaid_total</td> -->
           	      <!-- <td>non_tax_allowance</td> -->
           	      <!-- <td>ptkp_total</td> -->
           	      <!-- <td>irregular_tax</td> -->
           	      <!-- <td>regular_tax</td> -->
           	      <!-- <td>salary_status</td> -->
           	      <!-- <td>status_remarks</td> -->
           	      <!-- <td>pic_edit</td> -->
           	      <!-- <td>edit_time</td> -->
           	      <!-- <td>pic_input</td> -->
           	      <!-- <td>input_time</td> -->
           	      <!-- <td>Link Edit</td> -->
           	     <!-- </tr> -->
           	    </tbody>
           	  </table>
           	</div>
           </div>
          </div> <!-- class="tile" -->
        </div> <!-- class="col-md-12" -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
          var baseUrl = '<?php echo base_url()?>';
           let monthPeriod = $("#monthPeriod").val();
           let yearPeriod  = $("#yearPeriod").val();
          /* START AJAX FOR LOAD DATA */
          $.ajax({
            /* ***Url is here */
            url : baseUrl+'/transaction/trn_slip/getAll',
            method : "POST",
            success : function(data)
            {
              let srcData = JSON.parse(data);
              /* Edit Url Controller is here */
                /* ***Using Valid Path */
              // let updUrl = baseUrl+'/master/Mt_contract/getAll',
              /* START TABLE */
              let trnSlip = $("#trnSlip").DataTable({
                "paging":   false,
                "ordering": false,
                "info":     false,
                "filter":   false,
                "autoWidth": false,
                "columnDefs": [
                        {
                          /* Hide Table Id */
                          "targets": [0],
                          "visible": false,
                          "searchable": false
                        },
                        // {
                        //  /* Column For Edit Link, (ex : 5) depend on last column no */
                        //  "targets": 5,
                        //  "data": "download_link",
                        //  "render": function ( data, type, row, meta ) {
                        //    /* Change table_id with primary key of your table  */
                        //    return '<a href="'+updUrl+row['table_id']+'">Edit</a>';
                        //  }
                        // }
                ],
                data : srcData,
                columns: [
                  { data: "dept" },
                  { data: "position" }
                ]
              })
              /* END TABLE */
            }
          });
          /* END AJAX FOR LOAD DATA */
           $('.errMsg').hide();
           $('#dataProses').html('');
           var isValid = true;

            $('#btnProcess').on('click', function(){   
              
              confirmDialog("Are You sure to do process?", (ans) => {
                if (ans) {
                  // console.log("yes");
                $("#loader").show();
                $('.errMsg').hide();
                $('#btnProcess').prop('disabled', true);
                // if($('#clientName option:selected').text() == 'Pilih') 
                // {
                //   $('#clientName').focus();
                //   $('#clientNameErr').show();
                //   isValid = false;
                // }
                if($('#monthPeriod option:selected').text() == 'Pilih') 
                {
                  $('#monthPeriod').focus();
                  $('#monthPeriodErr').show();
                  isValid = false;
                }
                else if($('#yearPeriod option:selected').text() == 'Pilih') 
                {
                  $('#yearPeriod').focus();
                  $('#yearPeriodErr').show();
                  isValid = false;
                }

                if(isValid == false)
                {
                  $("#loader").hide();
                  $('#btnProcess').prop('disabled', false);
                          $.notify({
                            title: "<h5>Informasi : </h5>",
                            message: "<strong>Check Data</strong> </br></br> ",
                            icon: '' 
                        },
                        {
                            type: "warning",
                            delay: 3000
                        }); 
                  return false;
                }


                var monthPeriod = $('#monthPeriod').val();
                var yearPeriod  = $('#yearPeriod').val();
                // alert(fileName);
                
                $.ajax({
                    method : "POST",
                    url    : myUrl,
                    data   : {
                    monthPeriod  : monthPeriod,
                    yearPeriod   : yearPeriod
                    },
                    success : function(data){
                      // alert(data);
                    }
                });

          }
        });
    });
  });
</script>
