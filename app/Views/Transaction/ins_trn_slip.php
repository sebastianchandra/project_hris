      <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">
      	 	  <h3 class="tile-title">Trn_slip Form</h3>
      	 	  <div class="tile-body">
        		  <!-- Check Your Valid URL -->
      	 	    <form class="row" method="POST" action="../insData">
              <div class="form-group col-md-3">
                <label class="control-label">Slip Id</label>
                <input class="form-control" name="slipId" id="slipId" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Biodata Id</label>
                <input class="form-control" name="biodataId" id="biodataId" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Ts Id</label>
                <input class="form-control" name="tsId" id="tsId" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Year Period</label>
                <input class="form-control" name="yearPeriod" id="yearPeriod" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Month Period</label>
                <input class="form-control" name="monthPeriod" id="monthPeriod" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Full Name</label>
                <input class="form-control" name="fullName" id="fullName" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Dept</label>
                <input class="form-control" name="dept" id="dept" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Position</label>
                <input class="form-control" name="position" id="position" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Marital Status</label>
                <input class="form-control" name="maritalStatus" id="maritalStatus" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Class Id</label>
                <input class="form-control" name="classId" id="classId" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Class Base</label>
                <input class="form-control" name="classBase" id="classBase" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Work Total</label>
                <input class="form-control" name="workTotal" id="workTotal" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Base Wage</label>
                <input class="form-control" name="baseWage" id="baseWage" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Wages In</label>
                <input class="form-control" name="wagesIn" id="wagesIn" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Fixed Bonus</label>
                <input class="form-control" name="fixedBonus" id="fixedBonus" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Variable Bonus</label>
                <input class="form-control" name="variableBonus" id="variableBonus" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Jumbo Bonus</label>
                <input class="form-control" name="jumboBonus" id="jumboBonus" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Salvac Bonus</label>
                <input class="form-control" name="salvacBonus" id="salvacBonus" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Statutory</label>
                <input class="form-control" name="statutory" id="statutory" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Travel</label>
                <input class="form-control" name="travel" id="travel" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Allowance 01</label>
                <input class="form-control" name="allowance01" id="allowance01" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Allowance 02</label>
                <input class="form-control" name="allowance02" id="allowance02" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Allowance 03</label>
                <input class="form-control" name="allowance03" id="allowance03" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Allowance 04</label>
                <input class="form-control" name="allowance04" id="allowance04" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Allowance 05</label>
                <input class="form-control" name="allowance05" id="allowance05" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Adjustment</label>
                <input class="form-control" name="adjustment" id="adjustment" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Thr</label>
                <input class="form-control" name="thr" id="thr" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Contract Bonus</label>
                <input class="form-control" name="contractBonus" id="contractBonus" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Bpjs</label>
                <input class="form-control" name="bpjs" id="bpjs" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Jkk Jkm</label>
                <input class="form-control" name="jkkJkm" id="jkkJkm" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Jp</label>
                <input class="form-control" name="jp" id="jp" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Jht</label>
                <input class="form-control" name="jht" id="jht" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Emp Bpjs</label>
                <input class="form-control" name="empBpjs" id="empBpjs" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Emp Jp</label>
                <input class="form-control" name="empJp" id="empJp" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Emp Jht</label>
                <input class="form-control" name="empJht" id="empJht" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Unpaid Count</label>
                <input class="form-control" name="unpaidCount" id="unpaidCount" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Unpaid Total</label>
                <input class="form-control" name="unpaidTotal" id="unpaidTotal" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Non Tax Allowance</label>
                <input class="form-control" name="nonTaxAllowance" id="nonTaxAllowance" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Ptkp Total</label>
                <input class="form-control" name="ptkpTotal" id="ptkpTotal" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Irregular Tax</label>
                <input class="form-control" name="irregularTax" id="irregularTax" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Regular Tax</label>
                <input class="form-control" name="regularTax" id="regularTax" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Salary Status</label>
                <input class="form-control" name="salaryStatus" id="salaryStatus" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Status Remarks</label>
                <input class="form-control" name="statusRemarks" id="statusRemarks" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Pic Edit</label>
                <input class="form-control" name="picEdit" id="picEdit" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Edit Time</label>
                <input class="form-control" name="editTime" id="editTime" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Pic Input</label>
                <input class="form-control" name="picInput" id="picInput" type="text" placeholder="Id Karyawan">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Input Time</label>
                <input class="form-control" name="inputTime" id="inputTime" type="text" placeholder="Id Karyawan">
              </div>
      	 	    </form>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div class="tile-footer">
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
      	 	    &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
      	 	    <strong>
      	 	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	 	      <span style="color: red" class="errSaveMess"></span>
      	 	    </strong>
      	 	  </div>
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
          var baseUrl = '<?php echo base_url()?>';
          $("#slipId").focus();
          $("#dbSave").on("click", function(){
             let slipId = ("#slipId").val();
             let biodataId = ("#biodataId").val();
             let tsId = ("#tsId").val();
             let yearPeriod = ("#yearPeriod").val();
             let monthPeriod = ("#monthPeriod").val();
             let fullName = ("#fullName").val();
             let dept = ("#dept").val();
             let position = ("#position").val();
             let maritalStatus = ("#maritalStatus").val();
             let classId = ("#classId").val();
             let classBase = ("#classBase").val();
             let workTotal = ("#workTotal").val();
             let baseWage = ("#baseWage").val();
             let wagesIn = ("#wagesIn").val();
             let fixedBonus = ("#fixedBonus").val();
             let variableBonus = ("#variableBonus").val();
             let jumboBonus = ("#jumboBonus").val();
             let salvacBonus = ("#salvacBonus").val();
             let statutory = ("#statutory").val();
             let travel = ("#travel").val();
             let allowance01 = ("#allowance01").val();
             let allowance02 = ("#allowance02").val();
             let allowance03 = ("#allowance03").val();
             let allowance04 = ("#allowance04").val();
             let allowance05 = ("#allowance05").val();
             let adjustment = ("#adjustment").val();
             let thr = ("#thr").val();
             let contractBonus = ("#contractBonus").val();
             let bpjs = ("#bpjs").val();
             let jkkJkm = ("#jkkJkm").val();
             let jp = ("#jp").val();
             let jht = ("#jht").val();
             let empBpjs = ("#empBpjs").val();
             let empJp = ("#empJp").val();
             let empJht = ("#empJht").val();
             let unpaidCount = ("#unpaidCount").val();
             let unpaidTotal = ("#unpaidTotal").val();
             let nonTaxAllowance = ("#nonTaxAllowance").val();
             let ptkpTotal = ("#ptkpTotal").val();
             let irregularTax = ("#irregularTax").val();
             let regularTax = ("#regularTax").val();
             let salaryStatus = ("#salaryStatus").val();
             let statusRemarks = ("#statusRemarks").val();
             let picEdit = ("#picEdit").val();
             let editTime = ("#editTime").val();
             let picInput = ("#picInput").val();
             let inputTime = ("#inputTime").val();
             $(".errSaveMess").html("");
             if(slipId.trim() == "")
             {
               $("#slipId").focus();
               $(".errSaveMess").html("Slip Id cannot be empty");
             }
             else if(biodataId.trim() == "")
             {
               $("#biodataId").focus();
               $(".errSaveMess").html("Biodata Id cannot be empty");
             }
             else if(tsId.trim() == "")
             {
               $("#tsId").focus();
               $(".errSaveMess").html("Ts Id cannot be empty");
             }
             else if(yearPeriod.trim() == "")
             {
               $("#yearPeriod").focus();
               $(".errSaveMess").html("Year Period cannot be empty");
             }
             else if(monthPeriod.trim() == "")
             {
               $("#monthPeriod").focus();
               $(".errSaveMess").html("Month Period cannot be empty");
             }
             else if(fullName.trim() == "")
             {
               $("#fullName").focus();
               $(".errSaveMess").html("Full Name cannot be empty");
             }
             else if(dept.trim() == "")
             {
               $("#dept").focus();
               $(".errSaveMess").html("Dept cannot be empty");
             }
             else if(position.trim() == "")
             {
               $("#position").focus();
               $(".errSaveMess").html("Position cannot be empty");
             }
             else if(maritalStatus.trim() == "")
             {
               $("#maritalStatus").focus();
               $(".errSaveMess").html("Marital Status cannot be empty");
             }
             else if(classId.trim() == "")
             {
               $("#classId").focus();
               $(".errSaveMess").html("Class Id cannot be empty");
             }
             else if(classBase.trim() == "")
             {
               $("#classBase").focus();
               $(".errSaveMess").html("Class Base cannot be empty");
             }
             else if(workTotal.trim() == "")
             {
               $("#workTotal").focus();
               $(".errSaveMess").html("Work Total cannot be empty");
             }
             else if(baseWage.trim() == "")
             {
               $("#baseWage").focus();
               $(".errSaveMess").html("Base Wage cannot be empty");
             }
             else if(wagesIn.trim() == "")
             {
               $("#wagesIn").focus();
               $(".errSaveMess").html("Wages In cannot be empty");
             }
             else if(fixedBonus.trim() == "")
             {
               $("#fixedBonus").focus();
               $(".errSaveMess").html("Fixed Bonus cannot be empty");
             }
             else if(variableBonus.trim() == "")
             {
               $("#variableBonus").focus();
               $(".errSaveMess").html("Variable Bonus cannot be empty");
             }
             else if(jumboBonus.trim() == "")
             {
               $("#jumboBonus").focus();
               $(".errSaveMess").html("Jumbo Bonus cannot be empty");
             }
             else if(salvacBonus.trim() == "")
             {
               $("#salvacBonus").focus();
               $(".errSaveMess").html("Salvac Bonus cannot be empty");
             }
             else if(statutory.trim() == "")
             {
               $("#statutory").focus();
               $(".errSaveMess").html("Statutory cannot be empty");
             }
             else if(travel.trim() == "")
             {
               $("#travel").focus();
               $(".errSaveMess").html("Travel cannot be empty");
             }
             else if(allowance01.trim() == "")
             {
               $("#allowance01").focus();
               $(".errSaveMess").html("Allowance 01 cannot be empty");
             }
             else if(allowance02.trim() == "")
             {
               $("#allowance02").focus();
               $(".errSaveMess").html("Allowance 02 cannot be empty");
             }
             else if(allowance03.trim() == "")
             {
               $("#allowance03").focus();
               $(".errSaveMess").html("Allowance 03 cannot be empty");
             }
             else if(allowance04.trim() == "")
             {
               $("#allowance04").focus();
               $(".errSaveMess").html("Allowance 04 cannot be empty");
             }
             else if(allowance05.trim() == "")
             {
               $("#allowance05").focus();
               $(".errSaveMess").html("Allowance 05 cannot be empty");
             }
             else if(adjustment.trim() == "")
             {
               $("#adjustment").focus();
               $(".errSaveMess").html("Adjustment cannot be empty");
             }
             else if(thr.trim() == "")
             {
               $("#thr").focus();
               $(".errSaveMess").html("Thr cannot be empty");
             }
             else if(contractBonus.trim() == "")
             {
               $("#contractBonus").focus();
               $(".errSaveMess").html("Contract Bonus cannot be empty");
             }
             else if(bpjs.trim() == "")
             {
               $("#bpjs").focus();
               $(".errSaveMess").html("Bpjs cannot be empty");
             }
             else if(jkkJkm.trim() == "")
             {
               $("#jkkJkm").focus();
               $(".errSaveMess").html("Jkk Jkm cannot be empty");
             }
             else if(jp.trim() == "")
             {
               $("#jp").focus();
               $(".errSaveMess").html("Jp cannot be empty");
             }
             else if(jht.trim() == "")
             {
               $("#jht").focus();
               $(".errSaveMess").html("Jht cannot be empty");
             }
             else if(empBpjs.trim() == "")
             {
               $("#empBpjs").focus();
               $(".errSaveMess").html("Emp Bpjs cannot be empty");
             }
             else if(empJp.trim() == "")
             {
               $("#empJp").focus();
               $(".errSaveMess").html("Emp Jp cannot be empty");
             }
             else if(empJht.trim() == "")
             {
               $("#empJht").focus();
               $(".errSaveMess").html("Emp Jht cannot be empty");
             }
             else if(unpaidCount.trim() == "")
             {
               $("#unpaidCount").focus();
               $(".errSaveMess").html("Unpaid Count cannot be empty");
             }
             else if(unpaidTotal.trim() == "")
             {
               $("#unpaidTotal").focus();
               $(".errSaveMess").html("Unpaid Total cannot be empty");
             }
             else if(nonTaxAllowance.trim() == "")
             {
               $("#nonTaxAllowance").focus();
               $(".errSaveMess").html("Non Tax Allowance cannot be empty");
             }
             else if(ptkpTotal.trim() == "")
             {
               $("#ptkpTotal").focus();
               $(".errSaveMess").html("Ptkp Total cannot be empty");
             }
             else if(irregularTax.trim() == "")
             {
               $("#irregularTax").focus();
               $(".errSaveMess").html("Irregular Tax cannot be empty");
             }
             else if(regularTax.trim() == "")
             {
               $("#regularTax").focus();
               $(".errSaveMess").html("Regular Tax cannot be empty");
             }
             else if(salaryStatus.trim() == "")
             {
               $("#salaryStatus").focus();
               $(".errSaveMess").html("Salary Status cannot be empty");
             }
             else if(statusRemarks.trim() == "")
             {
               $("#statusRemarks").focus();
               $(".errSaveMess").html("Status Remarks cannot be empty");
             }
             else if(picEdit.trim() == "")
             {
               $("#picEdit").focus();
               $(".errSaveMess").html("Pic Edit cannot be empty");
             }
             else if(editTime.trim() == "")
             {
               $("#editTime").focus();
               $(".errSaveMess").html("Edit Time cannot be empty");
             }
             else if(picInput.trim() == "")
             {
               $("#picInput").focus();
               $(".errSaveMess").html("Pic Input cannot be empty");
             }
             else if(inputTime.trim() == "")
             {
               $("#inputTime").focus();
               $(".errSaveMess").html("Input Time cannot be empty");
             }
      	 	   /* ***Put URL your here */
             var myUrl ="../insData";
             $.ajax({
                url    : myUrl,
                method : "POST",
                data   : {
                   slipId : $("#slipId").val(),
                   biodataId : $("#biodataId").val(),
                   tsId : $("#tsId").val(),
                   yearPeriod : $("#yearPeriod").val(),
                   monthPeriod : $("#monthPeriod").val(),
                   fullName : $("#fullName").val(),
                   dept : $("#dept").val(),
                   position : $("#position").val(),
                   maritalStatus : $("#maritalStatus").val(),
                   classId : $("#classId").val(),
                   classBase : $("#classBase").val(),
                   workTotal : $("#workTotal").val(),
                   baseWage : $("#baseWage").val(),
                   wagesIn : $("#wagesIn").val(),
                   fixedBonus : $("#fixedBonus").val(),
                   variableBonus : $("#variableBonus").val(),
                   jumboBonus : $("#jumboBonus").val(),
                   salvacBonus : $("#salvacBonus").val(),
                   statutory : $("#statutory").val(),
                   travel : $("#travel").val(),
                   allowance01 : $("#allowance01").val(),
                   allowance02 : $("#allowance02").val(),
                   allowance03 : $("#allowance03").val(),
                   allowance04 : $("#allowance04").val(),
                   allowance05 : $("#allowance05").val(),
                   adjustment : $("#adjustment").val(),
                   thr : $("#thr").val(),
                   contractBonus : $("#contractBonus").val(),
                   bpjs : $("#bpjs").val(),
                   jkkJkm : $("#jkkJkm").val(),
                   jp : $("#jp").val(),
                   jht : $("#jht").val(),
                   empBpjs : $("#empBpjs").val(),
                   empJp : $("#empJp").val(),
                   empJht : $("#empJht").val(),
                   unpaidCount : $("#unpaidCount").val(),
                   unpaidTotal : $("#unpaidTotal").val(),
                   nonTaxAllowance : $("#nonTaxAllowance").val(),
                   ptkpTotal : $("#ptkpTotal").val(),
                   irregularTax : $("#irregularTax").val(),
                   regularTax : $("#regularTax").val(),
                   salaryStatus : $("#salaryStatus").val(),
                   statusRemarks : $("#statusRemarks").val(),
                   picEdit : $("#picEdit").val(),
                   editTime : $("#editTime").val(),
                   picInput : $("#picInput").val(),
                   inputTime : $("#inputTime").val()
                },
                success : function(data)
                {
      	 	         $.notify({
      	 	            title: "Information : ",
      	 	            message: "New data has been saved!",
      	 	            icon: "fa fa-check"
      	 	         },{
      	 	            type: "info"
      	 	         });
      	 	         /* Your redirect is here */
                   window.location.href = "#";
                }
             })
          });
        });
      </script>
