      <div class="row">
        <div class="col-md-12">
          <div class="tile">
          <h3 class="tile-title">Slip Process</h3>
           <div class="tile-footer">
           	<form class = "row is_header">
               <!-- <div class="form-group col-sm-12 col-md-2"> -->
                  <!-- <label class="control-label">CLIENT</label> -->
                  <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
                  <!-- <select class="form-control" id="clientName" name="clientName" required=""> -->
                    <!-- <option value="" disabled="" selected="">Pilih</option> -->
                    <!-- <option value="Redpath_CAD">Redpath</option> -->
                  <!-- </select> -->
                <!-- </div> -->

                <div class="form-group col-sm-12 col-md-2">
                  <label class="control-label">YEAR</label>
                  <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
                  <select class="form-control" id="yearPeriod" name="yearPeriod" required="">
                    <option value="" disabled="" selected="">Pilih</option>
                    <script type="text/javascript">
                      var dt = new Date();
                      var currYear = dt.getFullYear();
                      var currMonth = dt.getMonth();
                              var currDay = dt.getDate();
                              var tmpDate = new Date(currYear + 1, currMonth, currDay);
                              var startYear = tmpDate.getFullYear();
                      var endYear = startYear - 80;             
                      for (var i = startYear; i >= endYear; i--) 
                      {
                        document.write("<option value='"+i+"'>"+i+"</option>");           
                      }
                    </script>
                  </select>
                </div>

                <div class="form-group col-sm-12 col-md-2">
                  <label class="control-label">MONTH</label>
                  <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
                  <select class="form-control" id="monthPeriod" name="monthPeriod" required="">
                    <option value="" disabled="" selected="">Pilih</option>
                    <script type="text/javascript">
                      var tMonth = 1;
                      for (var i = tMonth; i <= 12; i++) 
                      {
                        if(i < 10)
                        {
                          document.write("<option value='0"+i+"'>0"+i+"</option>");             
                        }
                        else
                        {
                          document.write("<option value='"+i+"'>"+i+"</option>");               
                        }
                        
                      }

                    </script>
                  </select>
                </div>

                <div class="form-group col-sm-12 col-md-2">
                  <label class="control-label">PERIOD NO</label>
                  <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
                  <select class="form-control" id="periodNo" name="periodNo" required="">
                    <option value="1" selected="">1</option>
                    <option value="2">2</option>                    
                  </select>
                </div>
                <!-- <input type="file" name="tsFile" id="tsFile"> -->
              <!-- <a href="#"> -->
                <div class="form-group col-sm-12">
                  <a class="btn btn-primary" type="button" id="btnProcess"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i>Process</a>
                  <!-- <button class="btn btn-primary" type="submit" id="btnProcess">
                    <i class="fa fa-fw fa-lg fas fa-plus-circle "></i>Process
                  </button> -->
                </div>
                  <!-- <strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <span style="color: red" class="errSaveMess"></span>
                  </strong> -->
              <!-- </a> -->
              </form>
           </div>
           <br>
           <br>
           <div class="tile-body">
           	<!-- TABLE -->
           	<div class="table-responsive">
           	  <table class="table table-hover table-bordered" id="trSlip">
           	    <thead style="background-color: rgb(13 81 198);color: white;">
           	     <tr>
           	       <!-- <th>Slip Id</th>
           	       <th>Full Name</th>
           	       <th>Dept</th>
           	       <th>Position</th>
                   <th>Update</th> -->

                   <th>Slip Id</th>
                   <th>Biodata Id</th>
                   <th>Ts Id</th>
                   <th>Year Period</th>
                   <th>Month Period</th>
                   <th>Slip Period</th>
                   <th>Full Name</th>
                   <th>Dept</th>
                   <!-- <th>Position</th> -->
                   <!-- <th>Marital Status</th> -->
                   <th>Class Id</th>
                   <!-- <th>Class Base</th> -->
                   <!-- <th>Work Total</th> -->
                   <!-- <th>Base Wage</th> -->
                   <!-- <th>Wages In</th> -->
                   <!-- <th>Fixed Bonus</th> -->
                   <!-- <th>Variable Bonus</th> -->
                   <!-- <th>Jumbo Bonus</th> -->
                   <!-- <th>Salvac Bonus</th> -->
                   <!-- <th>Statutory</th> -->
                   <!-- <th>Travel</th> -->
                   <!-- <th>Pvb Val</th> -->
                   <!-- <th>Pvb Percent</th> -->
                   <!-- <th>Wl Qty</th> -->
                   <!-- <th>Wl Value</th> -->
                   <!-- <th>Allowance 01</th> -->
                   <!-- <th>Allowance 02</th> -->
                   <!-- <th>Allowance 03</th> -->
                   <!-- <th>Allowance 04</th> -->
                   <!-- <th>Allowance 05</th> -->
                   <!-- <th>Adjustment</th> -->
                   <!-- <th>Thr</th> -->
                   <!-- <th>Contract Bonus</th> -->
                   <!-- <th>Bpjs</th> -->
                   <!-- <th>Jkk</th> -->
                   <!-- <th>Jk</th> -->
                   <!-- <th>Jp</th> -->
                   <!-- <th>Jht</th> -->
                   <!-- <th>Emp Bpjs</th> -->
                   <!-- <th>Emp Jp</th> -->
                   <!-- <th>Emp Jht</th> -->
                   <!-- <th>Unpaid Count</th> -->
                   <!-- <th>Unpaid Total</th> -->
                   <!-- <th>Non Tax Allowance</th> -->
                   <!-- <th>Ptkp Total</th> -->
                   <!-- <th>Irregular Tax</th> -->
                   <!-- <th>Regular Tax</th> -->
                   <!-- <th>Salary Status</th> -->
                   <!-- <th>Status Remarks</th> -->
                   <th>Print</th>
           	     </tr>
           	    </thead>
           	    <tbody>
           	     <!-- <tr> -->
           	      <!-- <td>slip_id</td> -->
           	      <!-- <td>biodata_id</td> -->
           	      <!-- <td>ts_id</td> -->
           	      <!-- <td>year_period</td> -->
           	      <!-- <td>month_period</td> -->
           	      <!-- <td>full_name</td> -->
           	      <!-- <td>dept</td> -->
           	      <!-- <td>position</td> -->
           	      <!-- <td>marital_status</td> -->
           	      <!-- <td>class_id</td> -->
           	      <!-- <td>class_base</td> -->
           	      <!-- <td>work_total</td> -->
           	      <!-- <td>base_wage</td> -->
           	      <!-- <td>wages_in</td> -->
           	      <!-- <td>fixed_bonus</td> -->
           	      <!-- <td>variable_bonus</td> -->
           	      <!-- <td>jumbo_bonus</td> -->
           	      <!-- <td>salvac_bonus</td> -->
           	      <!-- <td>statutory</td> -->
           	      <!-- <td>travel</td> -->
           	      <!-- <td>allowance_01</td> -->
           	      <!-- <td>allowance_02</td> -->
           	      <!-- <td>allowance_03</td> -->
           	      <!-- <td>allowance_04</td> -->
           	      <!-- <td>allowance_05</td> -->
           	      <!-- <td>adjustment</td> -->
           	      <!-- <td>thr</td> -->
           	      <!-- <td>contract_bonus</td> -->
           	      <!-- <td>bpjs</td> -->
           	      <!-- <td>jkk_jkm</td> -->
           	      <!-- <td>jp</td> -->
           	      <!-- <td>jht</td> -->
           	      <!-- <td>emp_bpjs</td> -->
           	      <!-- <td>emp_jp</td> -->
           	      <!-- <td>emp_jht</td> -->
           	      <!-- <td>unpaid_count</td> -->
           	      <!-- <td>unpaid_total</td> -->
           	      <!-- <td>non_tax_allowance</td> -->
           	      <!-- <td>ptkp_total</td> -->
           	      <!-- <td>irregular_tax</td> -->
           	      <!-- <td>regular_tax</td> -->
           	      <!-- <td>salary_status</td> -->
           	      <!-- <td>status_remarks</td> -->
           	      <!-- <td>pic_edit</td> -->
           	      <!-- <td>edit_time</td> -->
           	      <!-- <td>pic_input</td> -->
           	      <!-- <td>input_time</td> -->
           	      <!-- <td>Link Edit</td> -->
           	     <!-- </tr> -->
           	    </tbody>
           	  </table>
           	</div>
           </div>
          </div> <!-- class="tile" -->
        </div> <!-- class="col-md-12" -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
          var baseUrl = '<?php echo base_url()?>';
          // alert(baseUrl+"/transaction/trn_slip/getAll");
          /* START AJAX FOR LOAD DATA */
          $.ajax({
            /* ***Url is here */
            url : baseUrl+"/transaction/trn_slip/getAll",
            method : "POST",
            success : function(data)
            {
              let srcData = JSON.parse(data);
              /* Edit Url Controller is here */
                /* ***Using Valid Path */
              let updUrl = '<?php echo base_url(); ?>/transaction/trn_slip/test_pdf/';
              /* START TABLE */
              let trSlip = $("#trSlip").DataTable({
                "paging":   false,
                "ordering": false,
                "info":     false,
                "filter":   false,
                "autoWidth": false,
                "columnDefs": [
                        {
                          /* Hide Table Id */
                          "targets": [0],
                          "visible": true,
                          "searchable": false
                        },
                        {
                          /* Column For Edit Link, (ex : 5) depend on last column no */
                          "targets": 9,
                          "data": "download_link",
                          "render": function ( data, type, row, meta ) {
                            /* Change table_id with primary key of your table  */
                            return '<a class="btn btn-warning" type="button" href="'+updUrl+row['slip_id']+'">Print</a>';
                          }
                        }
                ],
                data : srcData,
                columns: [
                  { data: "slip_id" },
                  { data: "biodata_id" },
                  { data: "ts_id" },
                  { data: "year_period" },
                  { data: "month_period" },
                  { data: "slip_period" },
                  { data: "full_name" },
                  { data: "dept" },
                  // { data: "position" },
                  // { data: "marital_status" },
                  { data: "class_id" },
                  // { data: "class_base" },
                  // { data: "work_total" },
                  // { data: "base_wage" },
                  // { data: "wages_in" },
                  // { data: "fixed_bonus" },
                  // { data: "variable_bonus" },
                  // { data: "jumbo_bonus" },
                  // { data: "salvac_bonus" },
                  // { data: "statutory" },
                  // { data: "travel" },
                  // { data: "pvb_val" },
                  // { data: "pvb_percent" },
                  // { data: "wl_qty" },
                  // { data: "wl_value" },
                  // { data: "allowance_01" },
                  // { data: "allowance_02" },
                  // { data: "allowance_03" },
                  // { data: "allowance_04" },
                  // { data: "allowance_05" },
                  // { data: "adjustment" },
                  // { data: "thr" },
                  // { data: "contract_bonus" },
                  // { data: "bpjs" },
                  // { data: "jkk" },
                  // { data: "jk" },
                  // { data: "jp" },
                  // { data: "jht" },
                  // { data: "emp_bpjs" },
                  // { data: "emp_jp" },
                  // { data: "emp_jht" },
                  // { data: "unpaid_count" },
                  // { data: "unpaid_total" },
                  // { data: "non_tax_allowance" },
                  // { data: "ptkp_total" },
                  // { data: "irregular_tax" },
                  // { data: "regular_tax" },
                  // { data: "salary_status" },
                  // { data: "status_remarks" }
                ]
              })
              /* END TABLE */
            }
          });
          /* END AJAX FOR LOAD DATA */

          $('#btnProcess').on("click", function(){
                // debugger;
                var monthPeriod = $('#monthPeriod').val();
                var yearPeriod  = $('#yearPeriod').val();
                var periodNo  = $('#periodNo').val();

                var myUrl ='<?php echo base_url() ?>/Transaction/Trn_slip/tsProcess';

                // alert(myUrl);
                
                $.ajax({
                    url : myUrl,
                    method : "POST",
                    data   : {
                      monthPeriod  : monthPeriod,
                      yearPeriod   : yearPeriod,
                      periodNo     : periodNo
                    },
                    success : function(data){
                      alert(data);
                      setTimeout(function () {
                        window.location.href = '<?php echo base_url() ?>'+'/Transaction/Trn_slip';
                      }, 2000);
                    }
                });

          });

        });
      </script>
