      <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">
      	 	  <h3 class="tile-title">Tr_timesheet Update</h3>
      	 	  <div class="tile-body">
        		  <!-- Check Your Valid URL -->
      	 	    <form class="row" method="POST" action="../updData">
              <div class="form-group col-md-3">
                <label class="control-label">Ts Id</label>
                <input class="form-control" name="tsId" id="tsId" type="text" value="<?php echo $trTimesheet['ts_id'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Biodata Id</label>
                <input class="form-control" name="biodataId" id="biodataId" type="text" value="<?php echo $trTimesheet['biodata_id'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Badge No</label>
                <input class="form-control" name="badgeNo" id="badgeNo" type="text" value="<?php echo $trTimesheet['badge_no'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Full Name</label>
                <input class="form-control" name="fullName" id="fullName" type="text" value="<?php echo $trTimesheet['full_name'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Payroll Base</label>
                <input class="form-control" name="payrollBase" id="payrollBase" type="text" value="<?php echo $trTimesheet['payroll_base'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Client Name</label>
                <input class="form-control" name="clientName" id="clientName" type="text" value="<?php echo $trTimesheet['client_name'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Dept</label>
                <input class="form-control" name="dept" id="dept" type="text" value="<?php echo $trTimesheet['dept'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Month Process</label>
                <input class="form-control" name="monthProcess" id="monthProcess" type="text" value="<?php echo $trTimesheet['month_process'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Year Process</label>
                <input class="form-control" name="yearProcess" id="yearProcess" type="text" value="<?php echo $trTimesheet['year_process'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Pass Day</label>
                <input class="form-control" name="passDay" id="passDay" type="text" value="<?php echo $trTimesheet['pass_day'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D01</label>
                <input class="form-control" name="d01" id="d01" type="text" value="<?php echo $trTimesheet['d01'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D02</label>
                <input class="form-control" name="d02" id="d02" type="text" value="<?php echo $trTimesheet['d02'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D03</label>
                <input class="form-control" name="d03" id="d03" type="text" value="<?php echo $trTimesheet['d03'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D04</label>
                <input class="form-control" name="d04" id="d04" type="text" value="<?php echo $trTimesheet['d04'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D05</label>
                <input class="form-control" name="d05" id="d05" type="text" value="<?php echo $trTimesheet['d05'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D06</label>
                <input class="form-control" name="d06" id="d06" type="text" value="<?php echo $trTimesheet['d06'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D07</label>
                <input class="form-control" name="d07" id="d07" type="text" value="<?php echo $trTimesheet['d07'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D08</label>
                <input class="form-control" name="d08" id="d08" type="text" value="<?php echo $trTimesheet['d08'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D09</label>
                <input class="form-control" name="d09" id="d09" type="text" value="<?php echo $trTimesheet['d09'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D10</label>
                <input class="form-control" name="d10" id="d10" type="text" value="<?php echo $trTimesheet['d10'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D11</label>
                <input class="form-control" name="d11" id="d11" type="text" value="<?php echo $trTimesheet['d11'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D12</label>
                <input class="form-control" name="d12" id="d12" type="text" value="<?php echo $trTimesheet['d12'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D13</label>
                <input class="form-control" name="d13" id="d13" type="text" value="<?php echo $trTimesheet['d13'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D14</label>
                <input class="form-control" name="d14" id="d14" type="text" value="<?php echo $trTimesheet['d14'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D15</label>
                <input class="form-control" name="d15" id="d15" type="text" value="<?php echo $trTimesheet['d15'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D16</label>
                <input class="form-control" name="d16" id="d16" type="text" value="<?php echo $trTimesheet['d16'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D17</label>
                <input class="form-control" name="d17" id="d17" type="text" value="<?php echo $trTimesheet['d17'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D18</label>
                <input class="form-control" name="d18" id="d18" type="text" value="<?php echo $trTimesheet['d18'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D19</label>
                <input class="form-control" name="d19" id="d19" type="text" value="<?php echo $trTimesheet['d19'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D20</label>
                <input class="form-control" name="d20" id="d20" type="text" value="<?php echo $trTimesheet['d20'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D21</label>
                <input class="form-control" name="d21" id="d21" type="text" value="<?php echo $trTimesheet['d21'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D22</label>
                <input class="form-control" name="d22" id="d22" type="text" value="<?php echo $trTimesheet['d22'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D23</label>
                <input class="form-control" name="d23" id="d23" type="text" value="<?php echo $trTimesheet['d23'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D24</label>
                <input class="form-control" name="d24" id="d24" type="text" value="<?php echo $trTimesheet['d24'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D25</label>
                <input class="form-control" name="d25" id="d25" type="text" value="<?php echo $trTimesheet['d25'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D26</label>
                <input class="form-control" name="d26" id="d26" type="text" value="<?php echo $trTimesheet['d26'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D27</label>
                <input class="form-control" name="d27" id="d27" type="text" value="<?php echo $trTimesheet['d27'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D28</label>
                <input class="form-control" name="d28" id="d28" type="text" value="<?php echo $trTimesheet['d28'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D29</label>
                <input class="form-control" name="d29" id="d29" type="text" value="<?php echo $trTimesheet['d29'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D30</label>
                <input class="form-control" name="d30" id="d30" type="text" value="<?php echo $trTimesheet['d30'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">D31</label>
                <input class="form-control" name="d31" id="d31" type="text" value="<?php echo $trTimesheet['d31'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Is Active</label>
                <input class="form-control" name="isActive" id="isActive" type="text" value="<?php echo $trTimesheet['is_active'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Pic Process</label>
                <input class="form-control" name="picProcess" id="picProcess" type="text" value="<?php echo $trTimesheet['pic_process'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Process Time</label>
                <input class="form-control" name="processTime" id="processTime" type="text" value="<?php echo $trTimesheet['process_time'] ?>">
              </div>
      	 	    </form>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div class="tile-footer">
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
      	 	    &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
      	 	    <strong>
      	 	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	 	      <span style="color: red" class="errSaveMess"></span>
      	 	    </strong>
      	 	  </div>
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
          var baseUrl = '<?php echo base_url()?>';
          $("#tsId").focus();
          $("#dbSave").on("click", function(){
             let tsId = ("#tsId").val();
             let biodataId = ("#biodataId").val();
             let badgeNo = ("#badgeNo").val();
             let fullName = ("#fullName").val();
             let payrollBase = ("#payrollBase").val();
             let clientName = ("#clientName").val();
             let dept = ("#dept").val();
             let monthProcess = ("#monthProcess").val();
             let yearProcess = ("#yearProcess").val();
             let passDay = ("#passDay").val();
             let d01 = ("#d01").val();
             let d02 = ("#d02").val();
             let d03 = ("#d03").val();
             let d04 = ("#d04").val();
             let d05 = ("#d05").val();
             let d06 = ("#d06").val();
             let d07 = ("#d07").val();
             let d08 = ("#d08").val();
             let d09 = ("#d09").val();
             let d10 = ("#d10").val();
             let d11 = ("#d11").val();
             let d12 = ("#d12").val();
             let d13 = ("#d13").val();
             let d14 = ("#d14").val();
             let d15 = ("#d15").val();
             let d16 = ("#d16").val();
             let d17 = ("#d17").val();
             let d18 = ("#d18").val();
             let d19 = ("#d19").val();
             let d20 = ("#d20").val();
             let d21 = ("#d21").val();
             let d22 = ("#d22").val();
             let d23 = ("#d23").val();
             let d24 = ("#d24").val();
             let d25 = ("#d25").val();
             let d26 = ("#d26").val();
             let d27 = ("#d27").val();
             let d28 = ("#d28").val();
             let d29 = ("#d29").val();
             let d30 = ("#d30").val();
             let d31 = ("#d31").val();
             let isActive = ("#isActive").val();
             let picProcess = ("#picProcess").val();
             let processTime = ("#processTime").val();
             $(".errSaveMess").html("");
             if(tsId.trim() == "")
             {
               $("#tsId").focus();
               $(".errSaveMess").html("Ts Id cannot be empty");
             }
             else if(biodataId.trim() == "")
             {
               $("#biodataId").focus();
               $(".errSaveMess").html("Biodata Id cannot be empty");
             }
             else if(badgeNo.trim() == "")
             {
               $("#badgeNo").focus();
               $(".errSaveMess").html("Badge No cannot be empty");
             }
             else if(fullName.trim() == "")
             {
               $("#fullName").focus();
               $(".errSaveMess").html("Full Name cannot be empty");
             }
             else if(payrollBase.trim() == "")
             {
               $("#payrollBase").focus();
               $(".errSaveMess").html("Payroll Base cannot be empty");
             }
             else if(clientName.trim() == "")
             {
               $("#clientName").focus();
               $(".errSaveMess").html("Client Name cannot be empty");
             }
             else if(dept.trim() == "")
             {
               $("#dept").focus();
               $(".errSaveMess").html("Dept cannot be empty");
             }
             else if(monthProcess.trim() == "")
             {
               $("#monthProcess").focus();
               $(".errSaveMess").html("Month Process cannot be empty");
             }
             else if(yearProcess.trim() == "")
             {
               $("#yearProcess").focus();
               $(".errSaveMess").html("Year Process cannot be empty");
             }
             else if(passDay.trim() == "")
             {
               $("#passDay").focus();
               $(".errSaveMess").html("Pass Day cannot be empty");
             }
             else if(d01.trim() == "")
             {
               $("#d01").focus();
               $(".errSaveMess").html("D01 cannot be empty");
             }
             else if(d02.trim() == "")
             {
               $("#d02").focus();
               $(".errSaveMess").html("D02 cannot be empty");
             }
             else if(d03.trim() == "")
             {
               $("#d03").focus();
               $(".errSaveMess").html("D03 cannot be empty");
             }
             else if(d04.trim() == "")
             {
               $("#d04").focus();
               $(".errSaveMess").html("D04 cannot be empty");
             }
             else if(d05.trim() == "")
             {
               $("#d05").focus();
               $(".errSaveMess").html("D05 cannot be empty");
             }
             else if(d06.trim() == "")
             {
               $("#d06").focus();
               $(".errSaveMess").html("D06 cannot be empty");
             }
             else if(d07.trim() == "")
             {
               $("#d07").focus();
               $(".errSaveMess").html("D07 cannot be empty");
             }
             else if(d08.trim() == "")
             {
               $("#d08").focus();
               $(".errSaveMess").html("D08 cannot be empty");
             }
             else if(d09.trim() == "")
             {
               $("#d09").focus();
               $(".errSaveMess").html("D09 cannot be empty");
             }
             else if(d10.trim() == "")
             {
               $("#d10").focus();
               $(".errSaveMess").html("D10 cannot be empty");
             }
             else if(d11.trim() == "")
             {
               $("#d11").focus();
               $(".errSaveMess").html("D11 cannot be empty");
             }
             else if(d12.trim() == "")
             {
               $("#d12").focus();
               $(".errSaveMess").html("D12 cannot be empty");
             }
             else if(d13.trim() == "")
             {
               $("#d13").focus();
               $(".errSaveMess").html("D13 cannot be empty");
             }
             else if(d14.trim() == "")
             {
               $("#d14").focus();
               $(".errSaveMess").html("D14 cannot be empty");
             }
             else if(d15.trim() == "")
             {
               $("#d15").focus();
               $(".errSaveMess").html("D15 cannot be empty");
             }
             else if(d16.trim() == "")
             {
               $("#d16").focus();
               $(".errSaveMess").html("D16 cannot be empty");
             }
             else if(d17.trim() == "")
             {
               $("#d17").focus();
               $(".errSaveMess").html("D17 cannot be empty");
             }
             else if(d18.trim() == "")
             {
               $("#d18").focus();
               $(".errSaveMess").html("D18 cannot be empty");
             }
             else if(d19.trim() == "")
             {
               $("#d19").focus();
               $(".errSaveMess").html("D19 cannot be empty");
             }
             else if(d20.trim() == "")
             {
               $("#d20").focus();
               $(".errSaveMess").html("D20 cannot be empty");
             }
             else if(d21.trim() == "")
             {
               $("#d21").focus();
               $(".errSaveMess").html("D21 cannot be empty");
             }
             else if(d22.trim() == "")
             {
               $("#d22").focus();
               $(".errSaveMess").html("D22 cannot be empty");
             }
             else if(d23.trim() == "")
             {
               $("#d23").focus();
               $(".errSaveMess").html("D23 cannot be empty");
             }
             else if(d24.trim() == "")
             {
               $("#d24").focus();
               $(".errSaveMess").html("D24 cannot be empty");
             }
             else if(d25.trim() == "")
             {
               $("#d25").focus();
               $(".errSaveMess").html("D25 cannot be empty");
             }
             else if(d26.trim() == "")
             {
               $("#d26").focus();
               $(".errSaveMess").html("D26 cannot be empty");
             }
             else if(d27.trim() == "")
             {
               $("#d27").focus();
               $(".errSaveMess").html("D27 cannot be empty");
             }
             else if(d28.trim() == "")
             {
               $("#d28").focus();
               $(".errSaveMess").html("D28 cannot be empty");
             }
             else if(d29.trim() == "")
             {
               $("#d29").focus();
               $(".errSaveMess").html("D29 cannot be empty");
             }
             else if(d30.trim() == "")
             {
               $("#d30").focus();
               $(".errSaveMess").html("D30 cannot be empty");
             }
             else if(d31.trim() == "")
             {
               $("#d31").focus();
               $(".errSaveMess").html("D31 cannot be empty");
             }
             else if(isActive.trim() == "")
             {
               $("#isActive").focus();
               $(".errSaveMess").html("Is Active cannot be empty");
             }
             else if(picProcess.trim() == "")
             {
               $("#picProcess").focus();
               $(".errSaveMess").html("Pic Process cannot be empty");
             }
             else if(processTime.trim() == "")
             {
               $("#processTime").focus();
               $(".errSaveMess").html("Process Time cannot be empty");
             }
      	 	  /* ***Put URL your here */
             var myUrl ="../updData";
             $.ajax({
                url    : myUrl,
                method : "POST",
                data   : {
                   tsId : $("#tsId").val(),
                   biodataId : $("#biodataId").val(),
                   badgeNo : $("#badgeNo").val(),
                   fullName : $("#fullName").val(),
                   payrollBase : $("#payrollBase").val(),
                   clientName : $("#clientName").val(),
                   dept : $("#dept").val(),
                   monthProcess : $("#monthProcess").val(),
                   yearProcess : $("#yearProcess").val(),
                   passDay : $("#passDay").val(),
                   d01 : $("#d01").val(),
                   d02 : $("#d02").val(),
                   d03 : $("#d03").val(),
                   d04 : $("#d04").val(),
                   d05 : $("#d05").val(),
                   d06 : $("#d06").val(),
                   d07 : $("#d07").val(),
                   d08 : $("#d08").val(),
                   d09 : $("#d09").val(),
                   d10 : $("#d10").val(),
                   d11 : $("#d11").val(),
                   d12 : $("#d12").val(),
                   d13 : $("#d13").val(),
                   d14 : $("#d14").val(),
                   d15 : $("#d15").val(),
                   d16 : $("#d16").val(),
                   d17 : $("#d17").val(),
                   d18 : $("#d18").val(),
                   d19 : $("#d19").val(),
                   d20 : $("#d20").val(),
                   d21 : $("#d21").val(),
                   d22 : $("#d22").val(),
                   d23 : $("#d23").val(),
                   d24 : $("#d24").val(),
                   d25 : $("#d25").val(),
                   d26 : $("#d26").val(),
                   d27 : $("#d27").val(),
                   d28 : $("#d28").val(),
                   d29 : $("#d29").val(),
                   d30 : $("#d30").val(),
                   d31 : $("#d31").val(),
                   isActive : $("#isActive").val(),
                   picProcess : $("#picProcess").val(),
                   processTime : $("#processTime").val()
                },
                success : function(data)
                {
      	 	         $.notify({
      	 	            title: "Information : ",
      	 	            message: "New data has been saved!",
      	 	            icon: "fa fa-check"
      	 	         },{
      	 	            type: "info"
      	 	         });
      	 	         /* Your redirect is here */
                   window.location.href = "#";
                }
             })
          });
        });
      </script>
