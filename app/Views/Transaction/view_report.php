      <div class="row">
        <div class="col-md-12">
          <div class="tile">
          <h3 class="tile-title">Report</h3>
           <div class="tile-footer">
           	<form class = "row is_header">
               <!-- <div class="form-group col-sm-12 col-md-2"> -->
                  <!-- <label class="control-label">CLIENT</label> -->
                  <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
                  <!-- <select class="form-control" id="clientName" name="clientName" required=""> -->
                    <!-- <option value="" disabled="" selected="">Pilih</option> -->
                    <!-- <option value="Redpath_CAD">Redpath</option> -->
                  <!-- </select> -->
                <!-- </div> -->

                <div class="form-group col-sm-12 col-md-2">
                  <label class="control-label">YEAR</label>
                  <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
                  <select class="form-control" id="yearPeriod" name="yearPeriod" required="">
                    <option value="" disabled="" selected="">Pilih</option>
                    <script type="text/javascript">
                      var dt = new Date();
                      var currYear = dt.getFullYear();
                      var currMonth = dt.getMonth();
                              var currDay = dt.getDate();
                              var tmpDate = new Date(currYear + 1, currMonth, currDay);
                              var startYear = tmpDate.getFullYear();
                      var endYear = startYear - 80;             
                      for (var i = startYear; i >= endYear; i--) 
                      {
                        document.write("<option value='"+i+"'>"+i+"</option>");           
                      }
                    </script>
                  </select>
                </div>

                <div class="form-group col-sm-12 col-md-2">
                  <label class="control-label">MONTH</label>
                  <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
                  <select class="form-control" id="monthPeriod" name="monthPeriod" required="">
                    <option value="" disabled="" selected="">Pilih</option>
                    <script type="text/javascript">
                      var tMonth = 1;
                      for (var i = tMonth; i <= 12; i++) 
                      {
                        if(i < 10)
                        {
                          document.write("<option value='0"+i+"'>0"+i+"</option>");             
                        }
                        else
                        {
                          document.write("<option value='"+i+"'>"+i+"</option>");               
                        }
                        
                      }

                    </script>
                  </select>
                </div>

                <div class="form-group col-sm-12 col-md-2">
                  <label class="control-label">PERIOD NO</label>
                  <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
                  <select class="form-control" id="periodNo" name="periodNo" required="">
                    <option value="1" selected="">1</option>
                    <option value="2">2</option>                    
                  </select>
                </div>

                <div class="form-group col-sm-12 col-md-2">
                  <label class="control-label">STAFF / HOURLY</label>
                  <!-- <code id="docKindErr" class="errMsg"><span> : Required</span></code> -->
                  <select class="form-control" id="empStatus" name="empStatus" required="">
                    <option value="STAFF" selected="">Staff</option>
                    <option value="HOURLY">Hourly</option>                    
                  </select>
                </div>
                <!-- <input type="file" name="tsFile" id="tsFile"> -->
              <!-- <a href="#"> -->
                <div class="form-group col-sm-12">
                  <a class="btn btn-primary" type="button" id="btnSearch"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i>Process</a>
                  <a class="btn btn-warning" href="<?php echo base_url(); ?>/transaction/timesheet/test_pdf"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i>Print</a>
                  <!-- <button class="btn btn-primary" type="submit" id="btnProcess">
                    <i class="fa fa-fw fa-lg fas fa-plus-circle "></i>Process
                  </button> -->
                </div>
                  <!-- <strong>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <span style="color: red" class="errSaveMess"></span>
                  </strong> -->
              <!-- </a> -->
              </form>
           </div>
           <br>
           <br>
           <div class="tile-body">
           	<!-- TABLE -->
           	<div class="table-responsive">
           	  <table class="table table-hover table-bordered" id="trReport">
           	    <thead style="background-color: rgb(13 81 198);color: white;">
           	     <tr>
           	       <th>Slip Id</th>
           	       <th>Full Name</th>
           	       <th>Dept</th>
           	       <th>Position</th>
                   <th>Wages In</th>
                   <th>NPWP</th>
                   <th>d01</th>
                   <th>Class Category</th>
                   <th>Jumbo bonus</th>
                   <!-- <th>Update</th> -->
           	     </tr>
           	    </thead>
           	    <tbody>
           	     <!-- <tr> -->
           	      <!-- <td>slip_id</td> -->
           	      <!-- <td>biodata_id</td> -->
           	      <!-- <td>ts_id</td> -->
           	      <!-- <td>year_period</td> -->
           	      <!-- <td>month_period</td> -->
           	      <!-- <td>full_name</td> -->
           	      <!-- <td>dept</td> -->
           	      <!-- <td>position</td> -->
           	      <!-- <td>marital_status</td> -->
           	      <!-- <td>class_id</td> -->
           	      <!-- <td>class_base</td> -->
           	      <!-- <td>work_total</td> -->
           	      <!-- <td>base_wage</td> -->
           	      <!-- <td>wages_in</td> -->
           	      <!-- <td>fixed_bonus</td> -->
           	      <!-- <td>variable_bonus</td> -->
           	      <!-- <td>jumbo_bonus</td> -->
           	      <!-- <td>salvac_bonus</td> -->
           	      <!-- <td>statutory</td> -->
           	      <!-- <td>travel</td> -->
           	      <!-- <td>allowance_01</td> -->
           	      <!-- <td>allowance_02</td> -->
           	      <!-- <td>allowance_03</td> -->
           	      <!-- <td>allowance_04</td> -->
           	      <!-- <td>allowance_05</td> -->
           	      <!-- <td>adjustment</td> -->
           	      <!-- <td>thr</td> -->
           	      <!-- <td>contract_bonus</td> -->
           	      <!-- <td>bpjs</td> -->
           	      <!-- <td>jkk_jkm</td> -->
           	      <!-- <td>jp</td> -->
           	      <!-- <td>jht</td> -->
           	      <!-- <td>emp_bpjs</td> -->
           	      <!-- <td>emp_jp</td> -->
           	      <!-- <td>emp_jht</td> -->
           	      <!-- <td>unpaid_count</td> -->
           	      <!-- <td>unpaid_total</td> -->
           	      <!-- <td>non_tax_allowance</td> -->
           	      <!-- <td>ptkp_total</td> -->
           	      <!-- <td>irregular_tax</td> -->
           	      <!-- <td>regular_tax</td> -->
           	      <!-- <td>salary_status</td> -->
           	      <!-- <td>status_remarks</td> -->
           	      <!-- <td>pic_edit</td> -->
           	      <!-- <td>edit_time</td> -->
           	      <!-- <td>pic_input</td> -->
           	      <!-- <td>input_time</td> -->
           	      <!-- <td>Link Edit</td> -->
           	     <!-- </tr> -->
           	    </tbody>
           	  </table>
           	</div>
           </div>
          </div> <!-- class="tile" -->
        </div> <!-- class="col-md-12" -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
          $('#demoSelect').select2();
          var baseUrl  = '<?php echo base_url()?>';
          // var dataList = [];
          // var baseUrl = '<?php echo base_url()?>';
          // alert(baseUrl+"/transaction/trn_slip/getAll");
          /* START AJAX FOR LOAD DATA */
          $.ajax({
            /* Url is here */
            url    : baseUrl+'/transaction/Trn_report/getAll',
            method : "POST",
            success : function(data)
            {
              let srcData  = JSON.parse(data);
              /* Edit Url Controller is here */
              // let updUrl   = '<?php //echo base_url(); ?>/master/mt_salary/upd_view/';
              /* START TABLE */
              let trReport = $("#trReport").DataTable({
                "paging":   true,
                "ordering": true,
                "info":     true,
                "filter":   false,
                "autoWidth": false,
                "columnDefs": [
                        // {
                        //  /* Hide Table Id */
                        //  "targets": [0],
                        //  "visible": false,
                        //  "searchable": false
                        // },
                        {
                          /* Column For Edit Link, (ex : 5) depend on last column no */
                          "targets": 8,
                          "data": "download_link",
                          "render": function ( data, type, row, meta ) {
                            /* Change table_id with primary key of your table  */
                            return '<a href="'+updUrl+row['salary_id']+'">Edit</a>';
                          }
                        }
                ],
                data : srcData,
                columns: [
                  // { data: "salary_id" },
                  // { data: "slip_id" },
                  { data: "full_name" },
                  { data: "dept" }
                  // { data: "position" },
                  // { data: "wages_in" },
                  // { data: "tax_no" },
                  // { data: "d01" },
                  // { data: "class_base" },
                  // { data: "jumbo_bonus" }
                ]
              })
              /* END TABLE */
            }
          });
          /* END AJAX FOR LOAD DATA */

          $('#btnSearch').on("click", function(){
                // debugger;
                var monthPeriod = $('#monthPeriod').val();
                var yearPeriod  = $('#yearPeriod').val();
                var periodNo  = $('#periodNo').val();
                var empStatus  = $('#empStatus').val();

                var myUrl ='<?php echo base_url() ?>/Transaction/Trn_report/getAll';
                
                $.ajax({
                    url : myUrl,
                    method : "POST",
                    data   : {
                      monthPeriod  : monthPeriod,
                      yearPeriod   : yearPeriod,
                      periodNo     : periodNo,
                      empStatus    : empStatus
                    },
                    success : function(data){
                      alert();
                    }
                });

          });

        });
      </script>
