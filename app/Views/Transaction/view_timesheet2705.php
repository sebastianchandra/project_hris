      <div class="row">
        <div class="col-md-12">
          <div class="tile">
          <h3 class="tile-title">Timesheet Upload</h3>
           <div class="tile-footer">
              <form method="post" id="tsUploadForm" enctype="multipart/form-data" action="<?php echo base_url() ?>/transaction/timesheet/upload">
                <input type="file" name="tsFile" id="tsFile">
              <!-- <a href="#"> -->
                <button class="btn btn-primary" type="submit" id="btUpload">
                  <i class="fa fa-fw fa-lg fas fa-plus-circle "></i>Timesheet Upload
                </button>
              <!-- </a> -->
              </form> 

              <br>
              <br>

              <form method="post" id="mnlUploadForm" enctype="multipart/form-data" action="<?php echo base_url() ?>/transaction/Tr_manual_value/upload">
                <input type="file" name="mnlFile" id="mnlFile">
              <!-- <a href="#"> -->
                <button class="btn btn-primary" type="submit" id="mnlUpload">
                  <i class="fa fa-fw fa-lg fas fa-plus-circle "></i>Manual Value Upload
                </button>
              <!-- </a> -->
              </form>
           </div>
           <br>
           <br>
           <div class="tile-body">
           	<!-- TABLE -->
           	<div class="table-responsive">
           	  <table class="table table-hover table-bordered" id="trTimesheet">
           	    <thead style="background-color: rgb(13 81 198);color: white;">
           	     <!-- <tr>
           	       <th>Ts Id</th>
           	       <th>Biodata Id</th>
           	       <th>Badge No</th>
           	       <th>Full Name</th>
           	       <th>Payroll Base</th>
           	       <th>Client Name</th>
           	       <th>Dept</th>
           	       <th>Month Process</th>
           	       <th>Year Process</th>
           	       <th>Pass Day</th>
           	       <th>D01</th>
           	       <th>D02</th>
           	       <th>D03</th>
           	       <th>D04</th>
           	       <th>D05</th>
           	       <th>D06</th>
           	       <th>D07</th>
           	       <th>D08</th>
           	       <th>D09</th>
           	       <th>D10</th>
           	       <th>D11</th>
           	       <th>D12</th>
           	       <th>D13</th>
           	       <th>D14</th>
           	       <th>D15</th>
           	       <th>D16</th>
           	       <th>D17</th>
           	       <th>D18</th>
           	       <th>D19</th>
           	       <th>D20</th>
           	       <th>D21</th>
           	       <th>D22</th>
           	       <th>D23</th>
           	       <th>D24</th>
           	       <th>D25</th>
           	       <th>D26</th>
           	       <th>D27</th>
           	       <th>D28</th>
           	       <th>D29</th>
           	       <th>D30</th>
           	       <th>D31</th>
           	     </tr> -->
           	    </thead>
           	    <tbody>
           	     <!-- <tr> -->
           	      <!-- <td>ts_id</td> -->
           	      <!-- <td>biodata_id</td> -->
           	      <!-- <td>badge_no</td> -->
           	      <!-- <td>full_name</td> -->
           	      <!-- <td>payroll_base</td> -->
           	      <!-- <td>client_name</td> -->
           	      <!-- <td>dept</td> -->
           	      <!-- <td>month_process</td> -->
           	      <!-- <td>year_process</td> -->
           	      <!-- <td>pass_day</td> -->
           	      <!-- <td>d01</td> -->
           	      <!-- <td>d02</td> -->
           	      <!-- <td>d03</td> -->
           	      <!-- <td>d04</td> -->
           	      <!-- <td>d05</td> -->
           	      <!-- <td>d06</td> -->
           	      <!-- <td>d07</td> -->
           	      <!-- <td>d08</td> -->
           	      <!-- <td>d09</td> -->
           	      <!-- <td>d10</td> -->
           	      <!-- <td>d11</td> -->
           	      <!-- <td>d12</td> -->
           	      <!-- <td>d13</td> -->
           	      <!-- <td>d14</td> -->
           	      <!-- <td>d15</td> -->
           	      <!-- <td>d16</td> -->
           	      <!-- <td>d17</td> -->
           	      <!-- <td>d18</td> -->
           	      <!-- <td>d19</td> -->
           	      <!-- <td>d20</td> -->
           	      <!-- <td>d21</td> -->
           	      <!-- <td>d22</td> -->
           	      <!-- <td>d23</td> -->
           	      <!-- <td>d24</td> -->
           	      <!-- <td>d25</td> -->
           	      <!-- <td>d26</td> -->
           	      <!-- <td>d27</td> -->
           	      <!-- <td>d28</td> -->
           	      <!-- <td>d29</td> -->
           	      <!-- <td>d30</td> -->
           	      <!-- <td>d31</td> -->
           	      <!-- <td>is_active</td> -->
           	      <!-- <td>pic_process</td> -->
           	      <!-- <td>process_time</td> -->
           	      <!-- <td>Link Edit</td> -->
           	     <!-- </tr> -->
           	    </tbody>
           	  </table>
           	</div>
           </div>
          </div> <!-- class="tile" -->
        </div> <!-- class="col-md-12" -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
        	var baseUrl = '<?php echo base_url()?>';
        	/* START AJAX FOR LOAD DATA */
        	$.ajax({
        		/* ***Url is here */
        		// url : baseUrl+'/transaction/timesheet/getAll',
        		method : "POST",
        		success : function(data)
        		{
        			let srcData = JSON.parse(data);
        			/* Edit Url Controller is here */
          			/* ***Using Valid Path */
        			// let updUrl = baseUrl+'/master/Mt_contract/getAll',
        			/* START TABLE */
        			let trTimesheet = $("#trTimesheet").DataTable({
        				"paging":   false,
        				"ordering": false,
        				"info":     false,
        				"filter":   false,
        				"autoWidth": false,
        				"columnDefs": [
        								{
        									/* Hide Table Id */
        									"targets": [0],
        									"visible": false,
        									"searchable": false
        								},
        								// {
        								// 	/* Column For Edit Link, (ex : 5) depend on last column no */
        								// 	"targets": 5,
        								// 	"data": "download_link",
        								// 	"render": function ( data, type, row, meta ) {
        								// 	  /* Change table_id with primary key of your table  */
        								// 	  return '<a href="'+updUrl+row['table_id']+'">Edit</a>';
        								// 	}
        								// }
        				],
        				data : srcData,
        				columns: [
        					{ data: "ts_id" },
        					{ data: "biodata_id" },
        					{ data: "badge_no" },
        					{ data: "full_name" },
        					{ data: "payroll_base" },
        					{ data: "client_name" },
        					{ data: "dept" },
        					{ data: "month_process" },
        					{ data: "year_process" },
        					{ data: "pass_day" },
        					{ data: "d01" },
        					{ data: "d02" },
        					{ data: "d03" },
        					{ data: "d04" },
        					{ data: "d05" },
        					{ data: "d06" },
        					{ data: "d07" },
        					{ data: "d08" },
        					{ data: "d09" },
        					{ data: "d10" },
        					{ data: "d11" },
        					{ data: "d12" },
        					{ data: "d13" },
        					{ data: "d14" },
        					{ data: "d15" },
        					{ data: "d16" },
        					{ data: "d17" },
        					{ data: "d18" },
        					{ data: "d19" },
        					{ data: "d20" },
        					{ data: "d21" },
        					{ data: "d22" },
        					{ data: "d23" },
        					{ data: "d24" },
        					{ data: "d25" },
        					{ data: "d26" },
        					{ data: "d27" },
        					{ data: "d28" },
        					{ data: "d29" },
        					{ data: "d30" },
        					{ data: "d31" }
        				]
        			})
        			/* END TABLE */
        		}
        	});
        	/* END AJAX FOR LOAD DATA *///JS TIMESHEET UPLOAD//
           $('#btUpload').on("click", function(){
                // debugger;
                var formData = new FormData($("#tsUploadForm")[0]);
                var fileName = $('#tsFile').val();

                alert(fileName);
                
                $.ajax({
                    url : baseUrl+"/transaction/timesheet/upload",
                    method : "POST",
                    data   : formData,
                    success : function(data){
                      alert(data);
                    }
                });

          })
           /* END AJAX FOR LOAD DATA *///JS MANUAL UPLOAD//
           $('#mnlUpload').on("click", function(){
                // debugger;
                var formData = new FormData($("#mnlUploadForm")[0]);
                var fileName = $('#mnlFile').val();

                alert(fileName);
                
                $.ajax({
                    url : baseUrl+"/transaction/Tr_manual_value/upload",
                    method : "POST",
                    data   : formData,
                    success : function(data){
                      alert(data);

                      setTimeout(function () {
                        window.location.href = '<?php echo base_url() ?>'+'/Transaction/tr_manual_value';
                      }, 2000);
                    }
                });

          })
        });
      </script>
