      <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">
      	 	  <h3 class="tile-title">Contract Update</h3>
      	 	  <div class="tile-body">
        		  <!-- Check Your Valid URL -->
              <form class="form-horizontal" method="POST" action="../updData">
                <div class="form-group row">
                  <label class="control-label col-md-2">Biodata Id</label>
                  <div class="col-md-3">
                    <select class="form-control" name="biodataId" id="biodataId">
                      <option value="" disabled="" selected="">Choose</option>
                      <?php 
                      foreach ($data_biodata as $key => $value) {
                        if($mtContract['biodata_id']==$value->biodata_id){
                          echo '<option value="'.$value->biodata_id.'">'.$value->full_name.' - '.$value->biodata_id.' </option>';
                        }else{
                          echo '<option value="'.$value->biodata_id.'" selected>'.$value->full_name.' - '.$value->biodata_id.' </option>';
                        }
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Badge No</label>
                  <div class="col-md-3">
                    <input class="form-control" name="badgeNo" id="badgeNo" type="text" value="<?php echo $mtContract['badge_no'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Contract No</label>
                  <div class="col-md-3">
                    <input class="form-control" name="contractNo" id="contractNo" type="text" value="<?php echo $mtContract['contract_no'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Dept</label>
                  <div class="col-md-3">
                    <input class="form-control" name="dept" id="dept" type="text" value="<?php echo $mtContract['dept'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Job Position</label>
                  <div class="col-md-3">
                    <input class="form-control" name="jobPosition" id="jobPosition" type="text" value="<?php echo $mtContract['job_position'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Contract Start</label>
                  <div class="col-md-2">
                    <input class="form-control" name="contractStart" id="contractStart" type="date" value="<?php echo $mtContract['contract_start'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Contract End</label>
                  <div class="col-md-2">
                    <input class="form-control" name="contractEnd" id="contractEnd" type="date" value="<?php echo $mtContract['contract_end'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Remarks</label>
                  <div class="col-md-5">
                    <input class="form-control" name="remarks" id="remarks" type="text" value="<?php echo $mtContract['remarks'] ?>">
                  </div>
                </div>
              </form>
      	 	    <form class="row" method="POST" action="../updData">
              <div class="form-group col-md-3">
                <label class="control-label">Contract Id</label>
                <input class="form-control" name="contractId" id="contractId" type="text" value="<?php echo $mtContract['contract_id'] ?>" disabled="">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Biodata Id</label>
                <input class="form-control" name="biodataId" id="biodataId" type="text" value="<?php echo  ?>">
              </div>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div class="tile-footer">
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
      	 	    &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/mt_contract/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
      	 	    <strong>
      	 	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	 	      <span style="color: red" class="errSaveMess"></span>
      	 	    </strong>
      	 	  </div>
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- js load file is here  -->
      <!-- <script src="../assets/js/main.js"></script>  -->
      <script>
        $(document).ready(function() {
          $("#contractId").focus();

          $('.errSaveMess').html('');
          $("#dbSave").on("click", function(){
            
            // var isActive = "Y";
            // if ($('#isActive').is(":checked"))
            // { 
            //   isActive = "Y";
            // }              
            // else
            // {
            //   isActive = "T";
            // }

            let contractId = $('#contractId').val();
             // let contractId = ("#contractId").val();
             let biodataId  = $('#biodataId').val();
             let badgeNo    = $('#badgeNo').val();
             let contractNo = $('#contractNo').val();
             let dept       = $('#dept').val();
             let jobPosition = $('#jobPosition').val();
             let contractStart = $('#contractStart').val();
             let contractEnd = $('#contractEnd').val();
             // let contractCounter = ("#contractCounter").val();
             // let isClose = ("#isClose").val();
             // let isActive = $('#isActive').val();
             let remarks = $('#remarks').val();
             let inputTime = $('#inputTime').val();
             let picInput = $('#picInput').val();
             // let editTime = ("#editTime").val();
             // let picEdit = ("#picEdit").val();

             if(contractId.trim() == "")
             {
               $("#contractId").focus();
               $(".errSaveMess").html("Contract Id cannot be empty");
             }
             else if(biodataId.trim() == "")
             {
               $("#biodataId").focus();
               $(".errSaveMess").html("Biodata Id cannot be empty");
             }
             else if(badgeNo.trim() == "")
             {
               $("#badgeNo").focus();
               $(".errSaveMess").html("Badge No cannot be empty");
             }
             else if(contractNo.trim() == "")
             {
               $("#contractNo").focus();
               $(".errSaveMess").html("Contract No cannot be empty");
             }
             else if(dept.trim() == "")
             {
               $("#dept").focus();
               $(".errSaveMess").html("Dept cannot be empty");
             }
             else if(jobPosition.trim() == "")
             {
               $("#jobPosition").focus();
               $(".errSaveMess").html("Job Position cannot be empty");
             }
             else if(contractStart.trim() == "")
             {
               $("#contractStart").focus();
               $(".errSaveMess").html("Contract Start cannot be empty");
             }
             else if(contractEnd.trim() == "")
             {
               $("#contractEnd").focus();
               $(".errSaveMess").html("Contract End cannot be empty");
             }
             // else if(contractCounter.trim() == "")
             // {
             //   $("#contractCounter").focus();
             //   $(".errSaveMess").html("Contract Counter cannot be empty");
             // }
             // else if(isClose.trim() == "")
             // {
             //   $("#isClose").focus();
             //   $(".errSaveMess").html("Is Close cannot be empty");
             // }
             // else if(isActive.trim() == "")
             // {
             //   $("#isActive").focus();
             //   $(".errSaveMess").html("Is Active cannot be empty");
             // }
             else if(remarks.trim() == "")
             {
               $("#remarks").focus();
               $(".errSaveMess").html("Remarks cannot be empty");
             }
            
      	 	   /* Put URL your here */
             var myUrl ='<?php echo base_url() ?>/Master/Mt_contract/updData';
             $.ajax({
                url    : myUrl,
                method : "POST",
                data   : {
                   contractId : $("#contractId").val(),
                   biodataId : $("#biodataId").val(),
                   badgeNo : $("#badgeNo").val(),
                   contractNo : $("#contractNo").val(),
                   dept : $("#dept").val(),
                   jobPosition : $("#jobPosition").val(),
                   contractStart : $("#contractStart").val(),
                   contractEnd : $("#contractEnd").val(),
                   contractCounter : $("#contractCounter").val(),
                   isClose : $("#isClose").val(),
                   isActive : $("#isActive").val(),
                   remarks : $("#remarks").val(),
                   editTime : $("#editTime").val(),
                   picEdit : $("#picEdit").val()
                },
                success : function(data)
                {
      	 	         $.notify({
      	 	            title: "Information : ",
      	 	            message: "New data has been saved!",
      	 	            icon: "fa fa-check"
      	 	         },{
      	 	            type: "info"
      	 	         });
      	 	         /* Your redirect is here */
                   window.location.href = '<?php echo base_url() ?>'+'/Master/Mt_contract'; 
                }
             })
          });
        });
      </script>
