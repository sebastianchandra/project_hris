    <!-- FORM -->
    <div class="row">     
      <div class="col-md-12">
        <div class="tile">
          <h3 class="tile-title">Salary Edit</h3>
          <div class="tile-body">
            <form class="row" method="POST" action="<?php echo base_url() ?>/Master/Mt_salary/editData">
              <!-- Hidden Id -->
              <input class="form-control" name="salaryId" id="salaryId" type="hidden" value="<?php echo $salary01['salary_id'] ?>">
              <div class="form-group col-md-3">
                <label class="control-label">Salary Id</label>
                <input class="form-control" name="salaryId" id="salaryId" type="text" value="<?php echo $salary01['salary_id'] ?>" disabled="">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Employee Name</label>
                <select class="form-control" name="biodataId" id="biodataId">
                  <option value="" disabled="" selected="">Choose</option>
                  <?php 
                  foreach ($data_biodata as $key => $value) {
                  echo '<option value="'.$value->biodata_id.'">'.$value->full_name.' - '.$value->biodata_id.' </option>';
                  }
                  ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Payroll Number</label>
                <input class="form-control" id="badgeNo" name="badgeNo" type="text" value="<?php echo $salary01['badge_no'] ?>">
              </div>
              <!-- <div class="form-group col-md-3">
                <label class="control-label">Full Name</label>
                <input class="form-control" id="fullName" name="fullName" type="text" value="<?php //echo $salary01['full_name'] ?>" disabled="">
              </div> -->
              <div class="form-group col-md-3">
                <label class="control-label">Class</label>
                <select class="form-control" name="classId" id="classId">
                  <option value="" disabled="" selected="">Choose</option>
                  <?php 
                  foreach ($data_class as $key => $value) {
                  echo '<option value="'.$value->class_id.'">'.$value->class_base.' - '.$value->class_category.'</option>';
                  }
                  ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Local Bank Id</label>
                <select class="form-control" name="localBankId" id="localBankId">
                  <option value="" disabled="" selected="">Choose</option>
                  <?php 
                  foreach ($data_bank_local as $key => $value) {
                  echo '<option value="'.$value->bank_id.'">'.$value->bank_id.' - '.$value->bank_name.' </option>';
                  }
                  ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Foreign Bank Id</label>
                <select class="form-control" name="foreignBankId" id="foreignBankId">
                  <option value="" disabled="" selected="">Choose</option>
                  <?php 
                  foreach ($data_bank_foreign as $key => $value) {
                  echo '<option value="'.$value->bank_id.'">'.$value->bank_id.' - '.$value->bank_name.' </option>';
                  }
                  ?>
                </select>
              </div>
              <div class="form-group col-md-12"> 
                <label class="control-label">Is Active</label>
                <!-- <input type="checkbox" id="is_renewal" name="isrenewal" value="1" checked=""> -->
                <!-- <input type="hidden" id="isActive" name="isActive" value="1" > -->
                <input type="checkbox" id="isActive" name="isActive" value="0">
              </div> 
              <!-- <div class="form-group col-md-3">
                <label class="control-label">Currency</label>
                <input class="form-control" type="text" disabled="" id="classCurrency" placeholder="Currency" value="<?php //echo $salary01['currency'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Base Wage</label>
                <input class="form-control" type="text" disabled="" id="classWage" placeholder="Base Wage" value="<?php //echo $salary01['base_wage'] ?>">
              </div> -->
              <!-- <div class="form-group col-md-12">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Submit</button>
              </div> -->
            </form>
          </div>
          <div class="tile-footer">
            <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/mt_salary/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
          </div>
        </div>
      </div>
    </div>

    <!-- This Line Must Have in Every Page Content -->
    <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>  
    <script>
      $('#demoSelect').select2();
      var baseUrl = '<?php echo base_url()?>';
      var dataList = []; 

      // $("#biodataId").select2({
      // placeholder: 'Select a Biodata Id',
      // allowClear: true});

      // $("#classId").select2().on('select2:select',function(e){//untuk change box
      // // debugger
      //   var classWage     = $('#classId option:selected').attr('data-class');
      //   var classCurrency = $('#classId option:selected').attr('kind-class'); 
      //   $('#classCurrency').val(classCurrency);//#ngambil dari IDnya////val kalau kosong mengambil, kalau diisi mengisi//
      //   $('#classWage').val(classWage);
      // });

      var msSalaryBiodata = "<?php echo $salary01['biodata_id'] ?>";
      var msClassId       = "<?php echo $salary01['class_id'] ?>";
      var msLocalBankId   = "<?php echo $salary01['local_bank_id'] ?>";
      var msForeignBankId = "<?php echo $salary01['foreign_bank_id'] ?>";

        if( (msSalaryBiodata != "") && (msSalaryBiodata != undefined) )
        {
          $('#biodataId option[value="'+msSalaryBiodata+'"]').attr('selected','selected');
        }
        if( (msClassId != "") && (msClassId != undefined) )
        {
          $('#classId option[value="'+msClassId+'"]').attr('selected','selected');
        }
        if( (msLocalBankId != "") && (msLocalBankId != undefined) )
        {
          $('#localBankId option[value="'+msLocalBankId+'"]').attr('selected','selected');
        }
        if( (msForeignBankId != "") && (msForeignBankId != undefined) )
        {
          $('#foreignBankId option[value="'+msForeignBankId+'"]').attr('selected','selected');
        }

        var baseUrl = '<?php echo base_url()?>';
        var dataList = []; 
        $(document).ready(function() {
          $("#salaryId").focus();

          var myUrl = '<?php echo base_url() ?>/Master/Mt_salary/updData'; 
          $("#dbSave").on("click", function(){

            var isActive = "Y";
            if ($('#isActive').is(":checked"))
            { 
              isActive = "Y";
            }              
            else
            {
              isActive = "T";
            }

            $.ajax({
              url: myUrl,
              type : "POST",  
              data: {
                salaryId        : $('#salaryId').val(),
                biodataId       : $('#biodataId').val(),
                // biodataKind     : $('#biodataId option:selected').attr('data-biodata'),
                classId         : $('#classId').val(),
                badgeNo         : $('#badgeNo').val(),
                localBankId     : $('#localBankId').val(),
                foreignBankId   : $('#foreignBankId').val(),
                isActive
              },

                success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status == false) {
                  $.notify({
                    message: 'Data Gagal disimpan'
                  },{
                    type: 'danger'
                  });
                  return false;
                }
                else
                  {
                    alert('Data Berhasil di Update.');

                      setTimeout(function () {
                        window.location.href = '<?php echo base_url() ?>'+'/Master/Mt_salary'; 
                      }, 2000);
                  }
              }
            });
          });
        });











    </script>