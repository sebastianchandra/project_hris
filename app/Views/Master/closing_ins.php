      <div class="app-title">
        <div>
          <h1>Process Master Closing</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master Closing</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
            <!-- <a href="<?= base_url('Master/Mt_process_closing/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a> -->
          </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">
      	 	  <div class="tile-body">
      	 	    <form class="form-horizontal" method="POST" action="../insData">
                <div class="form-group row">
                  <label class="control-label col-md-2">Year Close</label>
                  <div class="col-md-2">
                    <select class="form-control " id="yearClose" name="yearClose" required="">
                      <option value="" disabled="" selected="">Pilih</option>
                      <script type="text/javascript">
                        var dt = new Date();
                        var currYear = dt.getFullYear();
                        var currMonth = dt.getMonth();
                        var currDay = dt.getDate();
                        var tmpDate = new Date(currYear + 2, currMonth, currDay);

                        var startYear = tmpDate.getFullYear();
                        var endYear = startYear - 50;             
                        for (var i = startYear; i >= endYear; i--) 
                        {
                          document.write("<option value='"+i+"'>"+i+"</option>");           
                        }
                      </script>
                    </select>
                    <input type="hidden" id="isActive" value="1">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Month Close</label>
                  <div class="col-md-2">
                    <select class="form-control " id="monthClose" name="monthClose" required="">
                      <option value="" disabled="" selected="">Pilih</option>
                      <script type="text/javascript">
                        var tMonth = 1;
                        for (var i = tMonth; i <= 12; i++) 
                        {
                          if(i < 10)
                          {
                            document.write("<option value='0"+i+"'>0"+i+"</option>");             
                          }
                          else
                          {
                            document.write("<option value='"+i+"'>"+i+"</option>");               
                          }
                          
                        }
                      </script>
                    </select> 
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Period No</label>
                  <div class="col-md-2">
                    <select class="form-control" name="periodNo" id="periodNo">
                      <option value="" disabled="" selected="">Choose</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                    </select> 
                  </div>
                </div>
      	 	    </form>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div class="tile-footer">
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>&nbsp;&nbsp;&nbsp;
      	 	    <a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/mt_process_closing/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
      	 	    <strong>
      	 	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	 	      <span style="color: red" class="errSaveMess"></span>
      	 	    </strong>
      	 	  </div>
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
          var baseUrl = '<?php echo base_url()?>';
          $("#closingId").focus();
          $("#dbSave").on("click", function(){
             // let closingId = $("#closingId").val();
             let yearClose = $("#yearClose").val();
             let monthClose = $("#monthClose").val();
             let isActive = $("#isActive").val();
             let periodNo = $("#periodNo").val();
             // let inputTime = $('#inputTime').val();
             // let picInput = $('#picInput').val();
             $(".errSaveMess").html("");
             // if(closingId.trim() == "")
             // {
             //   $("#closingId").focus();
             //   $(".errSaveMess").html("Close Id cannot be empty");
             // }
             if(yearClose.trim() == "")
             {
               $("#yearClose").focus();
               $(".errSaveMess").html("Year cannot be empty");
             }
             else if(monthClose.trim() == "")
             {
               $("#monthClose").focus();
               $(".errSaveMess").html("Month cannot be empty");
             }
            
      	 	   /* ***Put URL your here */
             var myUrl ='<?php echo base_url() ?>/Master/Mt_process_closing/insData';

             // var isLocal = "Y";
              // if ($('#isLocal').is(":checked"))
              // { 
              //   isLocal = "Y";
              // }              
              // else
              // {
              //   isLocal = "T";
              // }

             $.ajax({
                url    : myUrl,
                method : "POST",
                data   : {
                   // closingId : $("#closingId").val(),
                   yearClose : $("#yearClose").val(),
                   monthClose : $("#monthClose").val(),
                   periodNo : $("#periodNo").val(),
                   isActive

                },
                success : function(data)
                {
      	 	         $.notify({
      	 	            title: "Information : ",
      	 	            message: "New data has been saved!",
      	 	            icon: "fa fa-check"
      	 	         },{
      	 	            type: "info"
      	 	         });
      	 	         /* Your redirect is here */
                   window.location.href = '<?php echo base_url() ?>'+'/Master/Mt_process_closing';
                }
             })
          });
        });
      </script>
