    <!-- FORM -->
    <div class="app-title">
      <div>
        <h1>Edit Master Class</h1>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Master</li>
          <li class="breadcrumb-item">Master Class</li>
        </ul>
      </div>
      <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?= base_url('master/Mt_class/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a>
        </li>
      </ul>
    </div>
    <div class="row">     
      <div class="col-md-12">
        <div class="tile">
          <div class="tile-body">
            <form class="form-horizontal" method="POST" action="../insData">
              <!-- Hidden Id -->              
              <div class="form-group row">
                <label class="control-label col-md-2">Code</label>
                <div class="col-md-2">
                  <input class="form-control" id="classCode" name="classCode" type="text" value="<?php echo $class01['class_code'] ?>">
                  <input class="form-control" name="classId" id="classId" type="hidden" value="<?php echo $class01['class_id'] ?>">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-2">Name</label>
                <div class="col-md-3">
                  <input class="form-control" id="className" name="className" type="text" value="<?php echo $class01['class_name'] ?>">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-2">Class Base</label>
                <div class="col-md-2">
                  <select class="form-control" name="classBase" id="classBase">
                    <option value="" disabled="" selected="">Choose</option>
                    <option value="STAFF">Staff</option>
                    <option value="HOURLY">Hourly</option>
                  </select> 
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-2">Base Wage</label>
                <div class="col-md-4">
                  <input class="form-control" id="baseWage" name="baseWage" type="text" value="<?php echo $class01['base_wage'] ?>">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-2">Production Bonus</label>
                <div class="col-md-4">
                  <input class="form-control" id="prodBonus" name="prodBonus" type="text" value="<?php echo $class01['prod_bonus'] ?>">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-2">Variabel Bonus</label>
                <div class="col-md-4">
                  <input class="form-control" id="varBonus" name="varBonus" type="text" value="<?php echo $class01['var_bonus'] ?>">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-2">Pension</label>
                <div class="col-md-4">
                  <input class="form-control" id="pension" name="pension" type="text" value="<?php echo $class01['pension'] ?>">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-2">Vacation Sup</label>
                <div class="col-md-4">
                  <input class="form-control" id="vacationSup" name="vacationSup" type="text" value="<?php echo $class01['vacation_sup'] ?>">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-2">Class Category</label>
                <div class="col-md-3">
                  <select class="form-control" name="classCategory" id="classCategory">
                    <option value="" disabled="" selected="">Choose</option>
                    <option value="CAD">CAD</option>
                    <option value="CAD-H">CAD-H</option>
                    <option value="USD">USD</option>
                    <option value="USD-H">USD-H</option>
                    <option value="PHP-H">PHP-H</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-2">Currency</label>
                <div class="col-md-2">
                  <select class="form-control" name="currency" id="currency">
                    <option value="" disabled="" selected="">Choose</option>
                    <option value="USD">USD</option>
                    <option value="AUD">AUD</option>
                    <option value="CAD">CAD</option>
                  </select> 
                </div>
              </div>
              
              <!-- <div class="form-group col-md-12">
              <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Submit</button>
              </div> -->
            </form>
          </div>
          <div class="tile-footer">
            <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>&nbsp;&nbsp;&nbsp;
            <a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/mt_class/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
          </div>
        </div>
      </div>
    </div>

    <!-- This Line Must Have in Every Page Content -->
    <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>  
    <script>
      $('#demoSelect').select2();
      var baseUrl = '<?php echo base_url()?>';
      var dataList = []; 

      $(document).ready(function() {
        $('#classId').focus();

        mtclass = {
            init : function()
            {
                
                var mtClassCategory = "<?php echo $class01['class_category'] ?>";
                var mtClassCurrency = "<?php echo $class01['currency'] ?>";
                var mtClassBase     = "<?php echo $class01['class_base'] ?>";

                  if( (mtClassCategory != "") && (mtClassCategory != undefined) ){
                  $('#classCategory option[value="'+mtClassCategory+'"]').attr('selected','selected');
                  }
                  if( (mtClassCurrency != "") && (mtClassCurrency != undefined) ){
                  $('#currency option[value="'+mtClassCurrency+'"]').attr('selected','selected');
                  }
                  if( (mtClassBase != "") && (mtClassBase != undefined) ){
                  $('#classBase option[value="'+mtClassBase+'"]').attr('selected','selected');
                  }

                var myUrl = '<?php echo base_url() ?>/Master/Mt_class/editData'; 
                $("#dbSave").on("click", function(){
                  
                  var isActive = "Y";
                  if ($('#isActive').is(":checked"))
                  { 
                    isActive = "Y";
                  }              
                  else
                  {
                    isActive = "T";
                  }


                  $.ajax({
                    url: myUrl,
                    type : "POST",  
                    data: {
                      classId        : $('#classId').val(),
                      classCode      : $('#classCode').val(),
                      className      : $('#className').val(),
                      classBase      : $('#classBase').val(),
                      baseWage       : $('#baseWage').val(),
                      prodBonus      : $('#prodBonus').val(),
                      varBonus       : $('#varBonus').val(),
                      pension        : $('#pension').val(),
                      vacationSup    : $('#vacationSup').val(),
                      classCategory  : $('#classCategory').val(),
                      currency       : $('#currency').val(),
                      isActive
                    },
                    success : function(resp){
                      if(resp.status == 'ERROR INSERT' || resp.status == false) {
                        toastr.success("Data has been Delete.", 'Alert', {"positionClass": "toast-top-center"});
                        return false;
                      }else{
                        toastr.success("Data has been Save.", 'Alert', {"positionClass": "toast-top-center"});

                        setTimeout(function () {
                          window.location.href = '<?php echo base_url() ?>'+'/Master/Mt_class';
                        }, 2000);
                      }
                    }
                  });
                });
            },
          }
          mtclass.init();
       });











    </script>