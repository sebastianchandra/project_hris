      <div class="app-title">
        <div>
          <h1>Update Master Department</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master Department</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
            <!-- <a href="<?= base_url('master/biodata/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a> -->
          </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">
      	 	  <h3 class="tile-title">Dept Update</h3>
      	 	  <div class="tile-body">
        		  <!-- Check Your Valid URL -->
              <form class="form-horizontal" method="POST" action="../insData">
                <div class="form-group row">
                  <label class="control-label col-md-2">Department Code</label>
                  <div class="col-md-2">
                    <input class="form-control" name="deptCode" id="deptCode" type="text" placeholder="Dept Code" value="<?php echo $mtDept['dept_code'] ?>">
                    <input class="form-control" name="deptId" id="deptId" type="hidden" value="<?php echo $mtDept['dept_id'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Department Name</label>
                  <div class="col-md-3">
                    <input class="form-control" name="deptName" id="deptName" type="text" placeholder="Dept Name" value="<?php echo $mtDept['dept_name'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Is Active</label>
                  <div class="col-md-3">
                    <input type="checkbox" id="isActive" name="isActive" value="1" <?= ($mtDept['is_active']==1)? 'checked=""' : ''; ?>>
                  </div>
                </div>
              </form>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div class="tile-footer">
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
              <a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/mt_dept/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
      	 	    <!-- &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
      	 	    <strong>
      	 	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	 	      <span style="color: red" class="errSaveMess"></span>
      	 	    </strong>
      	 	  </div>
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
          var baseUrl = '<?php echo base_url()?>';
          $("#deptId").focus();
          $("#dbSave").on("click", function(){
             // let bankId   = $("#bankId").val();
             let deptCode = $("#deptCode").val();
             let deptId = $("#deptId").val();
             let deptName = $("#deptName").val();
             let isActive = $("#isActive").val();
             let picEdit  = $("#picEdit").val();
             let editTime = $("#editTime").val();
             $(".errSaveMess").html("");
             if(deptCode.trim() == "")
             {
               $("#deptCode").focus();
               $(".errSaveMess").html("Dept Code cannot be empty");
             }
             else if(deptName.trim() == "")
             {
               $("#deptName").focus();
               $(".errSaveMess").html("Dept Name cannot be empty");
             }
             // else if(isLocal.trim() == "")
             // {
             //   $("#isLocal").focus();
             //   $(".errSaveMess").html("Is Local cannot be empty");
             // }
      	 	  /* ***Put URL your here */
             var myUrl ='<?php echo base_url() ?>/Master/Mt_dept/editData';

             // var isLocal = "Y";
             if ($('#isActive').is(":checked"))
              { 
                isActive = "Y";
              }              
              else
              {
                isActive = "T";
              }

             $.ajax({
                url    : myUrl,
                method : "POST",
                data   : {
                   deptId    : $("#deptId").val(),
                   deptCode    : $("#deptCode").val(),
                   deptName  : $("#deptName").val(),
                   // bankName  : $("#bankName").val(),
                   isActive,
                   picEdit   : $("#picEdit").val(),
                   editTime  : $("#editTime").val()
                },
                success : function(resp)
                {
      	 	        if(resp.status == 'ERROR INSERT' || resp.status == false) {
                    toastr.success("Data has been Delete.", 'Alert', {"positionClass": "toast-top-center"});
                    return false;
                  }else{
                    toastr.success("Data has been Save.", 'Alert', {"positionClass": "toast-top-center"});

                    setTimeout(function () {
                      window.location.href = '<?php echo base_url() ?>'+'/Master/Mt_class';
                    }, 2000);
                  }
                }
             })
          });
        });
      </script>
