      <div class="app-title">
        <div>
          <h1>Input Master Statutory Date</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master Statutory Date</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
            <!-- <a href="<?= base_url('Master/Mt_st_date/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a> -->
          </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">
      	 	  <div class="tile-body">
        		  <form class="form-horizontal" method="POST" action="../insData">
                <div class="form-group row">
                  <label class="control-label col-md-2">Date</label>
                  <div class="col-md-1">
                    <select class="form-control " id="msdDate" name="msdDate" required="">
                        <option value="" disabled="" selected="">Pilih</option>
                        <script type="text/javascript">         
                          var attendance = 1;
                          for (var i = attendance; i <= 31; i++){
                            if(i < 10){
                            document.write("<option value='0"+i+"'>0"+i+"</option>");             
                            }else{
                              document.write("<option value='"+i+"'>"+i+"</option>");               
                            }
                          }
                        </script>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Month</label>
                  <div class="col-md-1">
                    <select class="form-control " id="msdMonth" name="msdMonth" required="">
                      <option value="" disabled="" selected="">Pilih</option>
                      <script type="text/javascript">
                        var tMonth = 1;
                        for (var i = tMonth; i <= 12; i++) 
                        {
                          if(i < 10)
                          {
                            document.write("<option value='0"+i+"'>0"+i+"</option>");             
                          }
                          else
                          {
                            document.write("<option value='"+i+"'>"+i+"</option>");               
                          }
                          
                        }
                      </script>
                    </select> 
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Remarks</label>
                  <div class="col-md-5">
                    <input class="form-control" name="remarks" id="remarks" type="text" placeholder="Remarks">
                  </div>
                </div>
      	 	    </form>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div class="tile-footer">
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>&nbsp;&nbsp;&nbsp;
      	 	    <a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/mt_st_date/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
      	 	    <strong>
      	 	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	 	      <span style="color: red" class="errSaveMess"></span>
      	 	    </strong>
      	 	  </div>
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
          var baseUrl = '<?php echo base_url()?>';
          $("#msdId").focus();
          $("#dbSave").on("click", function(){
             // let closingId = $("#closingId").val();
             let msdDate = $("#msdDate").val();
             let msdMonth = $("#msdMonth").val();
             let remarks = $("#remarks").val();
             // let inputTime = $('#inputTime').val();
             // let picInput = $('#picInput').val();
             $(".errSaveMess").html("");
             // if(closingId.trim() == "")
             // {
             //   $("#closingId").focus();
             //   $(".errSaveMess").html("Close Id cannot be empty");
             // }
             if(msdDate.trim() == "")
             {
               $("#msdDate").focus();
               $(".errSaveMess").html("Date cannot be empty");
             }
             else if(msdMonth.trim() == "")
             {
               $("#msdMonth").focus();
               $(".errSaveMess").html("Month cannot be empty");
             }
            
      	 	   /* ***Put URL your here */
             var myUrl ='<?php echo base_url() ?>/Master/Mt_st_date/insData';

             // var isLocal = "Y";
              // if ($('#isLocal').is(":checked"))
              // { 
              //   isLocal = "Y";
              // }              
              // else
              // {
              //   isLocal = "T";
              // }

             $.ajax({
                url    : myUrl,
                method : "POST",
                data   : {
                   // closingId : $("#closingId").val(),
                   msdDate : $("#msdDate").val(),
                   msdMonth : $("#msdMonth").val(),
                   remarks : $("#remarks").val()
                },
                success : function(data)
                {
      	 	        toastr.success("Data has been Save.", 'Alert', {"positionClass": "toast-top-center"});
                   /* Your redirect is here */
                  setTimeout(function () {
                    window.location.href = baseUrl+'/Master/Mt_st_date'; //will redirect to google.
                  }, 2000);
                }
             })
          });
        });
      </script>
