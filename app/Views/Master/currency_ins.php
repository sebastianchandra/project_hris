      <div class="app-title">
        <div>
          <h1>Input Master Currency</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master Currency</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
          </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">      	 	  <div class="tile-body">
        		  <!-- Check Your Valid URL -->
      	 	    <form class="form-horizontal" method="POST" action="../insData">
                <div class="form-group row">
                  <label class="control-label col-md-2">Currency Code</label>
                  <div class="col-md-1">
                    <input class="form-control" name="crCode" id="crCode" type="text" placeholder="Currency Code">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Currency Country</label>
                  <div class="col-md-2">
                    <input class="form-control" name="crCountry" id="crCountry" type="text" placeholder="Currency Country">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Rate Value</label>
                  <div class="col-md-2">
                    <input class="form-control" name="rateValue" id="rateValue" type="text" placeholder="Rate Value">
                  </div>
                </div>
      	 	    </form>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div class="tile-footer">
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
              <a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/Mt_currency_rate/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
      	 	    <!-- &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
      	 	    <strong>
      	 	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	 	      <span style="color: red" class="errSaveMess"></span>
      	 	    </strong>
      	 	  </div>
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
       <script>
        $(document).ready(function() {
          var baseUrl = '<?php echo base_url()?>';
          $("#crCode").focus();
          $("#dbSave").on("click", function(){
             // let bankId = $("#bankId").val();
             let crCode = $("#crCode").val();
             let crCountry = $("#crCountry").val();
             let rateValue = $("#rateValue").val();
             let picData = $('#picData').val();
             let dataTime = $('#dataTime').val();
             $(".errSaveMess").html("");
             if(crCode.trim() == "")
             {
               $("#crCode").focus();
               $(".errSaveMess").html("Currency Code cannot be empty");
             }
             else if(crCountry.trim() == "")
             {
               $("#crCountry").focus();
               $(".errSaveMess").html("Currency Country cannot be empty");
             }
             else if(rateValue.trim() == "")
             {
               $("#rateValue").focus();
               $(".errSaveMess").html("Rate Value cannot be empty");
             }
             // else if(bankName.trim() == "")
             // {
             //   $("#bankName").focus();
             //   $(".errSaveMess").html("Bank Name cannot be empty");
             // }
             // else if(isLocal.trim() == "")
             // {
             //   $("#isActive").focus();
             //   $(".errSaveMess").html("Is Local cannot be empty");
             // }
      	 	   /* ***Put URL your here */
             var myUrl ='<?php echo base_url() ?>/Master/Mt_currency_rate/insData';

             // var isActive = "Y";
              // if ($('#isActive').is(":checked"))
              // { 
              //   isActive = "Y";
              // }              
              // else
              // {
              //   isActive = "T";
              // }

             $.ajax({
                url    : myUrl,
                method : "POST",
                data   : {
                   // bankId : $("#bankId").val(),
                   crCode : $("#crCode").val(),
                   crCountry : $("#crCountry").val(),
                   rateValue : $("#rateValue").val(),
                   // isActive,
                   picData : $("#picData").val(),
                   dataTime : $("#dataTime").val()
                },
                success : function(data)
                {
      	 	        toastr.success("Data has been Save.", 'Alert', {"positionClass": "toast-top-center"});

                  setTimeout(function () {
                    window.location.href = baseUrl+'/master/Mt_currency_rate'; //will redirect to google.
                  }, 2000);
                }
             })
          });
        });
      </script>
