      <div class="app-title">
        <div>
          <h1>View Master Statutory Date</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master Statutory Date</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?= base_url('Master/Mt_st_date/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a>
          </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
           <div class="tile-body">
           	<!-- TABLE -->
           	<div class="table-responsive">
           	  <table class="table table-striped table-bordered" id="mtStDate">
           	    <thead style="background-color: rgb(13 81 198);color: white;">
           	     <tr>
           	       <!-- <th>Statutory Id</th> -->
           	       <th>Date</th>
           	       <th>Month</th>
                   <th>Remarks</th>
                   <th>Edit</th>

           	     </tr>
           	    </thead>
           	    <tbody>
           	     <!-- <tr> -->
           	      <!-- <td>contract_id</td> -->
           	      <!-- <td>biodata_id</td> -->
           	      <!-- <td>badge_no</td> -->
           	      <!-- <td>contract_no</td> -->
           	      <!-- <td>dept</td> -->
           	      <!-- <td>job_position</td> -->
           	      <!-- <td>contract_start</td> -->
           	      <!-- <td>contract_end</td> -->
           	      <!-- <td>contract_counter</td> -->
           	      <!-- <td>is_close</td> -->
           	      <!-- <td>is_active</td> -->
           	      <!-- <td>remarks</td> -->
           	      <!-- <td>input_time</td> -->
           	      <!-- <td>pic_input</td> -->
           	      <!-- <td>edit_time</td> -->
           	      <!-- <td>pic_edit</td> -->
           	      <!-- <td>Link Edit</td> -->
           	     <!-- </tr> -->
           	    </tbody>
           	  </table>
           	</div>
           </div>
          </div> <!-- class="tile" -->
        </div> <!-- class="col-md-12" -->
      </div> <!-- class="row" -->
      <!-- js load file is here  -->
      <!-- <script src="../assets/js/main.js"></script>  -->
      <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>
      <script>
      $('#demoSelect').select2();
      var baseUrl  = '<?php echo base_url()?>';
      var dataList = [];

        $(document).ready(function() {
        	/* START AJAX FOR LOAD DATA */
        	$.ajax({
        		/* Url is here */
        		url : baseUrl+'/master/Mt_st_date/getAll',
        		method : "POST",
        		success : function(data)
        		{
        			let srcData = JSON.parse(data);
        			/* Edit Url Controller is here */
        			let updUrl = '<?php echo base_url(); ?>/master/mt_st_date/upd_view/'
        			/* START TABLE */
        			let mtStDate = $("#mtStDate").DataTable({
        				"paging":   true,
        				"ordering": true,
        				"info":     true,
        				"filter":   false,
        				"autoWidth": false,
        				"columnDefs": [
        								{
        									/* Hide Table Id */
        									// "targets": [0],
        									// "visible": false,
        									// "searchable": false
        								},
        								{
        									/* Column For Edit Link, (ex : 5) depend on last column no */
        									"targets": 3,
        									"data": "download_link",
        									"render": function ( data, type, row, meta ) {
        									  return '<a href="'+updUrl+row['msd_id']+'" class="btn btn-sm btn-warning">Edit</a> <button class="btn btn-sm btn-danger" type="button" id="delete" data-id="'+row['msd_id']+'">Delete</button>';
        									}
        								}
        				],
        				data : srcData,
        				columns: [
        					// { data: "msd_id" },
        					{ data: "msd_date" },
        					{ data: "msd_month" },
                  { data: "remarks" }
        				]
        			})
        			/* END TABLE */
        		}
        	});
          $('#mtStDate').on('click','#delete', function (e) {
            var v_id      = $(this).data('id');

            toastr.warning(
                'Do you want to delete this row ?<br /><br />'+
                '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-id="'+v_id+'">Yes</button> '+
                '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
                '<u>ALERT</u>', 
                {
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "closeButton": false,
                }
            );
          });
        	/* END AJAX FOR LOAD DATA */
        });

        function deleteRow(e){
          var id  = $(e).data('id');

          $.ajax({
            data: {
              id  : id
            },
            type : "POST",
            url: baseUrl+'/Master/Mt_st_date/del',
            success : function(resp){

              if(resp.status == 'ERROR INSERT' || resp.status == false) {
                toastr.success('Data not saved successfully', 'Alert', {"positionClass": "toast-top-center"});
                return false;

              } else {
                toastr.success("Data has been Delete.", 'Alert', {"positionClass": "toast-top-center"});

                setTimeout(function () {
                  window.location.href = baseUrl+'/master/Mt_st_date'; //will redirect to google.
                }, 2000);
              }
            }
          });
        }
      </script>
