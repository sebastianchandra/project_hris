      <!-- FORM -->
      <div class="app-title">
        <div>
          <h1>View Master Class</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master Class</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?= base_url('Master/Mt_class/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a>
          </li>
        </ul>
      </div>
      <div class="row">    	
		    <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">              
              <!-- TABLE -->
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="MtClassTable">
                  <thead style="background-color: rgb(13 81 198);color: white;">
                    <tr>
                      <th>Class Code</th>
                      <th>Class Name</th>
                      <th>Class Base</th>
                      <th>Base Wage</th>
                      <th>Production Bonus</th>
                      <th>Var Bonus</th>
                      <th>Pension</th>
                      <th>Vacation</th>
                      <th>Class Category</th>
                      <th>Currency</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <!-- <tr>
                      <td>Tiger Nixon</td>
                      <td>System Architect</td>
                      <td>Edinburgh</td>
                      <td>61</td>
                      <td>61</td>
                      <td>61</td>                      
                    </tr> -->                    
                  </tbody>
                </table>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <!-- <a href='<?php echo base_url(); ?>/master/Mt_class/upd_view/'></a> -->

       <!-- This Line Must Have in Every Page Content -->
    <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>  
    <script>
      $('#demoSelect').select2();
      var baseUrl = '<?php echo base_url()?>';
      var dataList = [];

      var MtClassTable = null;
      $(document).ready(function() {

        mtclass = {
            init : function()
            {
              $.ajax({
                // url    : baseUrl+'/General/Admin/Users/getAll',
                url    : baseUrl+'/master/Mt_class/getAll',
                method : "POST",
                success : function(data)
                {
                  // debugger
                  let srcData = JSON.parse(data);
                  let updUrl = '<?php echo base_url(); ?>/master/mt_class/upd_view/';

                  MtClassTable = $('#MtClassTable').DataTable({
                       "paging":   true,
                       "ordering": true,
                       "info":     true,
                       "filter":   false ,
                       "autoWidth": false, 
                       "columnDefs": [
                                      {
                                          // "targets": [0],
                                          // "visible": false,
                                          // "searchable": false
                                      },
                                      {
                                            "targets": 10,
                                            "data": "download_link",
                                            "render": function ( data, type, row, meta ) {
                                              return '<a href="'+updUrl+row['class_id']+'" class="btn btn-sm btn-warning">Edit</a> <button class="btn btn-sm btn-danger" type="button" id="delete" data-id="'+row['class_id']+'">Delete</button>';
                                            }
                                                      
                                      }
                                                                   
                                  ],
                        data : srcData,
                        columns: [
                            { data: 'class_code' },
                            { data: 'class_name' },
                            { data: 'class_base' },
                            { data: 'base_wage' },
                            { data: 'prod_bonus' },
                            { data: 'var_bonus' },
                            { data: 'pension' },
                            { data: 'vacation_sup' },
                            { data: 'class_category' },
                            { data: 'currency' }
                        ]      
                   })                  
                }
              });

              $('#MtClassTable').on('click','#delete', function (e) {
                var v_id      = $(this).data('id');

                toastr.warning(
                    'Do you want to delete this row ?<br /><br />'+
                    '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-id="'+v_id+'">Yes</button> '+
                    '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
                    '<u>ALERT</u>', 
                    {
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "closeButton": false,
                    }
                );
              });

            },
          }
          mtclass.init();
       });

      function deleteRow(e){
        debugger
        var id  = $(e).data('id');

        $.ajax({
          data: {
            id  : id
          },
          type : "POST",
          url: baseUrl+'/master/Mt_class/del',
          success : function(resp){

            if(resp.status == 'ERROR INSERT' || resp.status == false) {
              toastr.success('Data not saved successfully', 'Alert', {"positionClass": "toast-top-center"});
              return false;

            } else {
              toastr.success("Data has been Delete.", 'Alert', {"positionClass": "toast-top-center"});

              setTimeout(function () {
                window.location.href = baseUrl+'/master/Mt_class'; //will redirect to google.
              }, 2000);
            }
          }
        });
      }
      
          
    </script>