      <div class="app-title">
        <div>
          <h1>View Master Closing</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master Closing</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?= base_url('Master/Mt_process_closing/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a>
          </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
           <div class="tile-body">
           	<!-- TABLE -->
           	<div class="table-responsive">
           	  <table class="table table-striped table-bordered" id="mtClosing">
           	    <thead style="background-color: rgb(13 81 198);color: white;">
           	     <tr>
           	       <th>Closing Id</th>
           	       <th>Year Closing</th>
                   <th>Month Closing</th>
           	       <th>Status</th>
           	       <th>Action</th>
           	     </tr>
           	    </thead>
           	    <tbody>
           	    </tbody>
           	  </table>
           	</div>
           </div>
          </div> <!-- class="tile" -->
        </div> <!-- class="col-md-12" -->
      </div> <!-- class="row" -->
      <!-- js load file is here  -->
      <!-- <script src="../assets/js/main.js"></script>  -->
      <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>
      <script>
      $('#demoSelect').select2();
      var baseUrl  = '<?php echo base_url()?>';
      var dataList = [];

        $(document).ready(function() {
        	/* START AJAX FOR LOAD DATA */
        	$.ajax({
        		/* Url is here */
        		url : baseUrl+'/master/Mt_process_closing/getAll',
        		method : "POST",
        		success : function(data)
        		{
        			let srcData = JSON.parse(data);
        			/* Edit Url Controller is here */
        			let updUrl = '<?php echo base_url(); ?>/master/mt_process_closing/upd_view/'
        			/* START TABLE */
        			let mtClosing = $("#mtClosing").DataTable({
        				"paging":   true,
        				"ordering": true,
        				"info":     true,
        				"filter":   false,
        				"autoWidth": false,
        				"columnDefs": [
        								{
        									/* Hide Table Id */
        									// "targets": [0],
        									// "visible": false,
        									// "searchable": false
        								},
                        {
                          /* Column For Edit Link, (ex : 5) depend on last column no */
                          "targets": 3,
                          "data": "download_link",
                          "render": function (data,type,row,meta){
                            if(row['is_active']==1){
                              return 'Close ';
                            }else{
                              return 'Open ';
                            }
                            
                          }
                        },
        								{
        									/* Column For Edit Link, (ex : 5) depend on last column no */
        									"targets": 4,
        									"data": "download_link",
        									"render": function (data,type,row,meta){
                            if(row['is_active']==1){
                              return '<button class="btn btn-sm btn-info" type="button" id="open" data-id="'+row['closing_id']+'">Open</button>';
                            }else{
                              return '<button class="btn btn-sm btn-danger" type="button" id="close" data-id="'+row['closing_id']+'">Close</button>';
                            }
        									  
        									}
        								}
        				],
        				data : srcData,
        				columns: [
        					{ data: "closing_id" },
        					{ data: "close_year" },
        					{ data: "close_month" }
        				]
        			})
        			/* END TABLE */
        		}
        	});
          $('#mtClosing').on('click','#open', function (e) {
            var v_id      = $(this).data('id');

            toastr.info(
                'Do you want to Open this Periode ?<br /><br />'+
                '<button type="button" id="okBtn" class="btn btn-danger" onclick="return actRow(this)" data-open="0" data-id="'+v_id+'">Yes</button> '+
                '<button type="button" id="noBtn" class="btn btn-warning">No</button>', 
                '<u>ALERT</u>', 
                {
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "closeButton": false,
                }
            );
          });
          $('#mtClosing').on('click','#close', function (e) {
            var v_id      = $(this).data('id');

            toastr.info(
                'Do you want to Close this Periode ?<br /><br />'+
                '<button type="button" id="okBtn" class="btn btn-danger" onclick="return actRow(this)" data-open="1" data-id="'+v_id+'">Yes</button> '+
                '<button type="button" id="noBtn" class="btn btn-warning">No</button>', 
                '<u>ALERT</u>', 
                {
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "closeButton": false,
                }
            );
          });
        	/* END AJAX FOR LOAD DATA */ 
        });

        function actRow(e){
          var id    = $(e).data('id');
          var open  = $(e).data('open');

          $.ajax({
            data: {
              id  : id,
              open: open
            },
            type : "POST",
            url: baseUrl+'/Master/Mt_process_closing/status_closing',
            success : function(resp){
              debugger
              if(resp==0){
                toastr.success("Data has been Open.", 'Alert', {"positionClass": "toast-top-center"});

                setTimeout(function () {
                  window.location.href = baseUrl+'/master/Mt_process_closing'; //will redirect to google.
                }, 2000);
              }else{
                toastr.success("Data has been Close.", 'Alert', {"positionClass": "toast-top-center"});

                setTimeout(function () {
                  window.location.href = baseUrl+'/master/Mt_process_closing'; //will redirect to google.
                }, 2000);
              }
            }
          });
        }
      </script>
