      <!-- FORM -->
      <div class="app-title">
        <div>
          <h1>View Master Allowance</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master Allowance</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?= base_url('master/allowance/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a>
          </li>
        </ul>
      </div>
      <div class="row">    	
		    <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">               
              <!-- TABLE -->
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="biodataTable">
                  <thead style="background-color: rgb(13 81 198);color: white;">
                    <tr>
                      <th>Company Name</th>
                      <th>Allowance Name</th>
                      <th>Formula</th>
                      <th>Tax</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <!-- <tr>
                      <td>Tiger Nixon</td>
                      <td>System Architect</td>
                      <td>Edinburgh</td>
                      <td>61</td>
                      <td>61</td>
                      <td>61</td>                      
                    </tr> -->                    
                  </tbody>
                </table>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <a href='<?php echo base_url(); ?>/master/biodata/upd_view/'></a>    
	
	  <!-- This Line Must Have in Every Page Content -->
	  <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>  
	  <script>
      $('#demoSelect').select2();
      var baseUrl = '<?php echo base_url()?>';
      var dataList = []; 
      // var biodataTable = $('#biodataTable').DataTable();
      var biodataTable = null;
	  	$(document).ready(function() {
          // debugger;
          // $('#userId').focus();          
	        var fruits = ["Banana", "Orange", "Apple", "Mango"];
          var ppl = {"firstName":"John", "lastName":"Doe"}
          biodata = {
            init : function()
            {
              $.ajax({
                // url    : baseUrl+'/General/Admin/Users/getAll',
                url    : baseUrl+'/master/allowance/getAll',
                method : "POST",
                success : function(data)
                {
                  // debugger;
                  let srcData = JSON.parse(data);
                  let updUrl = '<?php echo base_url(); ?>/master/allowance/upd_view/';
                  // var bioId = srcData[0]['biodata_id'];
                  // var t = sanitizeData(srcData);
                  // alert(srcData({'biodata_id'}));
                  // alert(JSON.stringify(srcData));
                  biodataTable = $('#biodataTable').DataTable({
                       "paging":   true,
                       "ordering": false,
                       "info":     false,
                       "filter":   false ,
                       "autoWidth": false, 
                       "columnDefs": [
                                      {
                                        // //Hide Table ID//
                                        //   "targets": [0],
                                        //   "visible": false,
                                        //   "searchable": false
                                      }
                                      ,
                                      {
                                           "targets": 4,
                                            "data": "download_link",
                                            "render": function ( data, type, row, meta ) {
                                              return '<a href="'+updUrl+row['ma_id']+'" class="btn btn-sm btn-warning">Edit</a> ';
                                            }
                                                      
                                      }
                                                                   
                                  ],  
                       data : srcData,
                       columns: [
                            { data: 'company_name' },
                            { data: 'ma_name' },
                            { data: 'ma_formula' },
                            { data: 'is_tax' }


                            // { data: 'biodata_id' }
                            // { data: "<a href='<?php #echo base_url(); ?>/master/biodata/upd_view/'><button type='button' class='btn btn-warning btEdit'><i class='fas fa-edit'></i></button></a>" }
                       ]  
                   })                  
                }
              });

              $('#biodataTable tbody').on('click','.btEdit', function(){
                  // educationTable.row( $(this).parents('tr') ).remove().draw();
                  let tIdx = biodataTable.row( $(this).parents('tr') ).index();
                  let tCount = biodataTable.rows().count();
                  let tRow = $('#biodataTable').DataTable().row(tIdx).data();
                  let tBioId = tRow['biodata_id'];
                  // window.location.href = '<?php #echo base_url() ?>'+'/master/biodata/upd_view/'+tBioId;  
                  // alert(tBioId);
              });
            },
          }
          biodata.init();
	     } );

       function sanitizeData(data) {
          // debugger;
          var d = [];
          Object.keys(data).forEach(function(key) {
            d.push(data[key]);
          });
          return d;
       } 


	  </script>
