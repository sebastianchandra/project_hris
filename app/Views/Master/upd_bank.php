      <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">
      	 	  <h3 class="tile-title">Bank Update</h3>
      	 	  <div class="tile-body">
        		  <!-- Check Your Valid URL -->
      	 	    <form class="row" method="POST" action="../updData">
              <div class="form-group col-md-3">
                <label class="control-label">Bank Id</label>
                <input class="form-control" name="bankId" id="bankId" type="text" value="<?php echo $mtBank['bank_id'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Bank Code</label>
                <input class="form-control" name="bankCode" id="bankCode" type="text" value="<?php echo $mtBank['bank_code'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Bank Name</label>
                <input class="form-control" name="bankName" id="bankName" type="text" value="<?php echo $mtBank['bank_name'] ?>">
              </div>
              <div class="form-group col-md-6"> 
                <label class="control-label">Is Local</label>
                <!-- <input type="checkbox" id="is_renewal" name="isrenewal" value="1" checked=""> -->
                 <!-- <input type="hidden" id="isActive" name="isActive" value="1" > -->
                <input type="checkbox" id="isLocal" name="isLocal" value="1">
              </div> 
      	 	    </form>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div class="tile-footer">
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
              <a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/mt_bank/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
      	 	    <!-- &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
      	 	    <strong>
      	 	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	 	      <span style="color: red" class="errSaveMess"></span>
      	 	    </strong>
      	 	  </div>
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
          var baseUrl = '<?php echo base_url()?>';
          $("#bankId").focus();
          $("#dbSave").on("click", function(){
             let bankId   = $("#bankId").val();
             let bankCode = $("#bankCode").val();
             let bankName = $("#bankName").val();
             let isLocal  = $("#isLocal").val();
             let picEdit  = $("#picEdit").val();
             let editTime = $("#editTime").val();
             $(".errSaveMess").html("");
             if(bankId.trim() == "")
             {
               $("#bankId").focus();
               $(".errSaveMess").html("Bank Id cannot be empty");
             }
             else if(bankCode.trim() == "")
             {
               $("#bankCode").focus();
               $(".errSaveMess").html("Bank Code cannot be empty");
             }
             else if(bankName.trim() == "")
             {
               $("#bankName").focus();
               $(".errSaveMess").html("Bank Name cannot be empty");
             }
             // else if(isLocal.trim() == "")
             // {
             //   $("#isLocal").focus();
             //   $(".errSaveMess").html("Is Local cannot be empty");
             // }
      	 	  /* ***Put URL your here */
             var myUrl ='<?php echo base_url() ?>/Master/Mt_bank/editData';

             // var isLocal = "Y";
             if ($('#isLocal').is(":checked"))
              { 
                isLocal = "Y";
              }              
              else
              {
                isLocal = "T";
              }

             $.ajax({
                url    : myUrl,
                method : "POST",
                data   : {
                   bankId    : $("#bankId").val(),
                   bankCode  : $("#bankCode").val(),
                   bankName  : $("#bankName").val(),
                   isLocal,
                   picEdit   : $("#picEdit").val(),
                   editTime  : $("#editTime").val()
                },
                success : function(data)
                {
      	 	         $.notify({
      	 	            title: "Information : ",
      	 	            message: "New data has been saved!",
      	 	            icon: "fa fa-check"
      	 	         },{
      	 	            type: "info"
      	 	         });
      	 	         /* Your redirect is here */
                   window.location.href = '<?php echo base_url() ?>'+'/Master/Mt_bank';
                }
             })
          });
        });
      </script>
