
    <!-- <div class="app-title">
      <div>
        <h1><i class="fa fa-th-list"></i> Data Table</h1>
        <p>Table to display analytical data effectively</p>
      </div>
      <ul class="app-breadcrumb breadcrumb side">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item">Tables</li>
        <li class="breadcrumb-item active"><a href="#">Data Table</a></li>
      </ul>
    </div> -->
    <?php 
      // echo $biodata01['is_glasses']; 
      // echo $biodata01['is_active']; 
      // echo '<pre>';
      // print_r($biodata01); 
      // echo '</pre>'; 
    helper('general');
    ?>

    <!-- FORM -->
    <div class="row">     
      <div class="col-md-12">
        <div class="tile">
          <h3 class="tile-title">Biodata Edit</h3>
          <div class="tile-body">
            <form class="row" method="POST" action="<?php echo base_url() ?>/Master/Biodata/insData">
              <!-- Hidden Id -->
              <input class="form-control" name="biodataId" id="biodataId" type="hidden" value="<?php echo $biodata01['biodata_id'] ?>">
              <div class="form-group col-md-3">
                <label class="control-label">Redpath Number</label>
                <input class="form-control" name="empId" id="empId" type="text" value="<?php echo $biodata01['internal_id'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">First Name</label>
                <input class="form-control" id="firstName" name="firstName" type="text" required="" value="<?php echo $biodata01['first_name'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Middle Name</label>
                <input class="form-control" id="middleName" name="middleName" type="text" value="<?php echo $biodata01['middle_name'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Last Name</label>
                <input class="form-control" id="lastName" name="lastName" type="text" value="<?php echo $biodata01['last_name'] ?>">
              </div>
            
              <div class="form-group col-md-3">
                <label class="control-label">Nationality</label>
                <input class="form-control" id="nationality" name="nationality" type="text" value="<?php echo $biodata01['nationality'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Address</label>
                <input class="form-control" id="empAddress" name="empAddress" type="text" value="<?php echo $biodata01['id_card_address'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">NPWP No</label>
                <input class="form-control" id="npwpNo" name="npwpNo" type="text" value="<?php echo $biodata01['tax_no'] ?>">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">BPJS TK No</label>
                <input class="form-control" id="bpjsTKNo" name="bpjsTKNo" type="text" value="<?php echo $biodata01['bpjs_tk_no'] ?>">
              </div> 
             <!--  <div class="form-group col-md-3">
                <label class="control-label">Email</label>
                <input class="form-control" id="emailAddress" name="emailAddress" type="text" value="<?php //echo strtoupper ($biodata01['email_address']) ?>">
              </div>  -->
              <div class="form-group col-md-3">
                <label class="control-label">Email</label>
                <input class="form-control" id="emailAddress" name="emailAddress" type="text" value="<?php echo strtoupper ($biodata01['email_address']) ?>">
              </div> 
              <div class="form-group col-md-3">
                <label class="control-label">Join Date</label>
                <input class="form-control" id="joinDate" name="joinDate" type="date" value="<?php echo $biodata01['join_date'] ?>">
              </div>
              <!-- <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Submit</button>            -->
            </form>
          </div>
          <div class="tile-footer">
            <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/Biodata/reset"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
          </div>
        </div>
      </div>
    </div>
    <!-- TABLE -->
    <div class="row">    
      <div class="col-md-12">
        <div class="tile">
          <div class="tile-body">

            <!-- <div class="container"> -->
              <!-- <h2>Toggleable Pills</h2> -->
              <br>
              <!-- Nav pills -->
              <ul class="nav nav-pills" role="tablist">
                <li class="nav-item">
                  <a class="nav-link active" data-toggle="pill" href="#education">Education&nbsp;</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="pill" href="#experience">Experience</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" data-toggle="pill" href="#skill">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Skill&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                </li>
              </ul>
              
                  
              <!-- <div class="tile-footer">
                <button class="btn btn-info" data-fill="Education" type="button" data-toggle='modal' data-target='#myModal'>
                  <i class="fa fa-fw fa-lg fa-check-circle"></i>New
                </button>

                <button class="btn btn-info" data-fill="Experience" type="button" data-toggle='modal' data-target='#myModal'>
                  <i class="fa fa-fw fa-lg fa-check-circle"></i>New
                </button>

                <button class="btn btn-info" data-fill="Skill" type="button" data-toggle='modal' data-target='#myModal'>
                  <i class="fa fa-fw fa-lg fa-check-circle"></i>New
                </button>
              </div> -->

              <!-- Tab panes -->
              <div class="tab-content">
                <!-- START SCHOOL -->

                

                <div id="education" class="tab-pane active"><br>               
                  <!-- TABLE DATA -->
                  
                  <div class="tile-footer">
                    <button class="btn btn-info" data-fill="Education" type="button" data-toggle='modal' data-target='#myModal'>
                      <i class="fa fa-fw fa-lg fa-check-circle"></i>New
                    </button>                  
                  </div>

                  <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="educationTable">
                      <thead style="background-color: rgb(13 81 198);color: white;">
                        <tr>
                          <th>School Name</th>
                          <th>Major</th>
                          <th>City</th>
                          <th>Year</th>
                          <th>Certified</th>
                          <th>Delete</th>
                          <!-- <th>Delete</th> -->
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        $educationList = $biodata02;
                        $btEduDel = "<button type='button' class='btn btn-danger btEduDel'><i class='fas fa-trash'></i></button>";
                        foreach ($educationList as $key => $value) {
                          echo "<tr>";
                          echo "<td>".$value['school_name']."</td>";
                          echo "<td>".$value['major']."</td>";
                          echo "<td>".$value['city_name']."</td>";
                          echo "<td>".$value['education_year']."</td>";
                          echo "<td>".$value['is_certified']."</td>";
                          echo "<td>".$btEduDel."</td>";                          
                          echo "</tr>";
                        }   
                      ?>
                        <!-- <tr>
                          <td>Tiger</td>
                          <td>System Architect</td>
                          <td>Edinburgh</td>
                          <td>61</td>                      
                          <td>Yes</td>                      
                          <td>X</td>                      
                        </tr>   -->                  
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- END SCHOOL -->

                <!-- START EXPERIENCE -->
                <div id="experience" class="tab-pane"><br>                 
                  <!-- TABLE DATA -->
                  
                  <div class="tile-footer">
                    <button class="btn btn-info" data-fill="Experience" type="button" data-toggle='modal' data-target='#myModal'>
                      <i class="fa fa-fw fa-lg fa-check-circle"></i>New
                    </button>                  
                  </div>

                  <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="experienceTable">
                      <thead style="background-color: rgb(13 81 198);color: white;">
                        <tr>
                          <th>Company</th>
                          <th>Position</th>
                          <th>Job Desc</th>
                          <th>Year</th>
                          <th>Working Period</th>
                          <th>Moving Reason</th>
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        $experienceList = $biodata03;
                        $btExpDel = "<button type='button' class='btn btn-danger btExpDel'><i class='fas fa-trash'></i></button>";
                        foreach ($experienceList as $key => $value) {
                          echo "<tr>";
                          echo "<td>".$value['company_name']."</td>";
                          echo "<td>".$value['job_position']."</td>";
                          echo "<td>".$value['job_desc']."</td>";
                          echo "<td>".$value['work_year']."</td>";
                          echo "<td>".$value['work_period']."</td>";
                          echo "<td>".$value['moving_reason']."</td>";
                          echo "<td>".$btExpDel."</td>";                          
                          echo "</tr>";
                        }   
                      ?>  
                        <!-- <tr>
                          <td>Tiger Nixon</td>
                          <td>System Architect</td>
                          <td>Edinburgh</td>
                          <td>61</td>                      
                          <td>5</td>                      
                        </tr>  -->                   
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- END EXPERIENCE -->

                <!-- START SKILL -->
                <div id="skill" class="tab-pane"><br>                 
                  <!-- TABLE DATA -->
                  
                  <div class="tile-footer">
                    <button class="btn btn-info" data-fill="Skill" type="button" data-toggle='modal' data-target='#myModal'>
                      <i class="fa fa-fw fa-lg fa-check-circle"></i>New
                    </button>                  
                  </div>

                  <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="skillTable">
                      <thead style="background-color: rgb(13 81 198);color: white;">
                        <tr>
                          <th>Skill</th>
                          <th>Remarks</th>
                          <!-- <th>Edit</th> -->
                          <th>Delete</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php
                        $skillList = $biodata04;
                        $btSklDel = "<button type='button' class='btn btn-danger btSklDel'><i class='fas fa-trash'></i></button>";
                        foreach ($skillList as $key => $value) {
                          echo "<tr>";
                          echo "<td>".$value['skill']."</td>";
                          echo "<td>".$value['remarks']."</td>";
                          echo "<td>".$btSklDel."</td>";                          
                          echo "</tr>";
                        }   
                      ?> 
                        <!-- <tr>
                          <td>Tiger Nixon</td>
                          <td>System Architect</td>
                        </tr> -->                    
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- END SKILL -->
              </div>

          </div>
        </div>
      </div>
    </div>    


    <!-- The Modal -->
    <div class="modal" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header" style="background: #009688">
            <h4 class="modal-title">Data Detail</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          
          <!-- Modal body -->
          <div class="modal-body container">            
            <!-- DATA INPUT -->
            <div class="row dtSchool">
              <div class="form-group col-md-12">
                <label class="control-label">School name</label>
                <input class="form-control" name="schoolName" id="schoolName" type="text" placeholder="Nama Perguruan">
              </div>
              <div class="form-group col-md-12">
                <label class="control-label">Major</label>
                <input class="form-control" name="schoolMajor" id="schoolMajor" type="text" placeholder="Jurusan">
              </div>
              <div class="form-group col-md-12">
                <label class="control-label">City</label>
                <input class="form-control" name="schoolCity" id="schoolCity" type="text" placeholder="Kota">
              </div>
              <div class="form-group col-md-12">
                <label class="control-label">Year</label>
                <input class="form-control" name="schoolYear" id="schoolYear" value="2000" type="number" placeholder="Tahun">
              </div>                      
              <div class="form-group col-md-12">
                <label for="isSchoolCert">Is Certified</label>
                <select class="form-control" name="isSchoolCert" id="isSchoolCert">
                  <option value="1" selected="">Yes</option>
                  <option value="0">No</option>
                </select>                  
              </div>                      
            </div>

            <!-- DATA INPUT -->
            <div class="row dtExperience">
              <div class="form-group col-md-12">
                <label class="control-label">Company</label>
                <input class="form-control" name="expCompany" id="expCompany" type="text" placeholder="Nama Perusahaan">
              </div>
              <div class="form-group col-md-12">
                <label class="control-label">Position</label>
                <input class="form-control" name="expPosition" id="expPosition" type="text" placeholder="Posisi">
              </div>
              <div class="form-group col-md-12">
                <label class="control-label">Job Desc</label>
                <input class="form-control" name="expJobDesc" id="expJobDesc" type="text" placeholder="Job Desc">
              </div>
              <div class="form-group col-md-12">
                <label class="control-label">Year</label>
                <input class="form-control" name="expYear" id="expYear" value="2000" type="number" placeholder="Tahun">
              </div> 
              <div class="form-group col-md-12">
                <label class="control-label">Working Period</label>
                <input class="form-control" name="expPeriod" id="expPeriod" value="1" type="text" placeholder="Masa Kerja">
              </div>                      
              <div class="form-group col-md-12">
                <label for="expReason">Moving Reason</label>
                <select class="form-control" name="expReason" id="expReason">
                  <option value="Resign" selected="">Resign</option>
                  <option value="Terminated">Terminated</option>
                  <option value="Fired">Fired</option>
                  <option value="Contract End">Contract End</option>
                </select>                  
              </div>                      
            </div>
            
            <!-- DATA INPUT -->
            <div class="row dtSkill">
              <div class="form-group col-md-12">
                <label class="control-label">Skill</label>
                <input class="form-control" name="skillName" id="skillName" type="text" placeholder="Kemampuan">
              </div>
              <div class="form-group col-md-12">
                <label class="control-label">Remarks</label>
                <input class="form-control" name="skillRemarks" id="skillRemarks" type="text" placeholder="Keterangan">
              </div>                                       
            </div>

            <div class="errDetail">
              <span style="color: red" class="errMess"></span>
            </div>
          </div>

          <!-- Modal footer -->
          <div class="modal-footer">
            <!-- <button type="button" class="btn btn-primary" id="dtSave" data-dismiss="modal">Save</button> -->
            <button type="button" class="btn btn-primary" id="dtSave">Save</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          </div>

        </div>
      </div>
    </div>
  
    <!-- This Line Must Have in Every Page Content -->
    <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>  
    <script>
      $('#demoSelect').select2();
      var baseUrl = '<?php echo base_url()?>';
      var dataList = []; 
      // var educationTable = null;
      // alert('Hello');
      $(document).ready(function() {
          $('#empId').focus();          
          
          biodata = {
            /* Start init */
            init : function()
            {
              
          
              var svGender = "<?php echo $biodata01['gender'] ?>";
              var svMarital = "<?php echo $biodata01['marital_status'] ?>";
              var svReligion = "<?php echo $biodata01['religion'] ?>";
              var svResidenceStatus = "<?php echo $biodata01['residence_status'] ?>";
              var svBloodType = "<?php echo $biodata01['blood_type'] ?>";
              var svDept = "<?php echo $biodata01['dept'] ?>";
              var svPosition = "<?php echo $biodata01['emp_position'] ?>";
              var svEmpStatus = "<?php echo $biodata01['emp_status'] ?>";

              if( (svGender != "") && (svGender != undefined) ){
                $('#gender option[value="'+svGender+'"]').attr('selected','selected');
              }

              if( (svMarital != "") && (svMarital != undefined) ){
                $('#maritalStatus option[value="'+svMarital+'"]').attr('selected','selected');
              }
              if( (svReligion != "") && (svReligion != undefined) ){
                  $('#religion option[value="'+svReligion+'"]').attr('selected','selected');
              }
              
              if( (svResidenceStatus != "") && (svResidenceStatus != undefined) ){
                  $('#residenceStatus option[value="'+svResidenceStatus+'"]').attr('selected','selected');
              }

              if( (svBloodType != "") && (svBloodType != undefined) ){
                  $('#bloodType option[value="'+svBloodType+'"]').attr('selected','selected');
              }

              // debugger;
              if( (svDept != "") && (svDept != undefined) ){
                  $('#dept option[value="'+svDept+'"]').attr('selected','selected');
              }

              if( (svPosition != "") && (svPosition != undefined) ){
                  $('#position option[value="'+svPosition+'"]').attr('selected','selected');
              }

              if( (svEmpStatus != "") && (svEmpStatus != undefined) ){
                  $('#employeeStatus option[value="'+svEmpStatus+'"]').attr('selected','selected');
              }

              // alert(svResidenceStatus); 
              // $.ajax({
              //   url    : baseUrl+'/General/Admin/Users/getAll',
              //   method : "POST",
              //   success : function(data)
              //   {
              //     debugger;
              //     var srcData = JSON.parse(data);
              //     educationTable = $('#educationTable').DataTable({
              //          "paging":   false,
              //          "ordering": false,
              //          "info":     false,
              //          "filter":   false,
              //          data : srcData,
              //          columns: [
              //               { data: 'user_id' },
              //               { data: 'user_password' },
              //               { data: 'full_name' },
              //               { data: 'user_level' }
              //          ]  
              //     });             

                  
              //   }
              // });
              // alert(JSON.stringify(eduData));

              var educationTable = $('#educationTable').DataTable({
                                  "paging":   false,
                                  "ordering": false,
                                  "info":     false,
                                  "filter":   false ,
                                  "autoWidth": false,
                                  "columnDefs": [
                                      {
                                          "targets": [4],
                                          "visible": false,
                                          "searchable": false
                                      }
                                      // ,
                                      // {
                                      //     "targets": [-1],
                                      //     "data": null,
                                      //     "defaultContent": "<button type='button' class='btn btn-primary btModal' data-toggle='modal' data-target='#myModal'><i class='fas fa-edit'></i></button>" 
                                      // }                               
                                  ]                                                             
                              });


              var experienceTable = $('#experienceTable').DataTable({
                                  "paging":   false,
                                  "ordering": false,
                                  "info":     false,
                                  "filter":   false ,
                                  "autoWidth": false
                                  // "columnDefs": [
                                  //     {
                                  //         "targets": [2],
                                  //         "visible": false,
                                  //         "searchable": false
                                  //     }                                                                  
                                  // ]                                                             
                              });

              var skillTable = $('#skillTable').DataTable({
                                  "paging":   false,
                                  "ordering": false,
                                  "info":     false,
                                  "filter":   false ,
                                  "autoWidth": false
                                  // "columnDefs": [
                                  //     {
                                  //         "targets": [2],
                                  //         "visible": false,
                                  //         "searchable": false
                                  //     }                                                                  
                                  // ]                                                             
                              });




              /* Get Active Tab Position */
              var activeTab = 'Education';
              var previousTab = '';
              $('.dtSchool').show();
              $('.dtExperience').hide();
              $('.dtSkill').hide();
              $('a[data-toggle="pill"]').on('shown.bs.tab', function(e){
                  activeTab = $(e.target).text(); // Get the name of active tab
                  previousTab = $(e.relatedTarget).text(); // Get the name of previous tab
                  
              });
              /* Modal With Dynamic Element */ 
              $('#myModal').on('show.bs.modal', function (event) {
                $('.errMess').html('');
                var button = $(event.relatedTarget);
                var recipient = button.data('fill');
                var modal = $(this);
                modal.find('.modal-title').text(recipient);
                modal.find('.modal-body .dtSchool').hide();
                modal.find('.modal-body .dtExperience').hide();
                modal.find('.modal-body .dtSkill').hide();
                if(recipient == 'Education')
                {
                  modal.find('.modal-body .dtSchool').show();
                  // modal.find('.modal-body input').focus();
              
                  // modal.find('#schoolName').focus();
                  // $('#schoolName').focus();
                }
                else if(recipient == 'Experience')
                {
                  modal.find('.modal-body .dtExperience').show();

                }
                else if(recipient == 'Skill')
                {
                  modal.find('.modal-body .dtSkill').show();
                }

              });

              $('.errMess').html('');
              $('#dtSave').on('click', function(){
                /* Education */
                let schoolName = $('#schoolName').val();
                let schoolMajor = $('#schoolMajor').val();
                let schoolCity = $('#schoolCity').val();
                let schoolYear = $('#schoolYear').val();
                let isSchoolCert = $('#isSchoolCert').val();
                
                /* Experience */
                let expCompany = $('#expCompany').val();
                let expPosition = $('#expPosition').val();
                let expJobDesc = $('#expJobDesc').val();
                let expYear = $('#expYear').val();
                let expPeriod = $('#expPeriod').val();
                let expReason = $('#expReason').val();

                /* Skill */
                let skillName = $('#skillName').val();
                let skillRemarks = $('#skillRemarks').val();

                // alert(activeTab);
                /* START EDUCATION VALIDATION */
                if(activeTab.trim() == "Education")
                {                  
                  if(schoolName.trim() == '')
                  {                      
                      $('#schoolName').focus();
                      $('.errMess').html('School name cannot be empty');  
                  }
                  else if(schoolMajor.trim() == '')
                  {
                      $('#schoolMajor').focus();
                      $('.errMess').html('Major cannot be empty');  
                  }
                  else if(schoolCity.trim() == '')
                  {
                      $('#schoolCity').focus();
                      $('.errMess').html('City cannot be empty');  
                  }
                  else if(schoolYear.trim() == '')
                  {
                      $('#schoolYear').focus();
                      $('.errMess').html('Year cannot be empty');  
                  }
                  else
                  {
                    /* Validate data before added */
                    let isEdu = isEduExists(schoolName);
                    if(isEdu == '1')
                    {
                        $('#schoolName').focus();
                        $('#schoolName').select();
                        $('.errMess').html('School Name Already Exists');                        
                        // alert('Data Exists');
                        // $.notify({
                        //   title: "Information : ",
                        //   message: "School Name Already Exists!",
                        //   icon: 'fa fa-check' 
                        // },{
                        //   type: "info"
                        // });  
                        return false;
                    }
                    
                    /* Adding data to the table */
                    educationTable.row.add([
                                schoolName,
                                schoolMajor,
                                schoolCity,
                                schoolYear,
                                isSchoolCert,
                                "<button type='button' class='btn btn-danger btEduDel'><i class='fas fa-trash'></i></button>"
                              ]).draw(false); 


                    $('#myModal').modal('hide');
                    $.notify({
                      title: "Information : ",
                      message: "New education has been added!",
                      icon: 'fa fa-check' 
                    },{
                      type: "info"
                    });
                  }                   
                }
                /* END EDUCATION VALIDATION */
                /* START EXPERIENCE VALIDATION */
                else if(activeTab.trim() == "Experience")
                {
                    // debugger;
                    if(expCompany.trim() == '')
                    {                      
                        $('#expCompany').focus();
                        $('.errMess').html('Company name cannot be empty');  
                    }
                    else if(expPosition.trim() == '')
                    {
                        $('#expPosition').focus();
                        $('.errMess').html('Position cannot be empty');  
                    }
                    else if(expYear.trim() == '')
                    {
                        $('#expYear').focus();
                        $('.errMess').html('Year cannot be empty');  
                    }
                    else if(expPeriod.trim() == '')
                    {
                        $('#expPeriod').focus();
                        $('.errMess').html('Working period cannot be empty');  
                    }
                    else if(expReason.trim() == '')
                    {
                        $('#expReason').focus();
                        $('.errMess').html('Reason to quit cannot be empty');  
                    }
                    else
                    {
                      /* Validate data before added */
                      let isExp = isExpExists(expCompany);
                      if(isExp == '1')
                      {
                          $('#expCompany').focus();
                          $('#expCompany').select();
                          $('.errMess').html('Company Name Already Exists');                        
                          // alert('Data Exists');
                          // $.notify({
                          //   title: "Information : ",
                          //   message: "School Name Already Exists!",
                          //   icon: 'fa fa-check' 
                          // },{
                          //   type: "info"
                          // });  
                          return false;
                      }
                      // debugger;
                      /* Adding data to the table */
                      experienceTable.row.add([
                                  expCompany,
                                  expPosition,
                                  expJobDesc,
                                  expYear,
                                  expPeriod,
                                  expReason,
                                  "<button type='button' class='btn btn-danger btExpDel'><i class='fas fa-trash'></i></button>"
                                ]).draw(false); 


                      $('#myModal').modal('hide');
                      $.notify({
                        title: "Information : ",
                        message: "New experience has been added!",
                        icon: 'fa fa-check' 
                      },{
                        type: "info"
                      });
                    } 
                }
                /* END EXPERIENCE VALIDATION */
                /* START SKILL VALIDATION */
                else if(activeTab.trim() == "Skill")
                {
                    if(skillName.trim() == '')
                    {                      
                        $('#skillName').focus();
                        $('.errMess').html('Skill name cannot be empty');  
                    }
                    else if(skillRemarks.trim() == '')
                    {
                        $('#skillRemarks').focus();
                        $('.errMess').html('Remarks cannot be empty');   
                    }
                    else
                    {
                        /* Validate data before added */
                        let isExp = isSklExists(skillName);
                        if(isExp == '1')
                        {
                            $('#skillName').focus();
                            $('#skillName').select();
                            $('.errMess').html('Skill Name Already Exists');
                            return false;  
                        }

                        /* Adding data to the table */
                        skillTable.row.add([
                                    skillName,
                                    skillRemarks,
                                    "<button type='button' class='btn btn-danger btSklDel'><i class='fas fa-trash'></i></button>"
                                  ]).draw(false); 


                        $('#myModal').modal('hide');
                        $.notify({
                          title: "Information : ",
                          message: "New skill has been added!",
                          icon: 'fa fa-check' 
                        },{
                          type: "info"
                        });
                    }
                }  
                /* END SKILL VALIDATION */

              });

              function isEduExists(schoolName)
              {
                  // debugger;
                  let tEduTable = $('#educationTable').DataTable();
                  let dataCount = tEduTable.rows().count();
                  let found = '0';  
                  for (var i = 0; i < dataCount; i++) {
                    var dataRow = $('#educationTable').DataTable().row(i).data();
                    if(schoolName == dataRow[0])
                    {
                        found = '1';
                        break;
                    }
                    
                  }
                  return found;

              }

              function isExpExists(expCompany)
              {
                // debugger;
                let tExpTable = $('#experienceTable').DataTable();
                let dataCount = tExpTable.rows().count();
                let found = '0';
                for (var i = 0; i < dataCount; i++) 
                {
                  let dataRow = $('#experienceTable').DataTable().row(i).data();
                  if(expCompany == dataRow[0])
                  {
                    found = '1';
                    break;
                  } 
                }
                return found;
              }

              function isSklExists(skillName)
              {
                // debugger;
                let tSklTable = $('#skillTable').DataTable();
                let dataCount = tSklTable.rows().count();
                let found = '0';
                for (var i = 0; i < dataCount; i++) 
                {
                  let dataRow = $('#skillTable').DataTable().row(i).data();
                  if(skillName == dataRow[0])
                  {
                    found = '1';
                    break;
                  } 
                }
                return found;
              }

              /* Get Selected Row Index */
              /*
              var eduIdx = 0;
              $('#educationTable tbody').on('click', 'tr', function(){
                eduIdx = educationTable.row( this ).index();
              });

              var expIdx = 0;
              $('#experienceTable tbody').on('click', 'tr', function(){
                expIdx = experienceTable.row( this ).index();
              });

              var sklIdx = 0;
              $('#skillTable tbody').on('click', 'tr', function(){
                sklIdx = skillTable.row( this ).index();
              });
              */

              /* Delete Row */
              var isDel = false;
              var eduIdx = 0;  
              $('#educationTable tbody').on('click', '.btEduDel', function(){
                  educationTable.row( $(this).parents('tr') ).remove().draw();
              });

              var expIdx = 0;
              $('#experienceTable tbody').on('click', '.btExpDel', function(){
                  experienceTable.row( $(this).parents('tr') ).remove().draw();
              });

              var sklIdx = 0;
              $('#skillTable tbody').on('click', '.btSklDel', function(){
                    // sklIdx = skillTable.row( $(this).parents('tr') ).index();
                  skillTable.row( $(this).parents('tr') ).remove().draw();                        
                    // swal({
                    //   title: "Are you sure?",
                    //   text: "You will not be able to recover this imaginary file!",
                    //   type: "warning",
                    //   showCancelButton: true,
                    //   confirmButtonText: "Yes, delete it!",
                    //   cancelButtonText: "No, cancel plx!",
                    //   closeOnConfirm: false,
                    //   closeOnCancel: false
                    // }, function(isConfirm) {
                    //   if (isConfirm) {
                    //     swal("Deleted!", "Your data has been deleted.", "success");
                    //     isDel = true;
                    //   } else {
                    //     swal("Cancelled", "Delete has been cancelled", "error");
                    //   }

                    // });   
                    // confirmDialog("Are You sure to do delete?", (ans) => {
                      // if(ans)
                      // {
                      // }
                    // }                 
              });

              function getEducationList(e)
              {
                // debugger;
                var eduArr = [];
                var edut = $('#educationTable').DataTable().data()
                var dataCount = edut.length;
                // alert(dataCount); return false;
                for (let i = 0; i < dataCount; i++) 
                {
                    dataRow = $('#educationTable').DataTable().row(i).data();
                    eduArr.push ({
                      schoolName    : dataRow[0],  
                      major         : dataRow[1],  
                      cityName      : dataRow[2],  
                      educationYear : dataRow[3],  
                      isCertified   : dataRow[4]  
                    });
                }
                return eduArr;
                // alert(JSON.stringify(eduArr));
              }

              function getExperienceList(e)
              {
                // debugger;
                var expArr = [];
                var expt = $('#experienceTable').DataTable().data()
                var dataCount = expt.length;
                // alert(dataCount); return false;
                for (let i = 0; i < dataCount; i++) 
                {
                    dataRow = $('#experienceTable').DataTable().row(i).data();
                    // alert(dataRow[2]);
                    // tQty      = dataRow[2];
                    // tQty      = tQty.replace(/\,/g, '');
                    expArr.push ({
                      companyName    : dataRow[0],  
                      jobPosition    : dataRow[1],  
                      jobDesc        : dataRow[2],  
                      workYear       : dataRow[3],  
                      workPeriod     : dataRow[4],  
                      movingReason   : dataRow[5]  
                    });
                }
                // alert(JSON.stringify(expArr));
                return expArr;
              }

              function getSkillList(e)
              {
                // debugger;
                var sklArr = [];
                var sklt = $('#skillTable').DataTable().data()
                var dataCount = sklt.length;
                for (let i = 0; i < dataCount; i++) 
                {
                    dataRow = $('#skillTable').DataTable().row(i).data();
                    sklArr.push ({
                      skill    : dataRow[0],  
                      remarks  : dataRow[1]  
                    });
                }
                return sklArr;
                // alert(JSON.stringify(sklArr));
              }
              // var eduArr = getEducationList();
              // var expArr = getExperienceList();
              // var sklArr = getSkillList();
              var biodataId = $('#biodataId').val(); 
              var myUrl = '<?php echo base_url() ?>/Master/Biodata/updData/'+biodataId; 
              // alert(myUrl);
              $('#dbSave').on('click', function(){
                let eduCount = educationTable.rows().count();
                let expCount = experienceTable.rows().count();
                let sklCount = skillTable.rows().count();

                if(!$('#firstName').val()){
                alert("First Name Can't Be Empty")
                $('#firstName').focus();
                return false;
                }

                if( eduCount == 0 && expCount == 0 && sklCount == 0 )
                {
                    $.notify({
                        title: "Information : ",
                        message: "Detail cannot be empty!",
                        icon: 'fas fa-exclamation-circle' 
                      },{
                        type: "danger"
                      });
                    return false;
                }

                $.ajax({
                  url    : myUrl,
                  method : "POST",
                  data   : {
                    /* Header */
                    empId           : $('#empId').val(),
                    firstName       : $('#firstName').val(),
                    middleName      : $('#middleName').val(),
                    lastName        : $('#lastName').val(),
                    // empName         : $('#empName').val(), 
                    payrollNo       : $('#payrollNo').val(), 
                    maritalStatus   : $('#maritalStatus').val(),
                    nationality     : $('#nationality').val(), 
                    empAddress      : $('#empAddress').val(),
                    npwpNo          : $('#npwpNo').val(),
                    bpjsTKNo        : $('#bpjsTKNo').val(),
                    emailAddress    : $('#emailAddress').val(),
                    joinDate        : $('#joinDate').val(),
                    picInput        : $('#picInput').val(),
                    inputTime       : $('#inputTime').val(),
                    employeeStatus  : $('#employeeStatus').val(), 
                    isActive        : $('#isActive').val(),
                    /* Detail */
                    eduList         : getEducationList(),
                    expList         : getExperienceList(),
                    sklList         : getSkillList() 
                  },
                  success : function(data)
                  {
                    alert(data);
                    $.notify({
                      title: "Information : ",
                      message: "New data has been saved!",
                      icon: 'fa fa-check' 
                    },{
                      type: "info"
                    });
                    window.location.href = '<?php echo base_url() ?>'+'/master/biodata';
                    // alert(data);
                    // alert(JSON.stringify(getSkillList()));
                  }
                })                
              });
              
            } 
            /* End init */



          }
          biodata.init();
       } );      

        
    </script>
