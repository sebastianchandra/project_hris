 <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">
      	 	  <h3 class="tile-title">Closing Update</h3>
      	 	  <div class="tile-body">
        		  <!-- Check Your Valid URL -->
      	 	    <form class="row" method="POST" action="../updData">
              <div class="form-group col-md-3">
                <label class="control-label">closing Id</label>
                <input class="form-control" name="closingId" id="closingId" type="text" value="<?php echo $dClose['closing_id'] ?>" disabled="">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Year Close</label>
                <select class="form-control " id="yearClose" name="yearClose" required="" value="<?php echo $dClose['close_year'] ?>">
                      <option value="" disabled="" selected="">Pilih</option>
                      <script type="text/javascript">
                        var dt = new Date();
                        var currYear = dt.getFullYear();
                        var currMonth = dt.getMonth();
                        var currDay = dt.getDate();
                        var tmpDate = new Date(currYear + 2, currMonth, currDay);

                        var startYear = tmpDate.getFullYear();
                        var endYear = startYear - 80;             
                        for (var i = startYear; i >= endYear; i--) 
                        {
                          document.write("<option value='"+i+"'>"+i+"</option>");           
                        }
                      </script>
                    </select>
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Month Name</label>
                <select class="form-control " id="monthClose" name="monthClose" required="" alue="<?php echo $dClose['close_month'] ?>">
                      <option value="" disabled="" selected="">Pilih</option>
                      <script type="text/javascript">
                        var tMonth = 1;
                        for (var i = tMonth; i <= 12; i++) 
                        {
                          if(i < 10)
                          {
                            document.write("<option value='0"+i+"'>0"+i+"</option>");             
                          }
                          else
                          {
                            document.write("<option value='"+i+"'>"+i+"</option>");               
                          }
                          
                        }
                      </script>
                    </select> 
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Period No</label>
                <select class="form-control" name="periodNo" id="periodNo">
                  <option value="" disabled="" selected="">Choose</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                </select> 
              </div>
              <div class="form-group col-md-6"> 
                <label class="control-label">Is Active</label>
                <!-- <input type="checkbox" id="is_renewal" name="isrenewal" value="1" checked=""> -->
                 <!-- <input type="hidden" id="isActive" name="isActive" value="1" > -->
                <input type="checkbox" id="isActive" name="isActive" value="0">
              </div> 
              
      	 	    </form>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div class="tile-footer">
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>&nbsp;&nbsp;&nbsp;
      	 	    <a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/mt_process_closing/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
      	 	    <strong>
      	 	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	 	      <span style="color: red" class="errSaveMess"></span>
      	 	    </strong>
      	 	  </div>
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
          var baseUrl = '<?php echo base_url()?>';

          var mtYear  = "<?php echo $dClose['close_year'] ?>";
          var mtMonth = "<?php echo $dClose['close_month'] ?>";
          var mtPeriod = "<?php echo $dClose['period_no'] ?>";

            if( (mtYear != "") && (mtYear != undefined) )
            {
              $('#yearClose option[value="'+mtYear+'"]').attr('selected','selected');
            }
            if( (mtMonth != "") && (mtMonth != undefined) )
            {
              $('#monthClose option[value="'+mtMonth+'"]').attr('selected','selected');
            }
            if( (mtPeriod != "") && (mtPeriod != undefined) )
            {
              $('#periodNo option[value="'+mtPeriod+'"]').attr('selected','selected');
            }

          $("#closingId").focus();
          $("#dbSave").on("click", function(){
             let closingId   = $("#closingId").val();
             let yearClose = $("#yearClose").val();
             let monthClose = $("#monthClose").val();
             let isActive  = $("#isActive").val();
             let periodNo  = $("#periodNo").val();

             $(".errSaveMess").html("");
             // if(closingId.trim() == "")
             // {
             //   $("#closingId").focus();
             //   $(".errSaveMess").html("Close Id cannot be empty");
             // }
             // else if(yearClose.trim() == "")
             // {
             //   $("#yearClose").focus();
             //   $(".errSaveMess").html("Year cannot be empty");
             // }
             // else if(monthClose.trim() == "")
             // {
             //   $("#monthClose").focus();
             //   $(".errSaveMess").html("Month cannot be empty");
             // }
             // else if(isLocal.trim() == "")
             // {
             //   $("#isLocal").focus();
             //   $(".errSaveMess").html("Is Local cannot be empty");
             // }
      	 	  /* ***Put URL your here */
             var myUrl ='<?php echo base_url() ?>/Master/Mt_process_closing/editData';

             // var isLocal = "Y";
             if ($('#isActive').is(":checked"))
              { 
                isActive = "Y";
              }              
              else
              {
                isActive = "T";
              }

             $.ajax({
                url    : myUrl,
                method : "POST",
                data   : {
                   closingId : $("#closingId").val(),
                   yearClose : $("#yearClose").val(),
                   monthClose : $("#monthClose").val(),
                   periodNo : $("#periodNo").val(),
                   isActive
                },
                success : function(data)
                {
      	 	         $.notify({
      	 	            title: "Information : ",
      	 	            message: "New data has been saved!",
      	 	            icon: "fa fa-check"
      	 	         },{
      	 	            type: "info"
      	 	         });
      	 	         /* Your redirect is here */
                   window.location.href = '<?php echo base_url() ?>'+'/Master/Mt_process_closing';
                }
             })
          });
        });
      </script>