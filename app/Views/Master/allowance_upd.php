
    <div class="app-title">
      <div>
        <h1>Update Master Allowance</h1>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Master</li>
          <li class="breadcrumb-item">Master Allowance</li>
        </ul>
      </div>
      <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item">
          <!-- <a href="<?= base_url('master/biodata/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a> -->
        </li>
      </ul>
    </div>
    <!-- FORM -->

    <div class="row">     
      <div class="col-md-12">
        <div class="tile">
          <!-- <h3 class="tile-title">Biodata Form</h3> -->
          <div class="tile-body">
            <form class="form-horizontal" method="POST" action="../insData">
              <div class="form-group row">
                <label class="control-label col-md-3">Allowance Name</label>
                <div class="col-md-5">                  
                  <input class="form-control" name="ma_allowance" id="ma_allowance" type="text" placeholder="" value="<?= $data_allow['ma_name']; ?>">
                  <input class="form-control" name="ma_id" id="ma_id" type="hidden" placeholder="" value="<?= $data_allow['ma_id']; ?>">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Nama Company</label>
                <div class="col-md-3">                  
                  <select class="form-control" name="dept" id="company_id">
                    <option value="" disabled="" selected="">Choose</option>
                    <?php 
                    foreach ($data_company as $key => $value) {
                      if($data_allow['company_id']==$value['company_id']){
                        echo '<option value="'.$value['company_id'].'" selected>'.$value['company_name'].' </option>';
                      }else{
                        echo '<option value="'.$value['company_id'].'">'.$value['company_name'].' </option>';
                      }
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Allowance Component </label>
                <div class="col-md-9">
                    <?php 
                    foreach ($data_field as $key => $value) {
                    ?>
                      <button class="btn btn-primary col-md-1" type="button" onclick="return detail(this)" data-field_name="<?= $value['field_name'] ?>" style="margin-bottom: 3px;"><?= $value['field_alias'] ?></button>&nbsp;&nbsp;
                    <?php
                    }
                    ?>
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Formula</label>
                <div class="col-md-9">                  
                  <input class="form-control" id="ma_formula" name="ma_formula" type="text" placeholder="Formula" value="<?= $data_allow['ma_formula']; ?>">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Is Tax</label>
                <div class="col-md-1">                  
                  <input class="form-control" id="is_tax" name="is_tax" type="text" value="<?= $data_allow['is_tax']; ?>">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Tax Type</label>
                <div class="col-md-1">                  
                  <input class="form-control" id="tax_type" name="tax_type" type="text" value="<?= $data_allow['tax_type']; ?>">
                </div>
              </div>
              <!-- <div class="form-group row">
                <label class="control-label col-md-3">Tax No</label>
                <div class="col-md-2">                  
                  <input class="form-control" name="id_card_no" id="id_card_no" type="text" placeholder="PTFI ID">
                </div>
              </div> -->
              
              
              <!-- <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Submit</button>            -->
            </form>
          </div>
          
          <div class="tile-footer">
            <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/Biodata/reset"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
          </div>
        </div>
      </div>
    </div>
    <!-- TABLE -->
  
    <!-- This Line Must Have in Every Page Content -->
    <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>  
    <script>
      $('#field_name').select2();
      $('#company_id').select2();

      function detail(e){
        let field_name  = $(e).data('field_name');
        var ma_formula  = $('#ma_formula').val();

        $('#ma_formula').val(ma_formula+' '+field_name+' ');
        $('#ma_formula').focus();
      };

      var baseUrl = '<?php echo base_url()?>';
      var dataList = []; 
      var educationTable = null;
      $(document).ready(function() {
          // debugger;
          $('#empId').focus();          
          
          biodata = {
            /* Start init */
            init : function()
            {

              var myUrl = '<?php echo base_url() ?>/Master/allowance/updData'; 

              $('#dbSave').on('click', function(){                

                $.ajax({
                  url    : myUrl,
                  method : "POST",
                  data   : {
                    ma_id           : $('#ma_id').val(),
                    company_id      : $('#company_id').val(),
                    ma_allowance    : $('#ma_allowance').val(),
                    ma_formula      : $('#ma_formula').val(),
                    is_tax          : $('#is_tax').val(),
                    tax_type        : $('#tax_type').val()
                  },
                  success : function(data)
                  {
                    toastr.success("Data has been Save.", 'Alert', {"positionClass": "toast-top-center"});
                     /* Your redirect is here */
                    setTimeout(function () {
                      window.location.href = baseUrl+'/Master/allowance'; //will redirect to google.
                    }, 2000);

                  }
                })                
              });
              
            } 
            /* End init */



          }
          biodata.init();
       } );      

        
    </script>
