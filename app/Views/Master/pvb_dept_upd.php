      <div class="app-title">
        <div>
          <h1>Update Master PVP Department</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master PVP Department</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
            <!-- <a href="<?= base_url('Master/Mt_pvb_dept/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a> -->
          </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">
      	 	  <h3 class="tile-title">PVB Dept Update</h3>
      	 	  <div class="tile-body">
        		  <!-- Check Your Valid URL -->
              <form class="form-horizontal" method="POST" action="../insData">
                <div class="form-group row">
                  <label class="control-label col-md-2">Dept Code</label>
                  <div class="col-md-3">
                    <select class="form-control" name="deptCode" id="deptCode">
                      <option value="" disabled="" selected="">Choose</option>
                      <?php 
                      foreach ($data_dept as $key => $value) {
                        if($dept_pvb['dept_code']==$value->dept_code){
                          echo '<option value="'.$value->dept_code.'" selected>'.$value->dept_code.' - '.$value->dept_name.' </option>';
                        }else{
                          echo '<option value="'.$value->dept_code.'">'.$value->dept_code.' - '.$value->dept_name.' </option>';
                        }
                      }
                      ?>
                    </select>
                    <input class="form-control" name="pvbId" id="pvbId" type="hidden" value="<?php echo $dept_pvb['pvb_id'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Pvb Percent</label>
                  <div class="col-md-2">
                    <input class="form-control" name="percent" id="percent" type="text" value="<?php echo $dept_pvb['percent'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Remarks</label>
                  <div class="col-md-5">
                    <input class="form-control" name="remarks" id="remarks" type="text" value="<?php echo $dept_pvb['remarks'] ?>">
                  </div>
                </div>
              </form>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div class="tile-footer">
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
              <a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/Mt_pvb_dept/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
      	 	    <!-- &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
      	 	    <strong>
      	 	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	 	      <span style="color: red" class="errSaveMess"></span>
      	 	    </strong>
      	 	  </div>
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        var baseUrl = '<?php echo base_url()?>';
        var dataList = [];

          var mtPvbDeptCode = "<?php echo $dept_pvb['dept_code'] ?>";

          if( (mtPvbDeptCode != "") && (mtPvbDeptCode != undefined) )
          {
          $('#deptCode option[value="'+mtPvbDeptCode+'"]').attr('selected','selected');
          }

        $(document).ready(function() {
          var baseUrl = '<?php echo base_url()?>';

          $("#pvbId").focus();
          $("#dbSave").on("click", function(){
             // let bankId   = $("#bankId").val();
             let deptCode = $("#deptCode").val();
             let percent = $("#percent").val();
             let remarks = $("#remarks").val();
             let picEdit  = $("#picEdit").val();
             let editTime = $("#editTime").val();
             $(".errSaveMess").html("");
             if(deptCode.trim() == "")
             {
               $("#deptCode").focus();
               $(".errSaveMess").html("Dept Code cannot be empty");
             }
             else if(percent.trim() == "")
             {
               $("#percent").focus();
               $(".errSaveMess").html("Percent cannot be empty");
             }
             else if(remarks.trim() == "")
             {
               $("#remarks").focus();
               $(".errSaveMess").html("Remarks cannot be empty");
             }
      	 	  /* ***Put URL your here */
             var myUrl ='<?php echo base_url() ?>/Master/Mt_pvb_dept/editData';

             // var isLocal = "Y";

             $.ajax({
                url    : myUrl,
                method : "POST",
                data   : {
                   pvbId : $("#pvbId").val(),
                   deptCode : $("#deptCode").val(),
                   percent : $("#percent").val(),
                   remarks : $("#remarks").val(),
                   // isActive,
                   picEdit   : $("#picEdit").val(),
                   editTime  : $("#editTime").val()
                },
                success : function(data)
                {
                  toastr.success("Data has been Save.", 'Alert', {"positionClass": "toast-top-center"});
                   /* Your redirect is here */
                  setTimeout(function () {
                    window.location.href = baseUrl+'/Master/Mt_pvb_dept'; //will redirect to google.
                  }, 2000);
                }
             })
          });
        });
      </script>
