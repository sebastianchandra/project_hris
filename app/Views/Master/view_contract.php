      <div class="row">
        <div class="col-md-12">
          <div class="tile">
          <h3 class="tile-title">Contract List</h3>
           <div class="tile-footer">
           	<a href="<?php echo base_url().'/Master/Mt_contract/ins_view' ?>">
           		<!-- <button class="btn btn-primary" type="button"> -->
              <button class="btn btn-primary col-xs-2" type="button" style="float: right;">
           			<i class="fa fa-fw fa-lg fas fa-plus-circle "></i>New
           		</button>
           	</a>
           </div>
           <div class="tile-body">
           	<!-- TABLE -->
           	<div class="table-responsive">
           	  <table class="table table-striped table-bordered" id="mtContract">
           	    <thead style="background-color: rgb(13 81 198);color: white;">
           	     <tr>
           	       <!-- <th>Contract Id</th> -->
           	       <th>Biodata Id</th>
           	       <th>Badge No</th>
           	       <th>Contract No</th>
           	       <th>Dept</th>
           	       <th>Job Position</th>
           	       <th>Contract Start</th>
           	       <th>Contract End</th>
           	       <th>Remarks</th>
           	       <th>Edit</th>
           	     </tr>
           	    </thead>
           	    <tbody>
           	     <!-- <tr> -->
           	      <!-- <td>contract_id</td> -->
           	      <!-- <td>biodata_id</td> -->
           	      <!-- <td>badge_no</td> -->
           	      <!-- <td>contract_no</td> -->
           	      <!-- <td>dept</td> -->
           	      <!-- <td>job_position</td> -->
           	      <!-- <td>contract_start</td> -->
           	      <!-- <td>contract_end</td> -->
           	      <!-- <td>contract_counter</td> -->
           	      <!-- <td>is_close</td> -->
           	      <!-- <td>is_active</td> -->
           	      <!-- <td>remarks</td> -->
           	      <!-- <td>input_time</td> -->
           	      <!-- <td>pic_input</td> -->
           	      <!-- <td>edit_time</td> -->
           	      <!-- <td>pic_edit</td> -->
           	      <!-- <td>Link Edit</td> -->
           	     <!-- </tr> -->
           	    </tbody>
           	  </table>
           	</div>
           </div>
          </div> <!-- class="tile" -->
        </div> <!-- class="col-md-12" -->
      </div> <!-- class="row" -->
      <!-- js load file is here  -->
      <!-- <script src="../assets/js/main.js"></script>  -->
      <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>
      <script>
      $('#demoSelect').select2();
      var baseUrl  = '<?php echo base_url()?>';
      var dataList = [];

        $(document).ready(function() {
        	/* START AJAX FOR LOAD DATA */
        	$.ajax({
        		/* Url is here */
        		url : baseUrl+'/master/Mt_contract/getAll',
        		method : "POST",
        		success : function(data)
        		{
        			let srcData = JSON.parse(data);
        			/* Edit Url Controller is here */
        			let updUrl = '<?php echo base_url(); ?>/master/mt_contract/upd_view/'
        			/* START TABLE */
        			let mtContract = $("#mtContract").DataTable({
        				"paging":   true,
        				"ordering": true,
        				"info":     true,
        				"filter":   false,
        				"autoWidth": false,
        				"columnDefs": [
        								{
        									/* Hide Table Id */
        									// "targets": [0],
        									// "visible": false,
        									// "searchable": false
        								},
        								{
        									/* Column For Edit Link, (ex : 5) depend on last column no */
        									"targets": 8,
        									"data": "download_link",
        									"render": function ( data, type, row, meta ) {
        									   // Change table_id with primary key of your table  
        									  return '<a href="'+updUrl+row['contract_id']+'">Edit</a>';
        									}
        								}
        				],
        				data : srcData,
        				columns: [
        					// { data: "contract_id" },
        					{ data: "biodata_id" },
        					{ data: "badge_no" },
        					{ data: "contract_no" },
        					{ data: "dept" },
        					{ data: "job_position" },
        					{ data: "contract_start" },
        					{ data: "contract_end" },
        					// { data: "contract_counter" },
        					// { data: "is_close" },
        					// { data: "is_active" },
        					{ data: "remarks" }
        				]
        			})
        			/* END TABLE */
        		}
        	});
        	/* END AJAX FOR LOAD DATA */
        });
      </script>
