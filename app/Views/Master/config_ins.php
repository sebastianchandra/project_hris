
    <div class="app-title">
      <div>
        <h1>Input Master Config</h1>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item">Master</li>
          <li class="breadcrumb-item">Master Config</li>
        </ul>
      </div>
      <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item">
          <!-- <a href="<?= base_url('master/biodata/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a> -->
        </li>
      </ul>
    </div>
    <!-- FORM -->

    <div class="row">    	
	    <div class="col-md-12">
        <div class="tile">
          <!-- <h3 class="tile-title">Biodata Form</h3> -->
          <div class="tile-body">
            <form class="form-horizontal" method="POST" action="../insData">
              <div class="form-group row">
                <label class="control-label col-md-3">Nama Company</label>
                <div class="col-md-3">                  
                  <select class="form-control" name="dept" id="company_id">
                    <option value="" disabled="" selected="">Choose</option>
                    <?php 
                    foreach ($data_company as $key => $value) {
                    echo '<option value="'.$value['company_id'].'">'.$value['company_name'].' </option>';
                    }
                    ?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Iuran BPJS Kesehatan Company <strong>(%)</strong></label>
                <div class="col-md-1">                  
                  <input class="form-control" name="health_bpjs" id="health_bpjs" type="text" placeholder="(%)">
                </div>
              </div>
              <!-- <div class="form-group row">
                <label class="control-label col-md-3">Tax No</label>
                <div class="col-md-2">                  
                  <input class="form-control" name="id_card_no" id="id_card_no" type="text" placeholder="PTFI ID">
                </div>
              </div> -->
              <div class="form-group row">
                <label class="control-label col-md-3">Maksimal Iuran BPJS Company <strong>(Value)</strong></label>
                <div class="col-md-2">                  
                  <input class="form-control" id="max_health_bpjs" name="max_health_bpjs" type="text" placeholder="(Value)">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Iuran BPJS Karyawan <strong>(%)</strong></label>
                <div class="col-md-1">                  
                  <input class="form-control" id="emp_health_bpjs" name="emp_health_bpjs" type="text" placeholder="(%)">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Maksimal Iuran BPJS Karyawan <strong>(Value)</strong></label>
                <div class="col-md-2">                  
                  <input class="form-control" id="max_emp_bpjs" name="max_emp_bpjs" type="text" placeholder="(Value)">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Jaminan Pensiun Company <strong>(%)</strong></label>
                <div class="col-md-1">                  
                  <input class="form-control" id="jp" name="jp" type="text" placeholder="(%)">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Maksimal Jaminan Pensiun Company <strong>(Value)</strong></label>
                <div class="col-md-2">                  
                  <input class="form-control" id="max_jp" name="max_jp" type="text" placeholder="(Value)" >
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Jaminan Pensiun Karyawan <strong>(%)</strong></label>
                <div class="col-md-1">                  
                  <input class="form-control" id="emp_jp" name="emp_jp" type="text" placeholder="(%)">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Maksimal Jaminan Pensiun Karyawan <strong>(Value)</strong></label>
                <div class="col-md-2">                  
                  <input class="form-control" id="max_emp_jp" name="max_emp_jp" type="text" placeholder="(Value)">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Jaminan Keselamatan Kerja & Kematian <strong>(%)</strong></label>
                <div class="col-md-1">                  
                  <input class="form-control" id="jkk_jkm" name="jkk_jkm" type="text" placeholder="(%)">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Jaminan Hari Tua (Company) <strong>(%)</strong></label>
                <div class="col-md-1">                  
                  <input class="form-control" id="jht" name="jht" type="text" placeholder="(%)">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Jaminan Hari Tua (Karyawan) <strong>(%)</strong></label>
                <div class="col-md-1">                  
                  <input class="form-control" id="emp_jht" name="emp_jht" type="text" placeholder="(%)">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Penghasilan Tidak kena Pajak <strong>(Value)</strong></label>
                <div class="col-md-2">                  
                  <input class="form-control" id="ptkp" name="ptkp" type="text" placeholder="(Value)">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Nilai Tanggungan PTKP <strong>(Value)</strong></label>
                <div class="col-md-2">                  
                  <input class="form-control" id="ptkp_dependent" name="ptkp_dependent" type="text" placeholder="(Value)">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Potongan Tambahan NPWP <strong>(%)</strong></label>
                <div class="col-md-1">                  
                  <input class="form-control" id="npwp_charge" name="npwp_charge" type="text" placeholder="(%)">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Overtime (Time)</label>
                <div class="col-md-1">                  
                  <input class="form-control" id="1st_ot" name="1st_ot" type="text" placeholder="(1)">
                </div>
                <div class="col-md-1">                  
                  <input class="form-control" id="2st_ot" name="2st_ot" type="text" placeholder="(2)">
                </div>
                <div class="col-md-1">                  
                  <input class="form-control" id="3st_ot" name="3st_ot" type="text" placeholder="(3)">
                </div>
                <div class="col-md-1">                  
                  <input class="form-control" id="4st_ot" name="4st_ot" type="text" placeholder="(4)">
                </div>
              </div>
              <div class="form-group row">
                <label class="control-label col-md-3">Overtime Multiplier</label>
                <div class="col-md-1">                  
                  <input class="form-control" id="1st_multiplier" name="1st_multiplier" type="text" placeholder="(1)">
                </div>
                <div class="col-md-1">                  
                  <input class="form-control" id="2st_multiplier" name="2st_multiplier" type="text" placeholder="(2)">
                </div>
                <div class="col-md-1">                  
                  <input class="form-control" id="3st_multiplier" name="3st_multiplier" type="text" placeholder="(3)">
                </div>
                <div class="col-md-1">                  
                  <input class="form-control" id="4st_multiplier" name="4st_multiplier" type="text" placeholder="(4)">
                </div>
              </div>
              <!-- <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Submit</button>            -->
            </form>
          </div>
          
          <div class="tile-footer">
            <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>&nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/Biodata/reset"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
          </div>
        </div>
      </div>
    </div>
    <!-- TABLE -->
	
	  <!-- This Line Must Have in Every Page Content -->
	  <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>  
	  <script>
      $('#company_id').select2();
      var baseUrl = '<?php echo base_url()?>';
      var dataList = []; 
      var educationTable = null;
	  	$(document).ready(function() {
          // debugger;
          $('#empId').focus();          
	        
          biodata = {
            /* Start init */
            init : function()
            {

              var myUrl = '<?php echo base_url() ?>/Master/Config/insData'; 

              $('#dbSave').on('click', function(){                

                $.ajax({
                  url    : myUrl,
                  method : "POST",
                  data   : {
                    company_id        : $('#company_id').val(),
                    health_bpjs       : $('#health_bpjs').val(),
                    max_health_bpjs   : $('#max_health_bpjs').val(),
                    emp_health_bpjs   : $('#emp_health_bpjs').val(),
                    max_emp_bpjs      : $('#max_emp_bpjs').val(),
                    jp                : $('#jp').val(),
                    max_jp            : $('#max_jp').val(),
                    emp_jp            : $('#emp_jp').val(),
                    max_emp_jp        : $('#max_emp_jp').val(),
                    jkk_jkm           : $('#jkk_jkm').val(),
                    jht               : $('#jht').val(),
                    emp_jht           : $('#emp_jht').val(),
                    ptkp              : $('#ptkp').val(),
                    ptkp_dependent    : $('#ptkp_dependent').val(),
                    npwp_charge       : $('#npwp_charge').val(),
                    ot1               : $('#1st_ot').val(),
                    ot2               : $('#2st_ot').val(),
                    ot3               : $('#3st_ot').val(),
                    ot4               : $('#4st_ot').val(),
                    mp1               : $('#1st_multiplier').val(),
                    mp2               : $('#2st_multiplier').val(),
                    mp3               : $('#3st_multiplier').val(),
                    mp4               : $('#4st_multiplier').val()

                  },
                  success : function(data)
                  {
                    toastr.success("Data has been Save.", 'Alert', {"positionClass": "toast-top-center"});
                     /* Your redirect is here */
                    setTimeout(function () {
                      window.location.href = baseUrl+'/Master/config'; //will redirect to google.
                    }, 2000);

                  }
                })                
              });
              
            } 
            /* End init */



          }
          biodata.init();
	     } );      

        
	  </script>
