      <div class="app-title">
        <div>
          <h1>Input Master Contract</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master Contract</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
            <!-- <a href="<?= base_url('master/biodata/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a> -->
          </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">
      	 	  <div class="tile-body">
        		  <!-- Check Your Valid URL -->
      	 	    <form class="form-horizontal" method="POST" action="../insData">
                <div class="form-group row">
                  <label class="control-label col-md-2">Biodata Id</label>
                  <div class="col-md-3">
                    <select class="form-control" name="biodataId" id="biodataId">
                      <option value="" disabled="" selected="">Choose</option>
                      <?php 
                      foreach ($data_biodata as $key => $value) {
                      echo '<option value="'.$value->biodata_id.'">'.$value->full_name.' - '.$value->biodata_id.' </option>';
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Badge No</label>
                  <div class="col-md-3">
                <input class="form-control" name="badgeNo" id="badgeNo" type="text" placeholder="Badge No">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Contract No</label>
                  <div class="col-md-3">
                <input class="form-control" name="contractNo" id="contractNo" type="text" placeholder="Contract Number">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Dept</label>
                  <div class="col-md-3">
                    <input class="form-control" name="dept" id="dept" type="text" placeholder="Dept">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Job Position</label>
                  <div class="col-md-3">
                    <input class="form-control" name="jobPosition" id="jobPosition" type="text" placeholder="Job Position">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Contract Start</label>
                  <div class="col-md-2">
                    <input class="form-control" name="contractStart" id="contractStart" type="date" placeholder="Contract Start">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Contract End</label>
                  <div class="col-md-2">
                    <input class="form-control" name="contractEnd" id="contractEnd" type="date" placeholder="Contract end">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Remarks</label>
                  <div class="col-md-5">
                    <input class="form-control" name="remarks" id="remarks" type="text" placeholder="Remarks">
                  </div>
                </div>
      	 	    </form>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div class="tile-footer">
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>&nbsp;&nbsp;&nbsp;
      	 	    <a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/mt_contract/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
      	 	    <strong>
      	 	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	 	      <span style="color: red" class="errSaveMess"></span>
      	 	    </strong>
      	 	  </div>
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- js load file is here  -->
      <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
          $("#contractId").focus();

          $('.errSaveMess').html('');
          $("#dbSave").on("click", function(){
             // let contractId = $('#contractId').val();
             // let contractId = ("#contractId").val();
             let biodataId  = $('#biodataId').val();
             let badgeNo    = $('#badgeNo').val();
             let contractNo = $('#contractNo').val();
             let dept       = $('#dept').val();
             let jobPosition = $('#jobPosition').val();
             let contractStart = $('#contractStart').val();
             let contractEnd = $('#contractEnd').val();
             // let contractCounter = ("#contractCounter").val();
             // let isClose = ("#isClose").val();
             let isActive = $('#isActive').val();
             let remarks = $('#remarks').val();
             let inputTime = $('#inputTime').val();
             let picInput = $('#picInput').val();
             // let editTime = ("#editTime").val();
             // let picEdit = ("#picEdit").val();
             
             // if(contractId.trim() == '')
             // {
             //   $('#contractId').focus();
             //   $('.errSaveMess').html('Contract Id cannot be empty');
             // }
             if(biodataId.trim() == '')
             {
               $("#biodataId").focus();
               $(".errSaveMess").html("Biodata Id cannot be empty");
             }
             else if(badgeNo.trim() == '')
             {
               $("#badgeNo").focus();
               $(".errSaveMess").html("Badge No cannot be empty");
             }
             else if(contractNo.trim() == '')
             {
               $("#contractNo").focus();
               $(".errSaveMess").html("Contract No cannot be empty");
             }
             else if(dept.trim() == '')
             {
               $("#dept").focus();
               $(".errSaveMess").html("Dept cannot be empty");
             }
             else if(jobPosition.trim() == '')
             {
               $("#jobPosition").focus();
               $(".errSaveMess").html("Job Position cannot be empty");
             }
             else if(contractStart.trim() == '')
             {
               $("#contractStart").focus();
               $(".errSaveMess").html("Contract Start cannot be empty");
             }
             else if(contractEnd.trim() == '')
             {
               $("#contractEnd").focus();
               $(".errSaveMess").html("Contract End cannot be empty");
             }
             // else if(contractCounter.trim() == "")
             // {
             //   $("#contractCounter").focus();
             //   $(".errSaveMess").html("Contract Counter cannot be empty");
             // }
             // else if(isClose.trim() == "")
             // {
             //   $("#isClose").focus();
             //   $(".errSaveMess").html("Is Close cannot be empty");
             // }
             // else if(isActive.trim() == "")
             // {
             //   $("#isActive").focus();
             //   $(".errSaveMess").html("Is Active cannot be empty");
             // }
             else if(remarks.trim() == "")
             {
               $("#remarks").focus();
               $(".errSaveMess").html("Remarks cannot be empty");
             }

      	 	   /* Put URL your here */
             var myUrl ='<?php echo base_url() ?>/Master/Mt_contract/insData';
             $.ajax({
                url    : myUrl,
                method : "POST",
                data   : {
                   // contractId    : $("#contractId").val(),
                   biodataId     : $("#biodataId").val(),
                   badgeNo       : $("#badgeNo").val(),
                   contractNo    : $("#contractNo").val(),
                   dept          : $("#dept").val(),
                   jobPosition   : $("#jobPosition").val(),
                   contractStart : $("#contractStart").val(),
                   contractEnd   : $("#contractEnd").val(),
                   // contractCounter : $("#contractCounter").val(),
                   // isClose : $("#isClose").val(),
                   // isActive : $("#isActive").val(),
                   remarks       : $("#remarks").val(),
                   inputTime     : $("#inputTime").val(),
                   picInput      : $("#picInput").val()
                },
                success : function(data)
                {
      	 	         $.notify({
      	 	            title: "Information : ",
      	 	            message: "New data has been saved!",
      	 	            icon: "fa fa-check"
      	 	         },{
      	 	            type: "info"
      	 	         });
      	 	         /* Your redirect is here */
                   window.location.href = '<?php echo base_url() ?>'+'/Master/Mt_contract';
                }
             })
          });
        });
      </script>
