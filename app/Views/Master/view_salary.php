      <div class="row">
        <div class="col-md-12">
          <div class="tile">
          <h3 class="tile-title">Salary List</h3>
           <div class="tile-footer">
           	<a href="<?php echo base_url().'/Master/Mt_salary/ins_view' ?>">
           		<!-- <button class="btn btn-primary" type="button"> -->
              <button class="btn btn-primary col-xs-2" type="button" style="float: right;">
           			<i class="fa fa-fw fa-lg fas fa-plus-circle "></i>New
           		</button>
           	</a>
           </div>
           <div class="tile-body">
           	<!-- TABLE -->
           	<div class="table-responsive">
           	  <table class="table table-striped table-bordered" id="mtSalary">
           	    <thead style="background-color: rgb(13 81 198);color: white;">
           	     <tr>
           	       <!-- <th>Salary Id</th> -->
           	       <th>Biodata Id</th>
           	       <th>Full Name</th>
                   <th>Badge No</th>
           	       <th>Class Id</th>
                   <!-- <th>Class Code</th> -->
           	       <th>Currency</th>
           	       <th>Base Wage</th>
                   <th>Local Bank</th>
                   <th>Foreign Bank</th>
           	       <th>Edit</th>
           	     </tr>
           	    </thead>
           	    <tbody>
           	     <!-- <tr> -->
           	      <!-- <td>salary_id</td> -->
           	      <!-- <td>biodata_id</td> -->
           	      <!-- <td>full_name</td> -->
           	      <!-- <td>class_id</td> -->
           	      <!-- <td>currency</td> -->
           	      <!-- <td>base_wage</td> -->
           	      <!-- <td>pic_input</td> -->
           	      <!-- <td>input_time</td> -->
           	      <!-- <td>pic_edit</td> -->
           	      <!-- <td>edit_time</td> -->
           	      <!-- <td>Link Edit</td> -->
           	     <!-- </tr> -->
           	    </tbody>
           	  </table>
           	</div>
           </div>
          </div> <!-- class="tile" -->
        </div> <!-- class="col-md-12" -->
      </div> <!-- class="row" -->
      <!-- js load file is here  -->
      <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>  
      <script>
      $('#demoSelect').select2();
      var baseUrl  = '<?php echo base_url()?>';
      var dataList = [];
      
        $(document).ready(function() {
        	/* START AJAX FOR LOAD DATA */
        	$.ajax({
        		/* Url is here */
        		url : baseUrl+'/master/Mt_salary/getAll',
        		method : "POST",
        		success : function(data)
        		{
        			let srcData  = JSON.parse(data);
        			/* Edit Url Controller is here */
        			let updUrl   = '<?php echo base_url(); ?>/master/mt_salary/upd_view/';
        			/* START TABLE */
        			let mtSalary = $("#mtSalary").DataTable({
        				"paging":   true,
        				"ordering": true,
        				"info":     true,
        				"filter":   false,
        				"autoWidth": false,
        				"columnDefs": [
        								// {
        								// 	/* Hide Table Id */
        								// 	"targets": [0],
        								// 	"visible": false,
        								// 	"searchable": false
        								// },
        								{
        									/* Column For Edit Link, (ex : 5) depend on last column no */
        									"targets": 8,
        									"data": "download_link",
        									"render": function ( data, type, row, meta ) {
        									  /* Change table_id with primary key of your table  */
        									  return '<a href="'+updUrl+row['salary_id']+'">Edit</a>';
        									}
        								}
        				],
        				data : srcData,
        				columns: [
        					// { data: "salary_id" },
        					{ data: "biodata_id" },
        					{ data: "full_name" },
                  { data: "badge_no" },
        					{ data: "class_id" },
                  // { data: "class_code" },
        					{ data: "currency" },
        					{ data: "base_wage" },
                  { data: "local_bank" },
                  { data: "foreign_bank" }
        				]
        			})
        			/* END TABLE */
        		}
        	});
        	/* END AJAX FOR LOAD DATA */
        });
      </script>
