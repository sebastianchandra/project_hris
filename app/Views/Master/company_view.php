      <!-- FORM -->
      <div class="app-title">
        <div>
          <h1>View Master Company</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master Company</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?= base_url('Master/Company/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a>
          </li>
        </ul>
      </div>
      <div class="row">    	
		    <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">               
              <!-- TABLE -->
              <div class="table-responsive">
                <table class="table table-hover table-bordered" id="biodataTable">
                  <thead style="background-color: rgb(13 81 198);color: white;">
                    <tr>
                      <th>Code</th>
                      <th>Company Name</th>
                      <th>Tax No</th>
                      <th>Phone</th>
                      <th>Logo</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <!-- <tr>
                      <td>Tiger Nixon</td>
                      <td>System Architect</td>
                      <td>Edinburgh</td>
                      <td>61</td>
                      <td>61</td>
                      <td>61</td>                      
                    </tr> -->                    
                  </tbody>
                </table>
              </div>
            </div>
            
          </div>
        </div>
      </div>
      <a href='<?php echo base_url(); ?>/master/biodata/upd_view/'></a>    
	
	  <!-- This Line Must Have in Every Page Content -->
	  <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>  
	  <script>
      $('#demoSelect').select2();
      var baseUrl = '<?php echo base_url()?>';
      var dataList = []; 
      // var biodataTable = $('#biodataTable').DataTable();
      var biodataTable = null;
	  	$(document).ready(function() {
          // debugger;
          // $('#userId').focus();          
          biodata = {
            init : function(){
              $.ajax({
                url    : baseUrl+'/master/company/getAll',
                method : "POST",
                success : function(data)
                {
                  // debugger;
                  let srcData = JSON.parse(data);
                  let updUrl = '<?php echo base_url(); ?>/master/company/upd_view/';
                  // var bioId = srcData[0]['biodata_id'];
                  // var t = sanitizeData(srcData);
                  // alert(srcData({'biodata_id'}));
                  // alert(JSON.stringify(srcData));
                  biodataTable = $('#biodataTable').DataTable({
                       "paging":   false,
                       "ordering": false,
                       "info":     false,
                       "filter":   false ,
                       "autoWidth": false, 
                       "columnDefs": [
                                      {
                                        "targets": 5,
                                        "data": "download_link",
                                        "render": function ( data, type, row, meta ) {
                                          return '<a href="'+updUrl+row['company_id']+'" class="btn btn-sm btn-warning">Edit</a> <button class="btn btn-sm btn-danger" type="button" id="delete" data-id="'+row['company_id']+'">Delete</button>';
                                        }
                                                      
                                      }
                                                                   
                                  ],  
                       data : srcData,
                       columns: [
                            { data: 'company_code' },
                            { data: 'company_name' },
                            { data: 'tax_no' },
                            { data: 'phone_1' },
                            { data: 'logo' }
                       ]  
                   })                  
                }
              });

              $('#biodataTable tbody').on('click','.btEdit', function(){
                  // educationTable.row( $(this).parents('tr') ).remove().draw();
                  let tIdx = biodataTable.row( $(this).parents('tr') ).index();
                  let tCount = biodataTable.rows().count();
                  let tRow = $('#biodataTable').DataTable().row(tIdx).data();
                  let tBioId = tRow['biodata_id'];
                  // window.location.href = '<?php #echo base_url() ?>'+'/master/biodata/upd_view/'+tBioId;  
                  // alert(tBioId);
              });

              // $('#biodataTable tbody').on('click','#delete', function(){ 
              //   alert('tes');
              // });

              $('#biodataTable').on('click','#delete', function (e) {
                var v_id      = $(this).data('id');

                toastr.warning(
                    'Do you want to delete this row ?<br /><br />'+
                    '<button type="button" id="okBtn" class="btn btn-danger" onclick="return deleteRow(this)" data-id="'+v_id+'">Yes</button> '+
                    '<button type="button" id="noBtn" class="btn btn-info">No</button>', 
                    '<u>ALERT</u>', 
                    {
                        "positionClass": "toast-top-center",
                        "onclick": null,
                        "closeButton": false,
                    }
                );
              });

            },
          }
          biodata.init();
	     } );

      function deleteRow(e){
        var id  = $(e).data('id');

        $.ajax({
          data: {
            id  : id
          },
          type : "POST",
          url: baseUrl+'/master/company/del',
          success : function(resp){

            if(resp.status == 'ERROR INSERT' || resp.status == false) {
              toastr.success('Data not saved successfully', 'Alert', {"positionClass": "toast-top-center"});
              return false;

            } else {
              toastr.success("Data has been Delete.", 'Alert', {"positionClass": "toast-top-center"});

              setTimeout(function () {
                window.location.href = baseUrl+'/master/company'; //will redirect to google.
              }, 2000);
            }
          }
        });
      }

       function sanitizeData(data) {
          // debugger;
          var d = [];
          Object.keys(data).forEach(function(key) {
            d.push(data[key]);
          });
          return d;
       } 


	  </script>
