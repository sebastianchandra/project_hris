      <div class="app-title">
        <div>
          <h1>Input Master Company</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master Company</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
            <!-- <a href="<?= base_url('master/biodata/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a> -->
          </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">
      	 	  <div class="tile-body">
        		  <!-- Check Your Valid URL -->
      	 	    <form class="form-horizontal" method="POST" action="../insData">
                <div class="form-group row">
                  <label class="control-label col-md-2">Code</label>
                  <div class="col-md-1">
                    <input class="form-control" name="bankId" id="tCode" type="text" placeholder="Code">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Name</label>
                  <div class="col-md-3">
                    <input class="form-control" name="bankId" id="tName" type="text" placeholder="Company Name">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Logo</label>
                  <div class="col-md-3">
                    <input class="form-control" name="bankId" id="tLogo" type="text" placeholder="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Tax No</label>
                  <div class="col-md-3">
                    <input class="form-control" name="bankId" id="tTaxNo" type="text" placeholder="Tax No">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Tax Name</label>
                  <div class="col-md-3">
                    <input class="form-control" name="bankId" id="tTaxName" type="text" placeholder="Tax Name">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Address</label>
                  <div class="col-md-5">
                    <input class="form-control" name="bankId" id="tAddress" type="text" placeholder="Address">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Phone</label>
                  <div class="col-md-2">
                    <input class="form-control" name="bankId" id="tPhone1" type="text" placeholder="Phone 1">
                  </div>
                  <div class="col-md-2">
                    <input class="form-control" name="bankId" id="tPhone2" type="text" placeholder="Phone 2">
                  </div>
                </div>              
      	 	    </form>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div class="tile-footer">
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
              <a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/company/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
      	 	    <!-- &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
      	 	    <strong>
      	 	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	 	      <span style="color: red" class="errSaveMess"></span>
      	 	    </strong>
      	 	  </div>
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
          var baseUrl = '<?php echo base_url()?>';
          $("#bankId").focus();
          $("#dbSave").on("click", function(){
             let tCode    = $("#tCode").val();
             let tName    = $("#tName").val();
             let tLogo    = $("#tLogo").val();
             let tTaxNo   = $("#tTaxNo").val();
             let tTaxName = $('#tTaxName').val();
             let tAddress = $('#tAddress').val();
             let tPhone1  = $('#tPhone1').val();
             let tPhone2  = $('#tPhone2').val();
             // $(".errSaveMess").html("");
             // if(bankId.trim() == "")
             // {
             //   $("#bankId").focus();
             //   $(".errSaveMess").html("Bank Id cannot be empty");
             // }
             // else if(bankCode.trim() == "")
             // {
             //   $("#bankCode").focus();
             //   $(".errSaveMess").html("Bank Code cannot be empty");
             // }
             // else if(bankName.trim() == "")
             // {
             //   $("#bankName").focus();
             //   $(".errSaveMess").html("Bank Name cannot be empty");
             // }
             // else if(isLocal.trim() == "")
             // {
             //   $("#isLocal").focus();
             //   $(".errSaveMess").html("Is Local cannot be empty");
             // }
      	 	   /* ***Put URL your here */
             var myUrl ='<?php echo base_url() ?>/Master/Company/insData';

             // var isLocal = "Y";
              // if ($('#isLocal').is(":checked"))
              // { 
              //   isLocal = "Y";
              // }              
              // else
              // {
              //   isLocal = "T";
              // }

             $.ajax({
                url    : myUrl,
                method : "POST",
                data   : {
                  tCode,tName,tLogo,tTaxNo,tTaxName,tAddress,tPhone1,tPhone2
                },
                success : function(data)
                {
      	 	        toastr.success("Data has been Save.", 'Alert', {"positionClass": "toast-top-center"});

                  setTimeout(function () {
                    window.location.href = baseUrl+'/master/company'; //will redirect to google.
                  }, 2000);
                }
             })
          });
        });
      </script>
