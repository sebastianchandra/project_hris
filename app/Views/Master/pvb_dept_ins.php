      <div class="app-title">
        <div>
          <h1>Input Master PVP Department</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master PVP Department</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
            <!-- <a href="<?= base_url('Master/Mt_pvb_dept/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a> -->
          </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">
      	 	  <div class="tile-body">
        		  <!-- Check Your Valid URL -->
              <form class="form-horizontal" method="POST" action="../insData">
                <div class="form-group row">
                  <label class="control-label col-md-2">Dept Code</label>
                  <div class="col-md-3">
                    <select class="form-control" name="deptCode" id="deptCode">
                      <option value="" disabled="" selected="">Choose</option>
                      <?php 
                      foreach ($data_dept as $key => $value) {
                      echo '<option value="'.$value->dept_code.'">'.$value->dept_code.' - '.$value->dept_name.' </option>';
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Pvb Percent</label>
                  <div class="col-md-2">
                    <input class="form-control" name="percent" id="percent" type="text" placeholder="Pvb Percent">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="control-label col-md-2">Remarks</label>
                  <div class="col-md-5">
                    <input class="form-control" type="text" id="remarks" name="remarks" placeholder="Remarks">
                  </div>
                </div>
      	 	    </form>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div class="tile-footer">
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
              <a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/Mt_pvb_dept/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
      	 	    <!-- &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="#"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a> -->
      	 	    <strong>
      	 	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      	 	      <span style="color: red" class="errSaveMess"></span>
      	 	    </strong>
      	 	  </div>
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
       <script>
        $(document).ready(function() {
          var baseUrl = '<?php echo base_url()?>';
          $("#deptCode").focus();
          $("#dbSave").on("click", function(){
             // let bankId = $("#bankId").val();
             let deptCode = $("#deptCode").val();
             let percent = $("#percent").val();
             let remarks = $("#remarks").val();
             let inputTime = $('#inputTime').val();
             let picInput = $('#picInput').val();
             $(".errSaveMess").html("");
             if(deptCode.trim() == "")
             {
               $("#deptCode").focus();
               $(".errSaveMess").html("Dept Code cannot be empty");
             }
             else if(percent.trim() == "")
             {
               $("#percent").focus();
               $(".errSaveMess").html("Percent cannot be empty");
             }
             // else if(bankName.trim() == "")
             // {
             //   $("#bankName").focus();
             //   $(".errSaveMess").html("Bank Name cannot be empty");
             // }
             // else if(isLocal.trim() == "")
             // {
             //   $("#isActive").focus();
             //   $(".errSaveMess").html("Is Local cannot be empty");
             // }
      	 	   /* ***Put URL your here */
             var myUrl ='<?php echo base_url() ?>/Master/Mt_pvb_dept/insData';

             $.ajax({
                url    : myUrl,
                method : "POST",
                data   : {
                   // bankId : $("#bankId").val(),
                   deptCode : $("#deptCode").val(),
                   percent : $("#percent").val(),
                   remarks : $("#remarks").val(),
                   // isActive,
                   picInput : $("#picInput").val(),
                   inputTime : $("#inputTime").val()
                },
                success : function(data)
                {
      	 	        toastr.success("Data has been Save.", 'Alert', {"positionClass": "toast-top-center"});
                   /* Your redirect is here */
                  setTimeout(function () {
                    window.location.href = baseUrl+'/Master/Mt_pvb_dept'; //will redirect to google.
                  }, 2000);
                }
             })
          });
        });
      </script>
