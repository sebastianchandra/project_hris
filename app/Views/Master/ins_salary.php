      <div class="row">
        <div class="col-md-12">
      	 	<div class="tile">
      	 	  <h3 class="tile-title">Salary Form</h3>
      	 	  <div class="tile-body">
        		  <!-- Check Your Valid URL -->
      	 	    <form class="row" method="#">
              <!-- <div class="form-group col-md-3">
                <label class="control-label">Salary Id</label>
                <input class="form-control" name="salaryId" id="salaryId" type="text" placeholder="Id Karyawan">
              </div> -->
              <div class="form-group col-md-3">
                <label class="control-label">Employee Name</label>
                <select class="form-control" name="biodataId" id="biodataId">
                  <option value="" disabled="" selected="">Choose</option>
                  <?php 
                  foreach ($data_biodata as $key => $value) {
                  echo '<option value="'.$value->biodata_id.'">'.$value->full_name.' - '.$value->biodata_id.' </option>';
                  }
                  ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Class</label>
                <select class="form-control" name="classId" id="classId">
                  <option value="" disabled="" selected="">Choose</option>
                  <?php 
                  foreach ($data_class as $key => $value) {
                  echo '<option value="'.$value->class_id.'">'.$value->class_base.' - '.$value->class_category.'</option>';
                  }
                  ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Payroll Number</label>
                <!-- <code id="badgeNoDuplicate" class="errMsg"><span> Duplicate Badge No</span></code> -->
                <input class="form-control" id="badgeNo" name="badgeNo" type="text" placeholder="Payroll Number">
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Local Bank</label>
                <select class="form-control" name="localBankId" id="localBankId">
                  <option value="" disabled="" selected="">Choose</option>
                  <?php 
                  foreach ($data_bank_local as $key => $value) {
                  echo '<option value="'.$value->bank_id.'">'.$value->bank_id.' - '.$value->bank_name.' </option>';
                  }
                  ?>
                </select>
              </div>
              <div class="form-group col-md-3">
                <label class="control-label">Foreign Bank</label>
                <select class="form-control" name="foreignBankId" id="foreignBankId">
                  <option value="" disabled="" selected="">Choose</option>
                  <?php 
                  foreach ($data_bank_foreign as $key => $value) {
                  echo '<option value="'.$value->bank_id.'">'.$value->bank_id.' - '.$value->bank_name.' </option>';
                  }
                  ?>
                </select>
              </div>
              <div class="form-group col-md-12"> 
                <label class="control-label">Is Employee get tax</label>
                <!-- <input type="checkbox" id="is_renewal" name="isrenewal" value="1" checked=""> -->
                 <!-- <input type="hidden" id="isActive" name="isActive" value="1" > -->
                <input type="checkbox" id="isGetTax" name="isGetTax" value="1">
              </div> 
      	 	    </form>
      	 	  </div> <!-- class="tile-body" -->
      	 	  <div>
      	 	    <button class="btn btn-primary" type="button" id="dbSave"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
      	 	    &nbsp;&nbsp;&nbsp;<a class="btn btn-secondary" href="<?php echo base_url(); ?>/master/mt_salary/reset"><i class="fa fa-fw fa-lg fa fa-times-circle"></i>Cancel</a>
      	 	  </div> 
      	 	</div> <!-- class="tile" -->
      	 </div> <!-- class="col-md-12 -->
      </div> <!-- class="row" -->
      <!-- js load file is here  -->
      <!-- <script src="../assets/js/main.js"></script>  -->
      <!-- This Line Must Have in Every Page Content -->
      <script src="<?php echo base_url(); ?>/assets/js/main.js"></script>  
      <script>
      $('#demoSelect').select2();

      $("#biodataId").select2({
      placeholder: 'Select a Biodata Id',
      allowClear: true});

      // $("#classId").select2().on('select2:select',function(e){
      // // debugger
      //   var classWage     = $('#classId option:selected').attr('data-class');
      //   var classCurrency = $('#classId option:selected').attr('kind-class'); 
      //   $('#classCurrency').val(classCurrency);//#ngambil dari IDnya////val kalau kosong mengambil, kalau diisi mengisi//
      //   $('#classWage').val(classWage);
      // });
          // var isbadgeNoDuplicate = false;
          // var confirmUpdate = false;
          // $('#badgeNoDuplicate').hide();
          // $('#badgeNo').on('focusout', function(){        
          //   var badgeNo = $('#badgeNo').val();
          //   // var leave_date = $('#leave_date').val();
          //   var myUrl = "<?php //echo base_url() ?>"+"master/mt_salary/getIdNo/"+badgeNo;
          //   // var badgeNoLength = badgeNo.length;     
          //   $.ajax({
          //     method : "POST",
          //     url : myUrl,
          //     data : {
          //         badgeNo  : badgeNo
          //         // leave_date : leave_date
          //     },
          //     success : function(data){
          //           isbadgeNoDuplicate = false;
          //           $('#badgeNoDuplicate').hide(); 
          //             if(data > 0){                          
          //               /* Confirmation Data Update */
          //               confirmUpdate = confirm('Duplicate Data Badge No, Exit ?');
          //               if(confirmUpdate == true) {
          //                 window.location.href="<?php //echo base_url() ?>"+"master/mt_salary";                     
          //               } else {
          //                 $('#badgeNo').focus();  
          //               }
          //               $('#badgeNoDuplicate').show();
          //               isbadgeNoDuplicate = true;
          //             }
          //     }, 
          //     error : function(data){
          //       alert('Failed1');
          //     }
          //   });
          // });

        var baseUrl = '<?php echo base_url()?>';
        var dataList = []; 
        $(document).ready(function() {
          $("#salaryId").focus();

          var myUrl = '<?php echo base_url() ?>/Master/Mt_salary/insData'; 
          $("#dbSave").on("click", function(){

            var isGetTax = "Y";
                  if ($('#isGetTax').is(":checked"))
                  { 
                    isGetTax = "Y";
                  }              
                  else
                  {
                    isGetTax = "T";
                  }

            $.ajax({
              url: myUrl,
              type : "POST",  
              data: {
                // salaryId        : $('#salaryId').val(),
                biodataId       : $('#biodataId').val(),
                // biodataKind     : $('#biodataId option:selected').attr('data-biodata'),
                classId         : $('#classId').val(),
                badgeNo         : $('#badgeNo').val(),
                localBankId     : $('#localBankId').val(),
                foreignBankId   : $('#foreignBankId').val(),
                isGetTax
                // classWage       : $('#classWage').val(),
                // classCurrency   : $('#classCurrency').val()
              },

                success : function(resp){
                if(resp.status == 'ERROR INSERT' || resp.status == false) {
                  $.notify({
                    message: 'Data Gagal disimpan'
                  },{
                    type: 'danger'
                  });
                  return false;
                }
                else
                  {
                    alert('Data Berhasil di Simpan.');

                      setTimeout(function () {
                        window.location.href = '<?php echo base_url() ?>'+'/Master/Mt_salary';; 
                      }, 2000);
                  }
              }
            });
          });
        });
    </script>
