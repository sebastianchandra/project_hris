      <div class="app-title">
        <div>
          <h1>View Master Currency</h1>
          <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('home') ?>"><i class="fa fa-home fa-lg"></i></a></li>
            <li class="breadcrumb-item">Master</li>
            <li class="breadcrumb-item">Master Currency</li>
          </ul>
        </div>
        <ul class="app-breadcrumb breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?= base_url('master/Mt_currency_rate/ins_view') ?>" class="btn btn-primary"><i class="fa fa-fw fa-lg fas fa-plus-circle "></i> New </a>
          </li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
           <div class="tile-body">
           	<!-- TABLE -->
           	<div class="table-responsive">
           	  <table class="table table-hover table-bordered" id="mtCurrency">
           	    <thead style="background-color: rgb(13 81 198);color: white;">
           	     <tr>
           	       <!-- <th>Bank Id</th> -->
           	       <th>Currency Code</th>
           	       <th>Country Name</th>
                   <th>Value</th>
           	       <th>Action</th>
           	     </tr>
           	    </thead>
           	    <tbody>
           	     <!-- <tr> -->
           	      <!-- <td>bank_id</td> -->
           	      <!-- <td>bank_code</td> -->
           	      <!-- <td>bank_name</td> -->
           	      <!-- <td>pic_input</td> -->
           	      <!-- <td>input_time</td> -->
           	      <!-- <td>pic_edit</td> -->
           	      <!-- <td>edit_time</td> -->
           	      <!-- <td>Link Edit</td> -->
           	     <!-- </tr> -->
           	    </tbody>
           	  </table>
           	</div>
           </div>
          </div> <!-- class="tile" -->
        </div> <!-- class="col-md-12" -->
      </div> <!-- class="row" -->
      <!-- ***Using Valid js Path -->
      <script src="<?php echo base_url()?>/assets/js/main.js"></script>
      <script>
        $(document).ready(function() {
        	var baseUrl = '<?php echo base_url()?>';
        	/* START AJAX FOR LOAD DATA */
        	$.ajax({
        		/* ***Url is here */
        		url : baseUrl+'/master/Mt_currency_rate/getAll',
        		method : "POST",
        		success : function(data)
        		{
        			let srcData = JSON.parse(data);
        			/* Edit Url Controller is here */
          			/* ***Using Valid Path */
        			let updUrl = '<?php echo base_url(); ?>/master/Mt_currency_rate/upd_view/';
        			/* START TABLE */
        			let mtCurrency = $("#mtCurrency").DataTable({
        				"paging":   true,
        				"ordering": true,
        				"info":     true,
        				"filter":   false,
        				"autoWidth": false,
        				"columnDefs": [
        								{
        									/* Hide Table Id */
        									// "targets": [0],
        									// "visible": false,
        									// "searchable": false
        								},
        								{
        									/* Column For Edit Link, (ex : 5) depend on last column no */
        									"targets": 3,
        									"data": "download_link",
        									"render": function ( data, type, row, meta ) {
        									  /* Change table_id with primary key of your table  */
        									  return '<a href="'+updUrl+row['cr_id']+'" class="btn btn-sm btn-warning">Edit</a> ';
        									}
        								}
        				],
        				data : srcData,
        				columns: [
        					// { data: "dept_id" },
        					{ data: "cr_code" },
        					{ data: "cr_country" },
                  { data: "rate_value" }
                  // { data: "is_local" },
        				]
        			})
        			/* END TABLE */
        		}
        	});
        	/* END AJAX FOR LOAD DATA */
        });
      </script>
