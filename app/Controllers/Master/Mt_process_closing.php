<?php namespace App\Controllers\Master; 
/*********************************************************************
 *  Created By       : Generator Version 1.0.23                      *
 *  Created Date     : Sep 03, 2020                                 *
 *  Description      : All code generated by controller generator    *
 *  Generator Author : Tommy Maurice(tommy_maurice@yahoo.com)        *
 *********************************************************************/
/*********************************************************************
 * To use XSS Filter, Altering                                       *
 * your application/config/autoload.php file in the following way:   *
 * $autoload['helper']=array('security');                            *
 * Example : $data = $this->security->xss_clean($data);              *
 *********************************************************************/
use App\Controllers\BaseController;
use App\Models\Master\M_process_closing;

$session = session();
class Mt_process_closing extends BaseController
{
     /* START CONSTRUCTOR */
     // public function __construct()
     // {
     // 	parent::__construct();
     // 	$this->load->helper('security');
     // 	$this->load->model('M_mst_process_closing');
     // }
     /* END CONSTRUCTOR */
     /* START INDEX FUNCTION */
     public function index()
     {
         $data['actView'] = 'Master/closing_view';
         return view('home', $data);
     }
     /* END INDEX FUNCTION */
     /* START INPUT FUNCTION */
     public function ins_view()
     {
         // $mtBiodata = new M_biodata_01();
         $mtClosing   = new M_process_closing();
         // $data['data_biodata'] = $mtBiodata->get_biodata();
         
         $data['actView'] = 'Master/closing_ins';
         return view('home', $data);
     }
     /* END INPUT FUNCTION */
     /* START INSERT */
     public function insData()
     {
          $userId = $_SESSION['uId'];
          $currDateTm = date("Y-m-d H:i:s");

          $mtClosing = new M_process_closing();

          $headerId = $mtClosing->generateId('closing_id')['doc_id'];

          $mtClosing->resetValues();
          $mtClosing->setClosingId($headerId);
          $mtClosing->setCloseYear($_POST['yearClose']);
          $mtClosing->setCloseMonth($_POST['monthClose']);
          $mtClosing->setIsActive($_POST['isActive']);
          $mtClosing->setPeriodNo($_POST['periodNo']);
          // $mtClosing->setInputTime($currDateTm);
          // $mtClosing->setPicInput($userId);
          $mtClosing->ins();
     }
     /* END INSERT*/
     /* START UPDATE FUNCTION */
     public function upd_view($closing_id)
     {
          $mtClosing = new M_process_closing();
          $data['dClose'] = $mtClosing->getById($closing_id);
          /* ***Using Valid Path */
          $data['actView'] = 'Master/upd_closing';
          return view('home', $data);
     }
     /* END UPDATE FUNCTION */
     /* START UPDATE */
     public function editData()
     {

          $id = $_POST['closingId'];

          $mtClosing = new M_process_closing();
          $userId = $_SESSION['uId'];
          $currDateTm = date("Y-m-d H:i:s");

          $isActived = '1'; 
          
          if($_POST['isActive'] == "Y"){
          $isActived = 0; 
          }
          // else if($_POST['isLocal'] == "T"){
          // $localed = 0; 
          // }

          $mtClosing->setObjectById($id);
          $mtClosing->setClosingId($_POST['closingId']);
          $mtClosing->setCloseYear($_POST['yearClose']);
          $mtClosing->setCloseMonth($_POST['monthClose']);
          $mtClosing->setIsActive($isActived);
          $mtClosing->setPeriodNo($_POST['periodNo']);
          $mtClosing->upd($id);
     }
     /* END UPDATE */
     /* START DELETE */
     public function del()
     {
     	$mtClosing = new M_process_closing();
          $id = $_POST['id'];
          $mtStatutory->del($id);
     }
     /* END DELETE */
      /* START GET ALL */
     public function getAll()
     {
          $mtClosing = new M_process_closing();
          $rs = $mtClosing->getAll();
          return json_encode($rs); 
     }
     /* END GET ALL */
     public function reset()
     {
          /*Clear Session*/
          if (!(isset($_SESSION['unset_userdata']))) {

            return redirect()->to(base_url('master/mt_process_closing'));
            
        }
     }

     public function status_closing()
     {
          $mtClosing     = new M_process_closing();
          $id            = $_POST['id'];
          $open          = $_POST['open'];
          $status        = $mtClosing->status_closing($id,$open);
          return $status;
     }
}
?>
