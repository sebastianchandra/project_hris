<?php namespace App\Controllers\Master;

use App\Controllers\BaseController;
use App\Models\Master\M_config;
use App\Models\Master\M_company;
use App\Models\Master\M_overtime;

use App\Models\Master\M_dept;

$session = session();

class Config extends BaseController
{
     protected $table = 'mt_biodata_01';

     function __construct(){
          helper('common');
     }

	public function index()
	{	
		// $biodataModel = new BiodataModel(); 	
		// $rs = $ebiodataodel->getAll();
		// return json_encode($rs);
		$data['actView'] = 'Master/config_view';
          // $data['actView'] = 'Master/view_class';
		return view('home', $data);	
	}

     public function getAll()
     {
          $mConfig       = new M_config();
          $rs            = $mConfig->getAll();
          return json_encode($rs);
     }

     public function ins_view()
     {
          $companyModel            = new M_company(); 
          $data['data_company']    =  $companyModel->getAll();
          $data['actView']         = 'Master/config_ins';
          // $data['actView'] = 'Master/ins_class';
          return view('home', $data);       
     }

     public function upd_view($pc_id)
     {
          $companyModel            = new M_company(); 
          $mtPayrollConfig         = new M_config();
          $mtOvertime              = new M_overtime();

          $data['data_company']    =  $companyModel->getAll();
          $data['data_config']     =  $mtPayrollConfig->getById($pc_id);
          $data['data_overtime']   =  $mtOvertime->getByIdCompany($data['data_config']['company_name']);
          $data['actView']         = 'Master/config_upd';
          // test($data['data_config'],1);
          return view('home', $data); 
     }

	public function insData()
	{
          $mtPayrollConfig    = new M_config();
          $mtOvertime         = new M_overtime();

          $headerId      = $mtPayrollConfig->generateId('pc_id')['doc_id'];
          $userId        = $_SESSION['uId'];
          $currDateTm    = date("Y-m-d H:i:s");

          $mtPayrollConfig->resetValues();
          $mtPayrollConfig->setPcId($headerId);
          $mtPayrollConfig->setCompanyName($_POST['company_id']);
          $mtPayrollConfig->setHealthBpjs($_POST['health_bpjs']);
          $mtPayrollConfig->setMaxHealthBpjs($_POST['max_health_bpjs']);
          $mtPayrollConfig->setMaxEmpBpjs($_POST['max_emp_bpjs']);
          $mtPayrollConfig->setMaxJp($_POST['max_jp']);
          $mtPayrollConfig->setMaxEmpJp($_POST['max_emp_jp']);
          $mtPayrollConfig->setJkkJkm($_POST['jkk_jkm']);
          $mtPayrollConfig->setJp($_POST['jp']);
          $mtPayrollConfig->setJht($_POST['jht']);
          $mtPayrollConfig->setEmpJht($_POST['emp_jht']);
          $mtPayrollConfig->setEmpHealthBpjs($_POST['emp_health_bpjs']);
          $mtPayrollConfig->setEmpJp($_POST['emp_jp']);
          $mtPayrollConfig->setPtkp($_POST['ptkp']);
          $mtPayrollConfig->setPtkpDependent($_POST['ptkp_dependent']);
          $mtPayrollConfig->setNpwpCharge($_POST['npwp_charge']);
          $mtPayrollConfig->setPicInput($userId);
          $mtPayrollConfig->setInputTime($currDateTm);
          $mtPayrollConfig->ins();

          $otId      = $mtOvertime->generateId('ot_id')['doc_id'];

          $mtOvertime->setOtId($otId);
          $mtOvertime->setCompanyId($_POST['company_id']);
          $mtOvertime->setOt1($_POST['ot1']);
          $mtOvertime->setOt2($_POST['ot2']);
          $mtOvertime->setOt3($_POST['ot3']);
          $mtOvertime->setOt4($_POST['ot4']);
          $mtOvertime->setMultiplier1($_POST['mp1']);
          $mtOvertime->setMultiplier2($_POST['mp2']);
          $mtOvertime->setMultiplier3($_POST['mp3']);
          $mtOvertime->setMultiplier4($_POST['mp4']);
          $mtOvertime->ins();

	}

     public function updData()
     {
          $mtPayrollConfig    = new M_config();
          $mtOvertime         = new M_overtime();

          $userId        = $_SESSION['uId'];
          $currDateTm    = date("Y-m-d H:i:s");

          // $mtPayrollConfig->setPcId($headerId);
          $mtPayrollConfig->setCompanyName($_POST['company_id']);
          $mtPayrollConfig->setHealthBpjs($_POST['health_bpjs']);
          $mtPayrollConfig->setMaxHealthBpjs($_POST['max_health_bpjs']);
          $mtPayrollConfig->setMaxEmpBpjs($_POST['max_emp_bpjs']);
          $mtPayrollConfig->setMaxJp($_POST['max_jp']);
          $mtPayrollConfig->setMaxEmpJp($_POST['max_emp_jp']);
          $mtPayrollConfig->setJkkJkm($_POST['jkk_jkm']);
          $mtPayrollConfig->setJp($_POST['jp']);
          $mtPayrollConfig->setJht($_POST['jht']);
          $mtPayrollConfig->setEmpJht($_POST['emp_jht']);
          $mtPayrollConfig->setEmpHealthBpjs($_POST['emp_health_bpjs']);
          $mtPayrollConfig->setEmpJp($_POST['emp_jp']);
          $mtPayrollConfig->setPtkp($_POST['ptkp']);
          $mtPayrollConfig->setPtkpDependent($_POST['ptkp_dependent']);
          $mtPayrollConfig->setNpwpCharge($_POST['npwp_charge']);
          $mtPayrollConfig->setPicEdit($userId);
          $mtPayrollConfig->setEditTime($currDateTm);
          $mtPayrollConfig->upd($_POST['pc_id']);

          // $mtOvertime->setOtId($otId);
          $mtOvertime->setCompanyId($_POST['company_id']);
          $mtOvertime->setOt1($_POST['ot1']);
          $mtOvertime->setOt2($_POST['ot2']);
          $mtOvertime->setOt3($_POST['ot3']);
          $mtOvertime->setOt4($_POST['ot4']);
          $mtOvertime->setMultiplier1($_POST['mp1']);
          $mtOvertime->setMultiplier2($_POST['mp2']);
          $mtOvertime->setMultiplier3($_POST['mp3']);
          $mtOvertime->setMultiplier4($_POST['mp4']);
          $mtOvertime->upd($_POST['ot_id']);
     }

	public function test()
	{
		$mtBiodata01 = new M_biodata_03();
		$rs = $mtBiodata02->generateId('experience_id')['doc_id'];
		return json_encode($rs);
	}	

     // public function reset()
     // {
     //      /*Clear Session*/
     //      $session = session();
     //      $data['unset_userdata'] = 0;
     //      unset(
     //         $_SESSION['unset_userdata']
     //      );
     //      // $session->destroy();

     //      return view('view_biodata');    
     // }

     public function reset()
     {
          /*Clear Session*/
          if (!(isset($_SESSION['unset_userdata']))) {

            return redirect()->to(base_url('master/Biodata'));
            
        }
     }

     public function myTransaction()
     {
          $db = \Config\Database::connect('hris', false);


          $db->transBegin();

          
          $sql = "INSERT INTO test1 (t1, ket) VALUES ('1', 'Test')";
          $db->query($sql);
          // throw new Exception("Error Processing Request", 1);       
          $sql = "INSERT INTO test2 (t2, ket) VALUES ('1', 'Test')";
          $db->query($sql);


          if ($db->transStatus() === FALSE)
          {
                  $db->transRollback();
          }
          else
          {
                  $db->transCommit();
          }

     }

     public function getIdNo()
     {    
          $mtBiodata01 = new M_biodata_01();
          $ptfId = '';
          // $db = \Config\Database::connect('hris', false);
          if(isset($_POST['biodataId'])){
          $PTFId = $_POST['biodataId'];
          // $birthPlace = $_POST['birthPlace'];
          // $birthDate = $_POST['birthDate'];
          }

          $rs = $mtBiodata01->getIdDuplicate($PTFId);
          return json_encode($rs);

          // $ptfId = '';
          // $db = \Config\Database::connect('hris', false);
          // if(isset($_POST['biodataId'])){
          // $PTFId = $_POST['biodataId'];
          // // $birthPlace = $_POST['birthPlace'];
          // // $birthDate = $_POST['birthDate'];
          // }

          // $stQuery  = "SELECT biodata_id FROM ".$this->table." ";     
          // $stQuery .= "WHERE biodata_id = '".$this->db->escapeString($PTFId)."'";
          // $query  = $this->db->query($stQuery); 
          // $arrayRow = $query->getRowArray(); 
          // return $arrayRow; 
     }

}
