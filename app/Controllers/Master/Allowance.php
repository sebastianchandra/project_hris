<?php namespace App\Controllers\Master;

use App\Controllers\BaseController;
use App\Models\Master\M_config;
use App\Models\Master\M_allowance;
use App\Models\Master\M_company;

use App\Models\Master\M_dept;

$session = session();

class Allowance extends BaseController
{
     protected $table = 'mt_biodata_01';

     function __construct(){
          helper('common');
     }

	public function index()
	{	
		// $biodataModel = new BiodataModel(); 	
		// $rs = $ebiodataodel->getAll();
		// return json_encode($rs);
		$data['actView'] = 'Master/allowance_view';
          // $data['actView'] = 'Master/view_class';
		return view('home', $data);	
	}

     public function getAll()
     {
          $mAllowance    = new M_allowance();
          $rs            = $mAllowance->getAll();
          return json_encode($rs);
     }

     public function ins_view()
     {
          $mAllowance              = new M_allowance(); 
          $companyModel            = new M_company(); 
          $data['data_field']      = $mAllowance->getAllField();
          $data['data_company']    = $companyModel->getAll();
          $data['actView']         = 'Master/allowance_ins';
          // $data['actView'] = 'Master/ins_class';
          return view('home', $data);
     }

     public function upd_view($id)
     {
          $mAllowance              = new M_allowance(); 
          $companyModel            = new M_company(); 
          $data['data_field']      = $mAllowance->getAllField();
          $data['data_allow']      = $mAllowance->getById($id);
          $data['data_company']    = $companyModel->getAll();
          $data['actView']         = 'Master/allowance_upd';
          // $data['actView'] = 'Master/ins_class';
          return view('home', $data);
     }

	public function insData()
	{
          $mAllowance    = new M_allowance();

          $headerId = $mAllowance->generateId('ma_id')['doc_id'];

          $mAllowance->setMaId($headerId);
          $mAllowance->setCompanyId($_POST['company_id']);
          $mAllowance->setMaName($_POST['ma_allowance']);
          $mAllowance->setMaFormula($_POST['ma_formula']);
          $mAllowance->setIsTax($_POST['is_tax']);
          $mAllowance->setTaxType($_POST['tax_type']);
          $mAllowance->ins();

	}

     public function updData()
     {
          $mAllowance    = new M_allowance();

          $mAllowance->setCompanyId($_POST['company_id']);
          $mAllowance->setMaName($_POST['ma_allowance']);
          $mAllowance->setMaFormula($_POST['ma_formula']);
          $mAllowance->setIsTax($_POST['is_tax']);
          $mAllowance->setTaxType($_POST['tax_type']);
          $mAllowance->upd($_POST['ma_id']);
     }

	public function test()
	{
		$mtBiodata01 = new M_biodata_03();
		$rs = $mtBiodata02->generateId('experience_id')['doc_id'];
		return json_encode($rs);
	}	

     // public function reset()
     // {
     //      /*Clear Session*/
     //      $session = session();
     //      $data['unset_userdata'] = 0;
     //      unset(
     //         $_SESSION['unset_userdata']
     //      );
     //      // $session->destroy();

     //      return view('view_biodata');    
     // }

     public function reset()
     {
          /*Clear Session*/
          if (!(isset($_SESSION['unset_userdata']))) {

            return redirect()->to(base_url('master/Biodata'));
            
        }
     }

     public function myTransaction()
     {
          $db = \Config\Database::connect('hris', false);


          $db->transBegin();

          
          $sql = "INSERT INTO test1 (t1, ket) VALUES ('1', 'Test')";
          $db->query($sql);
          // throw new Exception("Error Processing Request", 1);       
          $sql = "INSERT INTO test2 (t2, ket) VALUES ('1', 'Test')";
          $db->query($sql);


          if ($db->transStatus() === FALSE)
          {
                  $db->transRollback();
          }
          else
          {
                  $db->transCommit();
          }

     }

     public function getIdNo()
     {    
          $mtBiodata01 = new M_biodata_01();
          $ptfId = '';
          // $db = \Config\Database::connect('hris', false);
          if(isset($_POST['biodataId'])){
          $PTFId = $_POST['biodataId'];
          // $birthPlace = $_POST['birthPlace'];
          // $birthDate = $_POST['birthDate'];
          }

          $rs = $mtBiodata01->getIdDuplicate($PTFId);
          return json_encode($rs);

          // $ptfId = '';
          // $db = \Config\Database::connect('hris', false);
          // if(isset($_POST['biodataId'])){
          // $PTFId = $_POST['biodataId'];
          // // $birthPlace = $_POST['birthPlace'];
          // // $birthDate = $_POST['birthDate'];
          // }

          // $stQuery  = "SELECT biodata_id FROM ".$this->table." ";     
          // $stQuery .= "WHERE biodata_id = '".$this->db->escapeString($PTFId)."'";
          // $query  = $this->db->query($stQuery); 
          // $arrayRow = $query->getRowArray(); 
          // return $arrayRow; 
     }

}
